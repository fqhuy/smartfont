#!/usr/bin/python

"""
2D Generalized ICP

WHAT:

This is a python implementation of the generalized ICP algorithm described in:
"Generalized-ICP", Aleksandr V. Segal, Dirk Haehnel, and Sebastian Thrun, In Robotics: Science and Systems 2009

Paper available on Aleksander Segal's personal website:
http://www.stanford.edu/~avsegal/

TO INSTALL:

This code requires 3 modules to be installed on your system.  Numpy, Scipy, and the Python NIPALS PCA module by Henning Risvik.

http://www.scipy.org/kk

http://numpy.scipy.org/

http://folk.uio.no/henninri/pca_module/


TO RUN:

To run the example program, simply execute this file from the command-line with the following command:
python gen_icp.py

Copy the main section to your own code to have a working implementation and include this file as a module.

This code is provided under the GNU General Public License, Version 3 

Copyright 2009, by Jacob Everist
jacob.everist@gmail.com

http://jacobeverist.com/gen_icp

"""

import numpy
import scipy 
import scipy.linalg
import scipy.optimize
import math
import pylab
import pca_module

from copy import copy
from copy import deepcopy
from os.path import join
from scipy.spatial.distance import euclidean as euc
from math import sin, cos

def dispPoint1(p, offset):
	xd = offset[0]
	yd = offset[1]
	theta = offset[2]

	T = numpy.matrix([	[math.cos(theta), -math.sin(theta), xd],
			[math.sin(theta), math.cos(theta), yd],
			[0.0, 0.0, 1.0]
		    ])

	p_hom = numpy.matrix([[p[0]], [p[1]], [1.0]])
	temp = T * p_hom
	p_off = [temp[0, 0], temp[1, 0]]


	R = numpy.matrix([	[math.cos(theta), -math.sin(theta)],
		[math.sin(theta), math.cos(theta)] ])

	nvec = numpy.sqrt( numpy.diag(p[2]) )
	nvec = R.dot(nvec)
	Ca = numpy.outer(nvec, nvec)
	#Cv = p[2]
	#Ca = R * Cv * numpy.transpose(R)
	p_off.append(Ca)

	return p_off	

# displace the point by the offset plus modify it's covariance
def dispPoint(p, offset):
	xd = offset[0]
	yd = offset[1]
	theta = offset[2]

	T = numpy.matrix([	[math.cos(theta), -math.sin(theta), xd],
			[math.sin(theta), math.cos(theta), yd],
			[0.0, 0.0, 1.0]
		    ])

	p_hom = numpy.matrix([[p[0]], [p[1]], [1.0]])
	temp = T * p_hom
	p_off = [temp[0, 0], temp[1, 0]]


	R = numpy.matrix([	[math.cos(theta), -math.sin(theta)],
		[math.sin(theta), math.cos(theta)] ])

	Cv = p[2]
	Ca = R * Cv * numpy.transpose(R)
	p_off.append(Ca)

	return p_off

# displace the point by the offset only.  No covariance
def dispOffset(p, offset):
	xd = offset[0]
	yd = offset[1]
	theta = offset[2]

	T = numpy.matrix([	[math.cos(theta), -math.sin(theta), xd],
			[math.sin(theta), math.cos(theta), yd],
			[0.0, 0.0, 1.0]
		    ])

	p_hom = numpy.matrix([[p[0]], [p[1]], [1.0]])
	temp = T * p_hom
	p_off = [temp[0, 0], temp[1, 0]]

	return p_off

def disp(ai, bi, T):
	temp = T*ai
	result = bi-temp	
	result[2] = 1.0
	return result

def disp1(ai, bi, T, d0):

	temp = T * ai
	result = bi - temp	
	op = -(result / numpy.linalg.norm(result[0:2])) * d0
	#result /= 2.
	#result /= numpy.linalg.norm(result)
	#result *= 2
	result += op
	result[2] = 1.0

	return result


def computeMatchError(offset, a, b, Ca, Cb): #, d0):

	xd = offset[0]
	yd = offset[1]
	theta = offset[2]

	T = numpy.matrix([	[math.cos(theta), -math.sin(theta), xd],
			[math.sin(theta), math.cos(theta), yd],
			[0.0, 0.0, 1.0]
		    ])

	R = numpy.matrix([	[math.cos(theta), -math.sin(theta)],
		[math.sin(theta), math.cos(theta)] ])

	Cv = Ca
	#this uses  a different distance metric
	#d_vec = disp1(numpy.concatenate((a, numpy.matrix([1.0]))), numpy.concatenate((b, numpy.matrix([1.0]))), T, d0)
	d_vec = disp(numpy.concatenate((a, numpy.matrix([1.0]))), numpy.concatenate((b, numpy.matrix([1.0]))), T)
	
	#TODO: experimenting here
	#d_vec = d_vec[0:2]
	#error = d_vec.T.dot(Cb).dot(d_vec)
	#error = d_vec.dot(Cb)
	#return error
	#end

	res = Cb + R * Cv * numpy.transpose(R)

	# remove the 3rd homogeneous dimension for inverse
	invMat = scipy.linalg.inv(res)

	# add homogeneous dimension back
	invMat = numpy.concatenate((invMat, numpy.matrix([[0.0], [0.0]])), 1)
	invMat = numpy.concatenate((invMat, numpy.matrix([0.0, 0.0, 0.0])))

	error = numpy.transpose(d_vec) * invMat * d_vec
	errVal = error[0, 0]

	return errVal

def findLocalNormal_(idx, points):
	'''
	find normal vector centered at point idx of points
	'''
	if idx > 0 and idx < len(points) - 1:
		nbs = [points[idx - 1], points[idx + 1]]
	
	if idx == 0:
		nbs = [points[idx], points[idx + 1]]
		
	if idx == len(points) - 1:
		nbs = [points[idx - 1], points[idx]]
		
	v = numpy.array(nbs[1][0:2]) - numpy.array(nbs[0][0:2])
	n = numpy.array([v[1], v[0]])
	n = n / numpy.linalg.norm(n)
	return n

def findLocalNormal(pnt, points):

	x_list = []
	y_list = []

	pnt_count = 0
	pnts_copy = deepcopy(points)

	while pnt_count < 3:
		# select 3 closest points
		minDist = 1e100
		minPoint = []
		for p in pnts_copy:
			dist = math.sqrt((p[0] - pnt[0]) ** 2 + (p[1] - pnt[1]) ** 2)
		
			if dist < minDist:
				minDist = dist
				minPoint = p

		x_list.append(minPoint[0])
		y_list.append(minPoint[1])

		pnts_copy.remove(minPoint)
		pnt_count += 1

	x_list.append(pnt[0])
	y_list.append(pnt[1])

	cov_a = scipy.cov(x_list, y_list)

	loadings = []

	# NOTE:  seems to create opposing colinear vectors if data is colinear, not orthogonal vectors

	try:
		scores, loadings, E = pca_module.nipals_mat(cov_a, 2, 0.000001, False)

	except:
		raise

	if len(loadings) < 2:
		raise

	# return the second vector returned from PCA because this has the least variance (orthogonal to plane)
	return loadings[1]

def computeVectorCovariance(vec, x_var, y_var):
	Cv = numpy.matrix([	[x_var, 0.0],
			[0.0, y_var]
		    ])

	mag = math.sqrt(vec[0] ** 2 + vec[1] ** 2)
	normVec = numpy.array([vec[0] / mag, vec[1] / mag])
	
	# TODO: experimenting here
	#return numpy.linalg.pinv( numpy.outer(normVec, normVec.T) )

	if normVec[1] == 0:
		R = numpy.matrix([	[1.0, 0.0],
				[0.0, 1.0]
			    ])

	else:
		B = -1 / (normVec[1] + normVec[0] ** 2 / normVec[1])
		A = -normVec[0] * B / normVec[1]
		R = numpy.matrix([	[A, -B],
				[B, A]
			    ])

	Ca = numpy.transpose(R) * Cv * R

	return Ca

# for point T*a, find the closest point b in B
def findClosestPointInB(b_data, a, offset):

	xd = offset[0]
	yd = offset[1]
	theta = offset[2]

	T = numpy.matrix([	[math.cos(theta), -math.sin(theta), xd],
			[math.sin(theta), math.cos(theta), yd],
			[0.0, 0.0, 1.0]
		    ])

	a_hom = numpy.matrix([[a[0]], [a[1]], [1.0]])
	temp = T * a_hom
	a_off = [temp[0, 0], temp[1, 0]]

	minDist = 1e100
	minPoint = None

	for p in b_data:
		dist = math.sqrt((p[0] - a_off[0]) ** 2 + (p[1] - a_off[1]) ** 2)
		if dist < minDist:
			minPoint = copy(p)
			minDist = dist


	if minPoint != None:
		return minPoint, minDist
	else:
		raise


def cost_func(offset, match_pairs):

	sum_ = 0.0
	for pair in match_pairs:
		
		a = numpy.matrix([[pair[0][0]], [pair[0][1]]])
		b = numpy.matrix([[pair[1][0]], [pair[1][1]]])
		#sum_ += computeMatchError(offset, a, b, pair[2], pair[3], pair[4])
		sum_ += computeMatchError(offset, a, b, pair[2], pair[3])
		# NOTE:  standard point-to-point ICP
		'''
		a = numpy.matrix([[pair[0][0]],[pair[0][1]],[1.0]])
		b = numpy.matrix([[pair[1][0]],[pair[1][1]],[1.0]])

		distVec = disp(a, b, T)
		mag = distVec[0,0]**2 + distVec[1,0]**2
		sum_ += mag
		'''

	return sum_

def precomputeCovariance(points, high_var=1.0, low_var=0.001):

	for px,p in enumerate(points):
		C = numpy.matrix([	[high_var, 0.0],
				[0.0, high_var]
				])

		try:
			# the covariance matrix that enforces the point-to-plane constraint
			#normVec = findLocalNormal_(px, points)
			normVec = findLocalNormal(p, points)
			
			#TODO: experimenting here
			#vec = numpy.array(normVec)
			#normVec = vec / numpy.linalg.norm(vec)
			#C = numpy.outer(normVec, normVec)
			#end
			
			C = computeVectorCovariance(normVec, low_var, high_var)
		except:
			#print 'bad'
			pass

		p.append(C)

def gen_ICP(a_data, b_data, offset=[0.,0.,0.], costThresh=0.1, minMatchDist=2.0, plotIter=False, maxIters=20):

	# 1. compute the local line for each point
	# 2. compute the covariance for point-to-line constraint
	# 3. find closest points in A of B
	# 4. discard matches beyond threshold d_max < | ai - T *bi |
	# 5. optimize T for the sum of computeMatchError
	# 6. if converged, stop, else go to 3 with b offset by new T

	# DONE:  store the covariance for each point untransformed so we don't have to repeat
	# DONE:  transform each covariance by the appropriate rotation at each point

	# compute the local covariance matrix for a point-to-plane contrainst
	precomputeCovariance(a_data)
	precomputeCovariance(b_data)

	numIterations = 0
	lastCost = 1e100
	iter = 0
	while True:
		if iter >= maxIters:
			break
		else:
			iter += 1
		a_trans = []

		# pre-transform the A points and their associated covariances
		for p in a_data:
			a_trans.append(dispPoint(p, offset))
			#a_trans.append(dispPoint1(p, offset))

		match_pairs = []
		#pairs = [[],[],[]]
		for i in range(len(a_trans)):
			a_p = a_trans[i]

			# for every transformed point of A, find it's closest neighbor in B
			b_p, minDist = findClosestPointInB(b_data, a_p, [0.0, 0.0, 0.0])

			if minDist <= minMatchDist:
	
				# add to the list of match pairs less than 1.0 distance apart 
				# keep A points and covariances untransformed
				Ca = a_data[i][2]
				#Ca = numpy.array([[0.,0.],[0., 0.]]) 
				Cb = b_p[2]

				# we store the untransformed point, but the transformed covariance of the A point
				#match_pairs.append([a_data[i], b_p, Ca, Cb, d0_data[i]])
				match_pairs.append([a_data[i], b_p, Ca, Cb])
				
				#pairs[0].append(b_p[0:2])
				#pairs[1].append(a_data[i][0:2])
				#pairs[2].append(Cb)
			else:
				print 'discarded', minDist

		
		# optimize the match error for the current list of match pairs
		#TODO: experimenting here
		#newOffset = linear_approximation(pairs[0], pairs[1], pairs[2])
		newOffset = scipy.optimize.fmin(cost_func, offset, [match_pairs])
		#print 'offset', newOffset

		# get the current cost
		newCost = cost_func(newOffset, match_pairs)

		# check for convergence condition, different between last and current cost is below threshold
		if abs(lastCost - newCost) < costThresh:
			offset = newOffset
			lastCost = newCost
			break

		# save the current offset and cost
		offset = newOffset
		lastCost = newCost
		numIterations += 1

		# optionally draw the position of the points in current transform
		if plotIter:
			draw(a_trans, b_data, "ICP_plot_%04u.png" % numIterations)

	return offset


def linear_approximation(d, s, n_):
	'''
	solve for optimal T by using linear least square fitting
	'''
	#recover the normal vector from projection matrix
	n = [numpy.sqrt(numpy.diag(i)) for i in n_]
	b = numpy.array([n[i][0] * d[i][0] + n[i][1] * d[i][1] - n[i][0] * s[i][0] - n[i][1] * s[i][1] for i in range(len(d))])
	A = numpy.hstack([numpy.array([[n[i][1] * s[i][0] - n[i][0] * s[i][1]] for i in range(len(d))]), numpy.array(n)])
	# [U,S,VT] = numpy.linalg.svd(A)
	# x = (VT.T * numpy.linalg.inv(S) * U.T).dot(b)
	try:
		x = numpy.linalg.pinv(A).dot(b)
	except:
		pass
	return [x[1], x[2], x[0]]

def draw(a_pnts, b_pnts, filename):

	for a in a_pnts:
		pylab.scatter([a[0]], [a[1]], linewidth=1, color='b')
		
	for b in b_pnts:
		pylab.scatter([b[0]], [b[1]], linewidth=1, color='r')

	#pylab.xlim(-4.5, 4.5)
	#pylab.ylim(-4, 4)
	pylab.autoscale()
	pylab.savefig(join('.tmp', filename))
	pylab.clf()


if __name__ == '__main__':
	from shapely.geometry import asLineString, Point
	# Max
	maxIters = 20
	# TUNE ME:  threshold cost difference between iterations to determine if converged
	costThresh = 0.05

	# TUNE ME:   minimum match distance before point is discarded from consideration
	minMatchDist = 2

	# plot the best fit at each iteration of the algorithm?
	plotIteration = True

	# initial guess for x, y, theta parameters
	offset = [0.0, 0.0, 0.0]

	# sample data
	path = '/media/phan/BIGDATA/SMARTFONTS/data'
	data = numpy.load(join(path, 'Adamina-Regular.ttf.npz'))
	points = data['points'][26][0]
	a_data = points[0] + list(reversed(points[1]))
	#b_data = points[2] #+ list(reversed(points[3]))
	b_data = []
	b_tmp = asLineString(points[2])
	for i in range(100):
		point = b_tmp.interpolate(i / 100., True)
		b_data.append(numpy.array(point.coords[0]))
	#d0_data = numpy.array([euc(a_data[p], b_data[p]) for p in range(len(a_data))])
	d0_data = numpy.array([b_tmp.distance(Point(a_data[p])) for p in range(len(a_data))])
	scale = 1. / numpy.max(d0_data)
	d0_data *= scale
	theta = numpy.radians(8)
	tx, ty = 0.0, 0.0
	#transform a bit
	T = numpy.matrix([[cos(theta), -sin(theta), tx],[sin(theta), cos(theta), ty],[0,0,1]])
	b_data = [(p * scale).tolist() for p in b_data]
	a_data = numpy.array(a_data) * scale
	a_data = T.dot( numpy.hstack([a_data, numpy.ones((len(a_data), 1), dtype='float64')]).T ).T[:,0:2].tolist()

	# plot the data without A transformed, plot 997
	draw(a_data, b_data, "rawData.png")

	# transform the points in A by 'offset'
	a_trans = []
	for p in a_data:
		a_trans.append(dispOffset(p, offset))

	# plot the data with A transformed, plot 998
	draw(a_trans, b_data, "initialGuess.png") 

	# run generalized ICP (a plot is made for each iteration of the algorithm)
	offset = gen_ICP(offset, a_data, b_data, d0_data, costThresh, minMatchDist, plotIteration, maxIters)

	# transform the points of A with parameters determined by algorithm
	a_trans = []
	for p in a_data:
		a_trans.append(dispPoint(p, offset))

	# plot the final result, plot 999
	draw(a_trans, b_data, "finalOutput.png") 

