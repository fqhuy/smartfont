# -*- coding: utf-8 -*-
"""
Created on Wed Jul  9 00:09:55 2014

@author: phan
"""
import Common as cm
#from Common import hausdorff_distance
from Graph import skel, scale
import PHOG
import numpy as np
import os
import copy
from os.path import join
from skimage.feature import hog
from scipy.spatial.distance import euclidean as euc
from cgal.CGAL import *
from Feature import calc_dhist_feature_graph
#import ICP as icp
from gicp import gicp
from fast.Distance import hausdorff

import math

def min_dist(a, b):
    d = 0
    for p in a:
        d += np.min([euc(p, p1) for p1 in b])
    return d

def retrieve_skeleton_graph(query, fonts, chars=cm.capital_chars):
    '''
    skeleton retrieval based on graph
    '''
    newgraphs = skel(query)
    dists = np.zeros((len(fonts), len(cm.all_chars)), dtype='float64')
    #scales = np.arange(0.5, 1.5, 0.1)
    for gx, graph in enumerate(newgraphs):
        if not cm.all_chars[gx] in chars:
            continue
        
        if len(graph.nodes()) < 10:
            #dists[fx, gx]
            print 'invalid char: ', cm.all_chars[gx]
            continue
        
        print cm.all_chars[gx],
        
        a_data = []
        for node in graph.nodes_iter():
            a_data.append( graph.node[node]['pos'] )
        a_data = np.ascontiguousarray(a_data, dtype='double')
        fx = 0
        for _, graphs2, _ in fonts:
            b_data = []
            for node in graphs2[gx].nodes_iter():
                b_data.append( graphs2[gx].node[node]['pos'])
                
            b_data = np.ascontiguousarray(b_data, dtype='double')    
            #offset = icp.gen_ICP([a.tolist() for a in a_data] , [b.tolist() for b in b_data],minMatchDist=500,plotIter=True)
            corres = np.zeros((len(b_data), 3),dtype='double')
            t1 = gicp.icp(b_data, a_data, corres, 500.0, 0.01)
            #offset = [t1[0, 3], t1[1, 3], math.acos(t1[0,0])]
            #t1[0:3,0:3] = np.eye(3,3)
            #trans_b_data = t1.dot( np.hstack((b_data, np.tile([0,1], b_data.shape[0]).reshape(b_data.shape[0], 2))).T ).T[:, 0:2]
            trans_b_data = b_data + np.array([t1[0, 3], t1[1, 3]])
            
            dists[fx, gx] = hausdorff(trans_b_data, a_data, mode=1) #corres[:, 2].sum()
            fx += 1
    
    mask = np.array([c in chars for c in cm.all_chars])
    #print dists[:, mask].sum(axis=1)
    #best_ids = dists.argmin(axis=0) #
    best_id = dists[:, mask].sum(axis=1).argmin()
    
    #data = np.load(join(data_path, fonts[best_id][0]))
    
    return fonts[best_id][0] #best_ids #
                
def retrieve_skeleton(path, bitmaps, chars):
    '''
    retrieve similar skeletons based on PHOG matching
    @param bitmaps - a list of characters in bitmap format
    @param path - contains all the annotated fonts
    '''
    allfiles = sorted([f for f in os.listdir(path) if f.endswith('.npz')])
    files = []    
    for f in allfiles:
        data = np.load(join(path, f))
        if 'chars' in data.files:
            chars_ = list(data['chars'])
        else:
            chars_ = cm.all_chars
        
        if 'segments' in data.files:
            segments = data['segments']
            frm = chars_.index('Q')
            to = chars_.index('M')
            valid = True
            for i in range(frm, to):
                if len(segments[i]) == 0:
                    valid = False
                    break
                
            #if np.all(segments[frm:to] != []):
            if valid:
                files.append(f[:-4] + '.phog.npy')
            
    t_max_score = cm.eps
    max_id = 0
    t_max_levels= []
    t_max_level, t_max_row, t_max_col = 0, 0, 0
    templates = []
    for bx, bitmap in enumerate(bitmaps):
        #idx = cm.all_chars.index(chars[bx])
        template = PHOG.hog(bitmap)
        templates.append(template)

    for fx, f in enumerate(files):
        data = np.load(join(path, f))
        max_scores, max_levels, max_rows, max_cols = [], [], [], []
        for bx in range(len(bitmaps)):
            idx = cm.all_chars.index(chars[bx])
            #template = PHOG.hog(bitmap)
            template = templates[bx]
            h1, w1 = template.shape[0], template.shape[1]
            max_score = 0
            max_level, max_row, max_col = 0, 0, 0
            for level in range(0, 3):
                im = data[idx][level] #copy.deepcopy(data[idx][level])
                h2, w2 = im.shape[0], im.shape[1]
                padx, pady = 0, 0
                if h1 > h2:
                    pady = h1 - h2
                    
                if w1 > w2:
                    padx = w1 - w2
                    
                if padx != 0 or pady != 0:
                    #try:
                    padded_image = np.pad(im, ((pady, pady), (padx, padx), (0, 0)),'constant')
                    #except ValueError:
                    #    print 'cannot pad image'
                else:
                    padded_image = im
                    
                if padx == 0:
                    padx = w2 - w1 + 1
                    
                if pady == 0:
                    pady = h2 - h1 + 1

                score = 0
                for row in range(pady):
                    for col in range(padx):
                        #try:
                        #score = euc(template.ravel(), padded_image[row : row + h1, col : col + w1].ravel())
                        score = template.ravel().dot( \
                        padded_image[row : row + h1, col : col + w1].ravel())
                        
                        #except ValueError:
                        #    print 'convolution failed'
                        if score > max_score:
                            max_score = score
                            max_row = row
                            max_col = col
                            max_level = level
                            
            max_levels.append(max_level)
            max_rows.append(max_row)
            max_cols.append(max_col)
            max_scores.append(max_score)
        
                         
        if sum(max_scores) > t_max_score:
            max_id = fx
            t_max_col = sum(max_cols) / len(max_cols)
            t_max_row = sum(max_rows) / len(max_rows)
            t_max_level = sum(max_levels) / len(max_levels)
            #t_max_levels = max_levels
            t_max_score = sum(max_scores)
            
        print f, sum(max_scores)
            
    data = np.load(join(path, files[max_id][:-9] + '.npz'))
    skels = data['annotations']
    _, scales = PHOG.build_indices()
    #ch_scales = [1] * 62
    print t_max_level, t_max_row, t_max_col, files[max_id]
    return skels * scales[t_max_level] #t_max_level, t_max_row, t_max_col, files[max_id]
    #return skels
    
'''    
if __name__ == '__main__':
    dataroot = '/media/phan/BIGDATA/SMARTFONTS/data'
    testfont = '/media/phan/BIGDATA/SMARTFONTS/img/AdventPro-Bold.ttf.npz'
    data = np.load(testfont)['bitmaps']
    indices = [cm.all_chars.index(ch) for ch in cm.capital_chars]
    #H, M, O = cm.all_chars.index('H'), cm.all_chars.index('M'), cm.all_chars.index('O')
    bitmaps = [data[idx] for idx in indices]
    
    skels = retrieve_skeleton(dataroot, bitmaps, cm.capital_chars)
    
    #print level, row, col, idd
'''

def find_similar_fonts(path, fname, fonts=[], mode='u', chars=[]):
    '''
    mode = (u)pper case or (l)ower case
    '''
    #preparing the database
    data_path = join(path, 'data')
    data = np.load(join(data_path, fname))
    graphs = data['graphs']
        
    allfiles = sorted([f for f in os.listdir(data_path) if f.endswith('.npz')])[:500]
    #fonts = [] 
    specs = np.load(join(path, 'word_specs.npz'))['specs']
    #retrieve the best fit one
    if len(chars) == 0:
        if mode == 'u':
            chars = cm.capital_chars
        elif mode == 'l':
            chars = cm.lower_chars
            
    if len(fonts) == 0:
        if os.path.isfile(join(path, mode + 'fonts.npy')):
            fonts = list(np.load(join(path, mode + 'fonts.npy')))
        else:    
            for f in allfiles: 
                #print f,
                data = np.load(join(data_path, f))
                if 'annotations' in data and 'graphs' in data:
                    #points = data['points']
                    graphs_ = data['graphs']
                    anns = data['annotations']
                       
                    #if all([ len([d for d in ds if len(d) > 0]) > specs[dsx] for dsx, ds in enumerate(data['annotations']) if cm.all_chars[dsx] in chars]):
                    valid = True
                    for dsx, ds in enumerate(anns):
                        if cm.all_chars[dsx] in chars:
                            if sum([len(d) > 0 for d in ds]) < specs[dsx]:
                                valid = False
                            
                    if valid:
                        print f,'*'
                        graphs_ = skel(graphs_)
                        fonts.append((f, graphs_, anns))
                        
            np.save(join(path, mode + 'fonts.npy'),fonts)
            
    print 'n_fonts = ', len(fonts)
    for fx, fn in enumerate(fonts):
        if fn[0] == fname:
            fonts.pop(fx)
            break
        
    bestid = retrieve_skeleton_graph(graphs, fonts, chars=chars)
    data = np.load(join(data_path, bestid))
    
    ref_graphs = data['graphs']
    ref_anns = data['annotations']
    scales = np.arange(0.5, 1.51, 0.1)
    newfonts = []
    for sclx, scl in enumerate(scales):
        newgraphs = []
        for ch in cm.all_chars:
            chx = cm.all_chars.index(ch, )
            if ch in chars:
                newgraphs.append(scale(ref_graphs[chx], scl))
            else:
                newgraphs.append(None)
            
        newfonts.append((str(sclx), newgraphs, ref_anns))
    
    bestscale = int(retrieve_skeleton_graph(graphs, newfonts, chars=chars))
    return (bestid, scales[bestscale], fonts) #[(fonts[idd][0], fonts[idd][1][idx], fonts[idd][2][idx]) for idx, idd in enumerate(bestids)]          
    #print rfile
    
if __name__ == '__main__':
    path = '/media/phan/BIGDATA/SMARTFONTS/'
    fname = 'Arapey-Regular.ttf.npz'
    print find_similar_fonts(path, fname)
    
    '''            
    f1 = open(join(path, 'a_data.ascii'),'w')
    for node in fonts[0][1][26].nodes_iter():
        p = fonts[0][1][26].node[node]['pos']
        f1.write( '%4.4f\t%4.4f\t0.0000\n' % tuple(p) )
        #a_data.append(graphs[26].node[node]['pos'])
    f1.close()
    
    f2 = open(join(path, 'b_data.ascii'),'w')
    for node in graphs[26].nodes_iter():
        f2.write( '%4.4f\t%4.4f\t0.0000\n' % tuple(graphs[26].node[node]['pos'].tolist()))
        #a_data.append(graphs[26].node[node]['pos'])
    f2.close()    
    '''   