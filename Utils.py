'''
Created on 15 Sep, 2014

@author: phan
'''
import svgfig
from svg.path import parse_path
import numpy as np
from os.path import join
from cgal import CGAL
from Synthesization import load_svg
all_chars = list("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890")

def find_cap_anchors(chrange):
    import Common as cm
    import os
    path = cm.PATH
    files = sorted([f for f in os.listdir(join(path, 'data')) if f.endswith('.ttf.npz')])[:500]
    fonts = []
    for f in files:
        data = np.load(join(path, f))
        if 'points' in data:
            points = data['points']
            specs_ = np.array([len(a) for a in points])
            if np.all(specs_[chrange] >= cm.specs[chrange]):
                #print f 
                anns = data['annotations']
                graphs = data['graphs']
                points = [[]] * len(cm.all_chars)
                for ax, ann in enumerate(anns): #every character
                    graph = graphs[ax]
                    pss = []
                    for seg in ann: #every segment
                        ps = [list(graph.node[p]['pos']) for p in seg]
                        pss.append(ps)
                        
                    points[ax] = np.array(pss)
            fonts.append(points)
            
    closests = []
    for font in fonts:
        closests.append([])
        for chx, char in enumerate(font):
            closests[-1] = []
            for px1, part1 in enumerate(char[cm.mains[chx]:]):
                best = [0, 0]
                for px2, part2 in enumerate(char[:cm.mains[chx]]):
                    terms = part2[0], part2[-1]
                    for tx, term in enumerate(terms):
                        d = euclidean(part1.mean(axis=0), term)
                        if d < mind:
                            mind = d
                            best = [px2, tx]
                closests[-1].append(best)

def parse_polylines(fontname, files, scale):
    outlines = [[] for _ in range(len(all_chars))]
    
    for filename, ch in files:
        '''
        svg = svgfig.load(filename)
        
        #select the first group as the main one
        for subx, sub in enumerate(svg.sub):
            if sub.t == 'g':
                idd = subx
                break
                    
        pss = []
        for subpath in svg.sub[idd].sub:
            if subpath.t == 'path':
                path = svgfig.pathtoPath(subpath) 
                ps = []
                cupos = np.array([0, 0])
                for item in path.d:
                    if len(item) == 4:
                        cupos += np.array([item[1], item[2]])
                        pos = cupos.copy()
                        ps.append(pos.tolist())
                        
                ps = np.array(ps) * np.array([scale, -scale])
                pss.append(ps[::-1])
        
        minpoint = np.min(np.concatenate(pss), axis=0)
        for contour in pss:
            contour -= minpoint
        pss = np.array(pss)
        '''
        pss = load_svg(filename, mode='polyline',folder='')
        for ps in pss:
            ps *= np.array([scale, -scale])
        outlines[all_chars.index(ch)] = pss 
        
    np.savez(fontname + '.npz', outlines=outlines, chars=all_chars, annotations=[[] for _ in range(62)], caps=[[] for _ in range(62)])
    
if __name__ == '__main__':
    path = '/home/phan/workspace/coolfont/coolfont/tests/.tmp' #'/media/phan/BIGDATA/SMARTFONTS/'
    chars = list('ADHLRST')

    #parse_polylines('/media/phan/BIGDATA/SMARTFONTS/examples/comparison_1/Test1.svg','A', 20.0)
    fnames = []   
    for ch in chars:
        fnames.append( (join(path, ch), ch) )
        
    parse_polylines(join(path, 'EG1.1.ttf'), fnames, 20)