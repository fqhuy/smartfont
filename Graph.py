'''
Created on 2 Sep, 2014

@author: phan
'''
from cgal import CGAL
from scipy.spatial.distance import euclidean as euc
import networkx as nx

def rename_node(graph, oldname, newname):
    attr = graph.node[oldname].copy()
    graph.add_node(newname, attr)
    
    nbs = nx.neighbors(graph, oldname)
    
    for nb in nbs:
        attr1 = graph.edge[oldname][nb]
        attr2 = graph.edge[nb][oldname]
        graph.add_edge(nb, newname, attr1)
        graph.add_edge(newname, nb, attr2)
        
    graph.remove_node(oldname)

def scale(graph_, scl, copy=True):
    graph = graph_.copy()
    for node in graph.nodes():
        newpos = graph.node[node]['pos'] * scl
        graph.node[node]['pos'] = newpos
        
    for n1, n2 in graph.edges():
        graph.edge[n1][n2]['d'] = euc(graph.node[n1]['pos'], graph.node[n2]['pos'])
    return graph

def skel(graphs, mode=1):
    newgraphs = []
    for graph in graphs: #for every character
        if graph == None:
            newgraphs.append(None)
            continue
            
        nocontour = graph.copy()
        if mode == 0:
            for u, v, data in nocontour.edges_iter(data=True):
                if data['mode'] == CGAL.CONTOUR:
                    nocontour.remove_edge(u, v)
        else:
            for v, data in nocontour.nodes_iter(data=True):
                if data['mode'] != CGAL.SKELETON:
                    nocontour.remove_node(v)
                   
        newgraphs.append(nocontour)
        
    return newgraphs

if __name__ == '__main__':
    pass