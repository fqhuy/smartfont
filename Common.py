# -*- coding: utf-8 -*-
"""
Created on Tue May  6 21:33:23 2014

@author: phan
"""
import numpy as np
import platform
#from scipy.spatial.distance import wminkowski
#import Feature as ft

#weuc1 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[1.0] * (len(a1)/2), [0.05] * (len(a1)/2)]).T.flatten())
#weuc2 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[0.1] * (len(a1)/2), [1.0] * (len(a1)/2)]).T.flatten())
#dists = {'outline' : weuc1, 'cap' : ft.simple_hausdorff, 'skel' : weuc2}
            
alphas = {'skel' : 0.005, 'outline' : 1e-4, 'cap' : 1e-6}

mains = np.array([2, 4, 3, 2, 2, 3, 2, 1, 2, 2, 2, 1, 2, 2, 3, 2, 1, 3, 1, 3, 3, 1, 2,
       2, 2, 3, 3, 4, 4, 3, 2, 3, 2, 1, 2, 2, 3, 1, 2, 3, 2, 3, 1, 3, 2, 3,
       3, 1, 2, 3, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

specs = np.array([4, 9, 4, 5, 4, 6, 5, 4, 2, 4, 4, 3, 4, 4, 5, 5, 4, 7, 3, 7, 7, 3, 5,
       4, 5, 7, 4, 9, 9, 6, 5, 6, 4, 3, 2, 4, 6, 3, 4, 7, 5, 7, 3, 7, 5, 7,
       7, 3, 5, 5, 7, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) 
#              0      1      2      3      4      5
all_parts = ['Q00', 'Q01', 'Q02', 'Q03', 'W00', 'W01', 'W02', 'W03', 'W04', 'W05', 'W06', 'W07', 'W08', 'E00', 'E01', 'E02', 'E03', 'E04', 'E05', 'E06', 'E07', 'E08', 'R00', 'R01', 'R02', 'R03', 'R04', 'R05', 'T00', 'T01', 'T02', 'T03', 'T04', 'Y00', 'Y01', 'Y02', 'Y03', 'Y04', 'Y05', 'U00', 'U01', 'U02', 'U03', 'I00', 'I01', 'I02', 'O00', 'O01', 'P00', 'P01', 'P02', 'P03', 'A00', 'A01', 'A02', 'A03', 'A04', 'A05', 'S00', 'S01', 'S02', 'D00', 'D01', 'D02', 'D03', 'F00', 'F01', 'F02', 'F03', 'F04', 'F05', 'F06', 'G00', 'G01', 'G02', 'G03', 'G04', 'H00', 'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'J00', 'J01', 'J02', 'K00', 'K01', 'K02', 'K03', 'K04', 'K05', 'K06', 'L00', 'L01', 'L02', 'L03', 'L04', 'Z00', 'Z01', 'Z02', 'Z03', 'Z04', 'Z05', 'Z06', 'X00', 'X01', 'X02', 'X03', 'X04', 'X05', 'X06', 'C00', 'C01', 'C02', 'V00', 'V01', 'V02', 'V03', 'V04', 'B00', 'B01', 'B02', 'B03', 'B04', 'N00', 'N01', 'N02', 'N03', 'N04', 'N05', 'N06', 'M00', 'M01', 'M02', 'M03', 'M04', 'M05', 'M06', 'M07', 'M08']

def get_max_val(mode):
    if mode == 'thickness':
        return 724.1452096346992
    elif mode == 'curvature':
        return 10275.210419232366
    elif mode == 'angle':
        return 1.5707963267948966

def get_max_thickness():
    return 724.1452096346992
    
anchors = {'Q03' : [('Q02', 1)], 
           'W04' : [('W00', 0)], 
           'W05' : [('W00', 1), ('W01', 1)], 
           'W06' : [('W01', 0), ('W02', 0)], 
           'W07' : [('W02', 1), ('W03', 1)], 
           'W08' : [('W03', 0)], 
           'E04' : [('E00', 0), ('E01', 0)],
           'E05' : [('E00', 1), ('E03', 0)],
           'E06' : [('E01', 1)], 
           'E07' : [('E02', 1)],
           'E08' : [('E03', 1)],
           'R03' : [('R00', 0), ('R01', 0)],
           'R04' : [('R00', 1)],
           'R05' : [('R02', 1)],
           'T02' : [('T01', 0)],
           'T03' : [('T01', 1)],
           'T04' : [('T00', 1)],
           'Y03' : [('Y00', 0)],
           'Y04' : [('Y02', 1)],
           'Y05' : [('Y01', 0)],
           'U02' : [('U00', 0)],
           'U03' : [('U01', 0)],
           'I01' : [('I00', 0)],
           'I02' : [('I00', 1)],
           'P02' : [('P00', 0)],
           'P03' : [('P00', 1)],
           'A03' : [('A00', 0), ('A01', 0)],
           'A04' : [('A00', 1)],
           'A05' : [('A01', 1)],
           'S01' : [('S00', 0)],
           'S02' : [('S00', 1)],
           'D02' : [('D00', 0), ('D01', 0)],
           'D03' : [('D00', 1), ('D01', 1)],
           'F03' : [('F00', 0), ('F01', 0)],
           'F04' : [('F00', 1)],
           'F05' : [('F01', 1)],
           'F06' : [('F02', 1)],
           'G03' : [('G00', 0)],
           'G02' : [('G01', 0)],
           'G04' : [('G01', 1)],
           'H03' : [('H00', 0)],
           'H04' : [('H00', 1)],
           'H05' : [('H01', 0)],
           'H06' : [('H01', 1)],
           'J02' : [('J00', 0)],
           'J01' : [('J00', 1)],
           'K03' : [('K00', 0)],
           'K04' : [('K00', 1)],
           'K05' : [('K01', 0)],
           'K06' : [('K02', 1)],
           'L02' : [('L00', 0)],
           'L03' : [('L00', 1), ('L01', 1)],
           'L04' : [('L01', 1)],
           'Z03' : [('Z01', 0)],
           'Z04' : [('Z02', 1)],
           'Z05' : [('Z00', 0), ('Z01', 1)],
           'Z06' : [('Z00', 1), ('Z02', 0)],
           'X03' : [('X00', 0)],
           'X04' : [('X02', 1)],
           'X05' : [('X01', 0)],
           'X06' : [('X00', 1)],
           'C01' : [('X00', 0)],
           'C02' : [('X00', 1)],
           'V02' : [('V00', 0)],
           'V03' : [('V00', 1), ('V01', 1)],
           'V04' : [('V01', 0)],
           'B03' : [('B00', 0), ('B01', 0)],
           'B04' : [('B00', 1), ('B02', 1)],
           'N03' : [('N00', 0), ('N01', 0)],
           'N04' : [('N00', 1)],
           'N05' : [('N02', 0)],
           'N06' : [('N01', 1), ('N02', 1)],
           'M04' : [('M00', 0), ('M01', 0)],
           'M05' : [('M00', 1)],
           'M06' : [('M01', 1), ('M02', 1)],
           'M07' : [('M02', 0), ('M03', 0)],
           'M08' : [('M03', 1)]
           }
           
if platform.system() is 'Windows':
    PATH = 'F:\DPAINTINGS'
else:
    PATH = '/media/phan/BIGDATA/SMARTFONTS'
    
all_chars = list("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890")
lower_chars = list("qwertyuiopasdfghjklzxcvbnm")
capital_chars = np.array(list("QWERTYUIOPASDFGHJKLZXCVBNM"))
eps = 1e-10
import matplotlib
import numpy as np
from scipy.spatial.distance import euclidean as euc

color_names = matplotlib.colors.cnames.keys()

def neighbours(plist, px, size, include_mid=True, get_id=False):
    left = max(0, px - size)
    right = min(px + size, len(plist) - 1)
    
    if get_id:
        return range(left, right + 1)    
    
    if include_mid:
        return plist[left : right + 1]
    else:
        return plist[left : px] + plist[px + 1 : right + 1]
    
def hausdorff_distance(poly1, poly2):
    #http://en.wikipedia.org/wiki/Hausdorff_distance <--- using this
    d1 = 0
    d2 = 0
    for p in poly1:
        d1 += min([euc(p, p1) for p1 in poly2])
        
    #for p in poly2:
    #    d2 += min([euc(p, p1)**2 for p1 in poly1])
        
    #d = d1 + d2
    #d = d1 / sum([euc(poly1[p], poly1[p] + 1)**2 for p in range(len(poly1) - 1)]) + \
    #d2 / sum([euc(poly2[p], poly2[p] + 1)**2 for p in range(len(poly2) - 1)])
    
    d = d1 / sum([euc(poly1[p], poly1[p] + 1) for p in range(len(poly1) - 1)]) #len(poly1)
    return d
        
def modify_npz(original_file, replacements):
    '''
    replacements [('fieldname', newvalue)]
    '''
    data = np.load(original_file)
    kwargs = {}
    for f in data.files:
        try:
            kwargs[f] = data[f]
        except Exception as e:
            kwargs[f] = None
        
    for field, rep in replacements:
        kwargs[field] = rep
        
    np.savez(original_file, **kwargs)
