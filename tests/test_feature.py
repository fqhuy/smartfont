'''
Created on 12 Sep, 2014

@author: phan
'''
import unittest
from os.path import join
import numpy as np
import Feature as ft
from pyemd import emd
import Common as cm

class Test(unittest.TestCase):


    def setUp(self):
        path = '/media/phan/BIGDATA/SMARTFONTS'
        data_path = join(path, 'data')
        self.fname = 'Adamina-Regular.ttf.npz'
        
        tmpdata = np.load(join(path, 'outline_dict.npz'))
        self.dictionary = tmpdata['clusters']
        self.dmat = tmpdata['mat']
        self.norm_params = (tmpdata['mean'], tmpdata['minva'], tmpdata['thresh'])
        
        tmpdata = np.load(join(data_path, self.fname))
        self.points = tmpdata['points']
        
        #tmpdata = np.load(join(path, 'outline.pWs.npz'))
        #self.parts = tmpdata['parts']
        
        tmpdata = np.load(join(path, 'word_specs.npz'))
        self.mains = tmpdata['mains']

    def tearDown(self):
        pass

    def testStrokeFeature(self):
        pairs = [('Y', 0), ('Y', 1)]
        #hists = []
        #for ch, pt in pairs:
        ch, pt = pairs[0]
        dists = []
        for ch1 in range(26, 52):
            for prt1 in range(self.mains[ch1]):
                chx = cm.all_chars.index(ch)
                #feat = ft._calc_feature_stroke(self.points[chx][pt], mode='stroke')
                hist0 = ft._calc_dhist_feature_graph(self.points[chx][pt], self.dictionary, mode='stroke',norm_params=self.norm_params)
                hist1 = ft._calc_dhist_feature_graph(self.points[ch1][prt1], self.dictionary, mode='stroke',norm_params=self.norm_params)
                dists.append((cm.all_chars[ch1] + str(prt1), emd(hist0, hist1, self.dmat)))
                
        print list(sorted(dists, key=lambda x: x[1]))
    
    def testCapFeature(self):
        pairs = [('D', 2), ('D', 3)]
        feats = []
        for ch, pt in pairs:
            chx = cm.all_chars.index(ch)
            feats.append(self.points[chx][pt])
        
        #print ft.overlap(feats[0], feats[1])
            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()