# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 17:44:50 2015

@author: phan
"""
import unittest
import numpy as np
import gensim
from os.path import join
import Common as cm
#from LDA import SimpleBOWCorpus, get_values, StyleModel
import LDA
#from Synthesization import transfer_style_graph, stroke_estimation 
#import Synthesization as sz
#from descartes import PolygonPatch
from shapely.geometry import asPolygon, Polygon, Point, asMultiPolygon, box
from shapely.ops import cascaded_union
from matplotlib import pyplot
from matplotlib.colors import cnames
from scipy.spatial.distance import euclidean as euc
from scipy import signal
import SkeletonRetrieval as sr
from fast.Distance import hausdorff
from DataSource import SimpleBOWCorpus
#import pickle, os
import GPy
import os
from scipy.spatial.distance import euclidean as euc
from scipy.misc import comb
import itertools
#from sklearn.svm import SVC, NuSVC, LinearSVC
from Clustering import compute_best_ids

BLUE = '#6699cc'
GRAY = '#999999'

class Test(unittest.TestCase):
    def setUp(self):
        prefix = 'new_test.' #'new_test.'
        self.path = '/media/phan/BIGDATA/SMARTFONTS/'
        self.modes = ['outline'] #['skel', 'outline', 'cap']
        #gplvm 215.164274693 8.36073162737
        #lda 206.107707143 15.9314494026
        #retrieval 198.475533164 11.7168053509
        
        #loading testing data (input). (the  query font but converted to document form)
        self.fnames = {}
        self.orig_pWs = {}
        self.pWs = {}
        self.parts = {}
        self.W = {}
        #self.docs = {}
        
        for mode in self.modes:
            ds = np.load(join(self.path, prefix + mode + '.pWs.npz'))
            self.fnames[mode] = list(ds['fnames'])
            self.parts[mode] = ds['parts']
            self.W[mode] = list(ds['W'])
            #self.orig_pWs[mode] = ds['orig_pWs']
            self.orig_pWs[mode] = ds['pWs']
            self.pWs[mode] = ds['pWs']
            self.feats = ds['all_feats']
            
            #ds = np.load(join(self.path, mode + '_dict.npz'))
            #self.dmat = ds['mat']
            #self.docs[mode] = SimpleBOWCorpus(join(self.model_path, prefix +  mode + '.docword.fonts.txt'))
            
    def getIndices(self, chars, parts):
        '''
        get the indices of the parts that contain chars
        '''
        indices = []
        for px, p in enumerate(parts):
            for ch in chars:
                if ch in p:
                    indices.append(px)
                    break
                    
        return np.array(indices)
    
    def getKeyChars(self, n):
        data = np.load(join(self.path, 'groundtruth.npz'))
        chars = data['arr_1']
        dt = data['arr_0'].tolist()
        return chars[ np.argsort(dt['groundtruth'])[-n:] ]
        
    def testInference(self):
        Q = 7
        num_inducing = 20
        n_tests = 5 #2600
        train_ratio = 0.6
        #given_chars = list('GHV')
        
        masks = {}
        scores = {}
        methods = [
        #'clustering',
        'gplvm',
        'lda',
        'retrieval',
        
        'groundtruth'
        ] 
        
        '''
        lda 116.369433159 6.95117158056
        retrieval 109.925316668 5.10874086334
        gplvm 118.224453771 5.27200171975
        groundtruth 150.06414994 5.50762488551        
        '''
        #given_chars = []
        perms = []
        n_fonts = len(self.fnames[self.modes[0]])
        
        given_chars = self.getKeyChars(n_tests)
        #it = itertools.combinations(cm.capital_chars, 3)
        for i in range(n_tests):
            #perm = np.random.permutation(len(cm.capital_chars))
            #given_chars.append( cm.capital_chars[perm[:3]] )
            #given_chars.append(list('EOM'))
            #given_chars.append(it.next())
            perms.append( np.random.permutation(n_fonts) )
        
        for method in methods:
            scores[method] = []
            for tx in range(n_tests):
                print 'given chars', given_chars[tx]
                
                for mode in self.modes:
                    masks[mode] = np.zeros(len(self.W[mode]),dtype=bool)
                    for wx, w in enumerate(self.W[mode]):
                        remove = True
                        #for ch in given_chars[tx]:
                        if str(w)[0] in given_chars[tx] and  str(w)[4] in given_chars[tx]: #.find(ch) != -1:
                                remove = False
                                
                        if remove:
                            masks[mode][wx] = True
                    
                    
                    perm = perms[tx] #np.random.permutation(n_fonts)
                    n_train = int(n_fonts * train_ratio)
                    
                    train_ids = perm[:n_train]
                    test_ids = perm[n_train:]
                    
                    Y_train = np.copy(self.pWs[mode][train_ids])
                    Y_test = np.copy(self.pWs[mode][test_ids])
                    #Y_train = np.copy(self.orig_pWs[mode][train_ids])
                    #Y_test = np.copy(self.orig_pWs[mode][test_ids])
                    
                    n_parts = Y_train.shape[1]
                    Y_train = Y_train.reshape(-1, n_parts ** 2)
                    Y_test = Y_test.reshape(-1, n_parts ** 2)
                    
                    if method == 'groundtruth':
                        Y_full = Y_test
                        
                    elif method == 'clustering':
                        Y_test[:, masks[mode]] = np.nan
                        feats = []
                        for f in  self.feats[train_ids]:
                            #tmp = []
                            #for ch in f[26:52]:
                            #    tmp.append(ch)
                                
                            feats.append(np.concatenate(f[26:52]))
                        feats = np.array(feats)
                        bestmatches = compute_best_ids(Y_test, Y_train, feats, self.orig_pWs[mode][train_ids], dmat=self.dmat, parts = self.parts, n_neighbors=3)
                    
                    elif method == 'retrieval':
                        nmask = np.bitwise_not(masks[mode])
                        queries = Y_test[:, nmask]
                        
                        Y_full = []
                        for qx, query in enumerate(queries):
                            
                            dists =  [euc(query, item[nmask]) for item in Y_train]
                            best_id = np.argmin(dists)
                            Y_full.append(Y_train[best_id])
                        
                        fnames = np.array(self.fnames[mode])
                        print fnames[test_ids][qx], fnames[train_ids][best_id]
                    
                    
                    elif method == 'bgplvm':
                        
                        Y_test[:, masks[mode]] = np.nan
                        kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)   + GPy.kern.Bias(Q, np.exp(-2))
                        m = GPy.models.BayesianGPLVM(Y_train , Q, kernel=kernel, num_inducing=num_inducing)
                        m.optimize('bfgs', messages=0, max_iters=400, gtol=1e-4)
                        Y_full = []
                            
                        for ytest in Y_test:
                            X_star,_ = m.infer_newX(ytest[np.newaxis, :], optimize=True) #do_test_latents(Y[:2])
                            Y_full.append( m.predict(np.array(X_star.mean))[0] )
                            
                    elif method == 'gplvm':
                        
                        Y_test[:, masks[mode]] = np.nan
                        kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                        m = GPy.models.GPLVM(Y_train, Q, kernel=kernel)   
                        m.optimize('scg', messages=0,max_iters=400)
                        Y_full = []
                        #m.plot_latent(which_indices=(0,1))
                        #raw_input("press to continue")                            
                        for ytest in Y_test:
                            X_star,_ = m.infer_newX(ytest[np.newaxis, :], optimize=True) #do_test_latents(Y[:2])
                            Y_full.append( m.predict(np.array(X_star))[0] )                    
                        
                    elif method == 'lda':
                        Y_test[:, masks[mode]] = 0 #only keep the input words
                        prefix = "tmp."

                        id2word = dict([(i, w) for i, w in enumerate(self.W[mode])])

                        mm = gensim.matutils.Dense2Corpus(\
                        Y_train , \
                        #self.pWs[mode][train_ids].reshape(-1, n_parts ** 2) * 100, \
                        documents_columns=False)
                        
                        print 'training model (%s, %s)' % (prefix, mode)
                        lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=6, 
                                                          update_every=1, chunksize=10, passes=5)   

                        score = 0
                        Y_full = []
                        for test in Y_test:
                            test_corpus = gensim.matutils.Dense2Corpus(test[np.newaxis, :], documents_columns=False)
                            topic_weights = lda.inference(test_corpus)
                            Y_full.append( (lda.state.get_lambda() * topic_weights[0].transpose()).sum(axis=0) )
                    
                    if method != 'clustering':
                        #calculate the score
                        Y_full = np.array(Y_full)
                        Y_full = Y_full.reshape(-1, n_parts, n_parts)
                        ids = self.getIndices(given_chars[tx], self.parts[mode])
                        mask = np.zeros((n_parts, n_parts), dtype=bool)
                        mask[ids, :] = True
                        mask[ids, ids] = False
                        Y_full[:, np.bitwise_not(mask)] = 0 #only consider words that have at least 1 part from given_chars
                        bestmatches = np.argmax(Y_full, axis = 1)
                        
                    score = 0
                    for row in range(len(bestmatches)):
                        score += self.orig_pWs[mode][test_ids][row, range(n_parts), bestmatches[row] ].sum()
                        #W =  np.array(self.W[mode]).reshape(n_parts, n_parts)
                        #print W[range(n_parts), bestmatches[row]]
                        
                    scores[method] += [score]
                    
        from scipy.stats import f_oneway,ttest_rel,ttest_ind
        np.savez('.tmp/tmp_test9.npz', scores, given_chars)
        #print 'gplvm - lda', ttest_ind(scores['gplvm'], scores['lda'])
        #print 'gplvm - re', ttest_ind(scores['gplvm'], scores['retrieval'])
        #print 'lda - re', ttest_ind(scores['lda'], scores['retrieval'])
        #print 'clustering - re', ttest_ind(scores['clustering'], scores['retrieval'])
        for m in range(len(methods)):
            for n in range(m+1, len(methods)):
                print methods[m] + methods[n] , ttest_ind(scores[methods[m]], scores[methods[n]])
                
        for method in methods:
            print method, np.mean(scores[method]), np.std(scores[method])
            
if __name__ == '__main__':
    unittest.main()