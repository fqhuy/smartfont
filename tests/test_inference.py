# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 17:44:50 2015

@author: phan
"""
import unittest
import numpy as np
import gensim
from os.path import join
import Common as cm
#from LDA import SimpleBOWCorpus, get_values, StyleModel
import LDA
#from Synthesization import transfer_style_graph, stroke_estimation 
#import Synthesization as sz
#from descartes import PolygonPatch
#from shapely.geometry import asPolygon, Polygon, Point, asMultiPolygon, box
#from shapely.ops import cascaded_union
from matplotlib import pyplot
from matplotlib.colors import cnames
from scipy.spatial.distance import euclidean as euc
from scipy import signal
#import SkeletonRetrieval as sr
from fast.Distance import hausdorff
#from DataSource import SimpleBOWCorpus
#import pickle, os
import GPy
import os
from scipy.misc import comb
import itertools
#from sklearn.svm import SVC, NuSVC, LinearSVC
from Clustering import compute_best_ids
import DataSource__ as dsrc
import Feature as ft
from sklearn.mixture import GMM
from Regression import gmm_predict
from scipy.stats import f_oneway,ttest_rel,ttest_ind
from scipy.spatial.distance import wminkowski
from Common import alphas

BLUE = '#6699cc'
GRAY = '#999999'
#alphas = {'skel' : 0.005, 'outline' : 0.05, 'cap' : 0.00005}
weuc1 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[1.0] * (len(a1)/2), [0.05] * (len(a1)/2)]).T.flatten())
weuc2 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[0.1] * (len(a1)/2), [1.0] * (len(a1)/2)]).T.flatten())
dists = {'outline' : weuc1, 'cap' : ft.simple_hausdorff, 'skel' : weuc2}
            
class Test(unittest.TestCase):
    def setUp(self):
        prefix = 'full3.' #'full1.'
        self.path = '/media/phan/BIGDATA/SMARTFONTS/'
        self.modes = ['skel', 'outline', 'cap']
        #gplvm 215.164274693 8.36073162737
        #lda 206.107707143 15.9314494026
        #retrieval 198.475533164 11.7168053509
        
        #loading testing data (input). (the  query font but converted to document form)
        self.fnames = {}
        self.orig_pWs = {}
        self.pWs = {}
        self.parts = {}
        self.W = {}
        self.feats = {}
        self.chrange = np.arange(26, 52) #Capital characters
        #self.docs = {}
        
        
        for mode in self.modes:
            ds = np.load(join(self.path, prefix + mode + '.pWs.npz'))
            self.fnames[mode] = list(ds['fnames'])
            self.parts[mode] = ds['parts']
            self.W[mode] = list(ds['W'])
            #self.orig_pWs[mode] = ds['orig_pWs']
            #self.orig_pWs[mode] = ds['pWs']
            self.pWs[mode] = ds['pWs']
            self.pWs[mode] [np.isnan(self.pWs[mode])] = 0
            
            if mode == 'cap':
                self.pWs[mode] = np.e ** (-alphas[mode] * self.pWs[mode] ** 2)
            else:
                self.pWs[mode] = 1 - (self.pWs[mode].T / self.pWs[mode].max(axis=1)).T
                
            #self.pWs[mode] = np.e ** (-alphas[mode] * self.pWs[mode] ** 2)
            #self.pWs[mode] = np.e ** (-alphas[mode] * (self.pWs[mode].T - self.pWs[mode].mean(axis=1)).T ** 2)
            self.feats[mode] = list(ds['all_feats'])
            
            
            #ds = np.load(join(self.path, mode + '_dict.npz'))
            #self.dmat = ds['mat']
            #self.docs[mode] = SimpleBOWCorpus(join(self.model_path, prefix +  mode + '.docword.fonts.txt'))
            
    def getIndices(self, chars, parts):
        '''
        get the indices of the parts that contain chars
        '''
        indices = []
        for px, p in enumerate(parts):
            for ch in chars:
                if ch in p:
                    indices.append(px)
                    break
                    
        return np.array(indices)
    
    def getKeyChars(self, n):
        data = np.load(join(self.path, 'groundtruth.npz'))
        chars = data['arr_1']
        dt = data['arr_0'].tolist()
        return chars[ np.argsort(dt['groundtruth'])[-n:] ]
        
    def testInference(self):
        Q = 7
        num_inducing = 20
        n_tests = 40 #2600
        train_ratio = 0.6
        
        #given_chars = list('GHV')
        
        masks = {}
        
        methods = [
        #'clustering',
        #'lda',
        #'gmm',
        #'bgplvm',
        #'retrieval',
        #'appearance_retrieval',
        'groundtruth'
        ] 
        
        perms = []
        n_fonts = len(self.fnames[self.modes[0]])
        
        given_chars = self.getKeyChars(n_tests)
        print given_chars
        for i in range(n_tests):
            perms.append( np.random.permutation(n_fonts) )
        
        scores = {}    
        for mode in self.modes:
            scores[mode] = {}
            for method in methods:
                scores[mode][method] = []
                for tx in range(n_tests):
                    print 'given chars', given_chars[tx]
                    vocab = dsrc.get_feature_names(mode, self.chrange)
                    parts = dsrc.get_parts(mode, self.chrange)
                    masks[mode] = np.zeros(len(self.W[mode]),dtype=bool)
                    for wx, w in enumerate(self.W[mode]):
                        remove = True
                        if str(w)[0] in given_chars[tx] and  str(w)[4] in given_chars[tx]: #.find(ch) != -1:
                                remove = False
                                
                        if remove:
                            masks[mode][wx] = True
                    
                    
                    perm = perms[tx] #np.random.permutation(n_fonts)
                    n_train = int(n_fonts * train_ratio)
                    
                    train_ids = perm[:n_train]
                    test_ids = perm[n_train:]
                    
                    Y_train = np.copy(self.pWs[mode][train_ids])
                    Y_test = np.copy(self.pWs[mode][test_ids])
                    
                    n_tfonts = len(Y_test)
                    n_parts = len(parts)
                    #Y_train = Y_train.reshape(-1, n_parts ** 2)
                    #Y_test = Y_test.reshape(-1, n_parts ** 2)
                    
                    if method == 'groundtruth':
                        Y_full = Y_test
                        
                    elif method == 'clustering':
                        Y_test[:, masks[mode]] = np.nan
                        feats = []
                        for f in  self.feats[train_ids]:
                            #tmp = []
                            #for ch in f[26:52]:
                            #    tmp.append(ch)
                                
                            feats.append(np.concatenate(f[26:52]))
                        feats = np.array(feats)
                        bestmatches = compute_best_ids(Y_test, Y_train, feats, self.orig_pWs[mode][train_ids], dmat=self.dmat, parts = self.parts, n_neighbors=3)
                    
                    elif method == 'appearance_retrieval':
                        dists = {'outline' : euc, 'cap' : ft.simple_hausdorff, 'skel' : euc}
                        dist = dists[mode]
                        Y_full = []
                        for qx in test_ids:
                            scores_ = []
                            query = self.feats[mode][qx]
                            for fx in train_ids:
                                font = self.feats[mode][fx]
                                score = 0
                                for chx in range(len(font)):
                                    for px in range(len(font[chx])):
                                        if len(query[chx][px][0]) > 0:
                                            #try:
                                            if len(font[chx][px][0]) == 0:
                                                score += 1e8
                                                #print self.fnames[mode][fx]
                                            else:
                                                score += dist(font[chx][px][0], query[chx][px][0])
                                            #except:
                                            #    pass
                                            
                                scores_.append(score)
                            best_id = np.argmin(scores_)
                            
                            Y_full.append( Y_train[best_id] )
                        #fnames = np.array(self.fnames[mode])
                        #print fnames[test_ids][qx], fnames[train_ids][best_id]                            
                    elif method == 'retrieval':
                        nmask = np.bitwise_not(masks[mode])
                        queries = Y_test[:, nmask]
                        
                        Y_full = []
                        for qx, query in enumerate(queries):
                            try:
                                dists =  [euc(query, item[nmask]) for item in Y_train]
                            except:
                                pass
                            best_id = np.argmin(dists)
                            Y_full.append(Y_train[best_id])
                        
                        #fnames = np.array(self.fnames[mode])
                        #print fnames[test_ids][qx], fnames[train_ids][best_id]
                    
                    
                    elif method == 'bgplvm':
                        
                        Y_test[:, masks[mode]] = np.nan
                        kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)   + GPy.kern.Bias(Q, np.exp(-2))
                        m = GPy.models.BayesianGPLVM(Y_train , Q, kernel=kernel, num_inducing=num_inducing)
                        m.optimize('bfgs', messages=1, max_iters=400, gtol=1e-4)
                        Y_full = []
                            
                        for ytest in Y_test:
                            X_star,_ = m.infer_newX(ytest[np.newaxis, :], optimize=True) #do_test_latents(Y[:2])
                            Y_full.append( m.predict(np.array(X_star.mean))[0][0] )
                            
                    elif method == 'gplvm':
                        
                        Y_test[:, masks[mode]] = np.nan
                        kernel = GPy.kern.RBF(Q, ARD=True) + GPy.kern.Bias(Q)
                        m = GPy.models.GPLVM(Y_train, Q, kernel=kernel)   
                        m.optimize('scg', messages=0,max_iters=400)
                        Y_full = []
                        #m.plot_latent(which_indices=(0,1))
                        #raw_input("press to continue")                            
                        for ytest in Y_test:
                            X_star,_ = m.infer_newX(ytest[np.newaxis, :], optimize=True) #do_test_latents(Y[:2])
                            Y_full.append( m.predict(np.array(X_star))[0] )                    
                        
                    elif method == 'lda':
                        Y_test[:, masks[mode]] = 0 #only keep the input words
                        prefix = "tmp."

                        id2word = dict([(i, w) for i, w in enumerate(self.W[mode])])

                        mm = gensim.matutils.Dense2Corpus(\
                        Y_train  * 100, \
                        #self.pWs[mode][train_ids].reshape(-1, n_parts ** 2) * 100, \
                        documents_columns=False)
                        
                        print 'training model (%s, %s)' % (prefix, mode)
                        lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=6, 
                                                          update_every=1, chunksize=5, passes=5,
                                                          alpha='auto',decay=0.2)   

                        score = 0
                        Y_full = []
                        Y_test = Y_test * 100
                        for test in Y_test:
                            test_corpus = gensim.matutils.Dense2Corpus( test[np.newaxis, :], documents_columns=False)
                            topic_weights = lda.inference(test_corpus)
                            Y_full.append( (lda.state.get_lambda() * topic_weights[0].transpose()).sum(axis=0) )
                            
                    if method == 'gmm':
                        Y_test[:, masks[mode]] = np.nan
                        Y_full = []
                        for ytest in Y_test:                        
                            gmm = GMM(n_components = 8, covariance_type='diag')
                            #gmm = find_best_params_gmm(Xtrain, np.arange(4, 30, 2), ['full'], criterion='aic') #GMM(n_components = 45, covariance_type='spherical')
                            gmm.fit(Y_train)
                            yfull = gmm_predict(ytest[np.newaxis, :], gmm)
                            Y_full.append(yfull)
                            
                    #calculate the score
                    Y_full = np.array(Y_full)
                                       
                    missing_chars = [ch for ch in np.array(cm.all_chars)[self.chrange] if ch not in given_chars[tx]]
                    bestmatches = np.zeros((n_tfonts, n_parts), dtype=int)
                    ids = self.getIndices(missing_chars, parts)
                    missing_parts = [parts[idd] for idd in ids]
                        
                    for mpt in missing_parts:
                        candidates = []
                        for vx, v in enumerate(vocab):
                            if (v[0] in given_chars[tx] or v[4] in given_chars[tx]) and mpt in v:
                                candidates.append(vx)
                                
                        selected_vocab = [vocab[v] for v in candidates]        
                        for yx, yfull in enumerate(Y_full):
                            #if method is 'lda':
                            bestword = selected_vocab[yfull[candidates].argmax()]
                            #else: 
                            #bestword = selected_vocab[yfull[candidates].argmin()]
                                
                            #act, ch1, pt1, ch2, pt2 = dsrc.word2part(bestword)
                            bestmatches[yx, parts.index(mpt)] =  vocab.index(bestword)
                            
                    score = 0
                    for row in range(len(bestmatches)):
                        for mpt in missing_parts:
                            score += self.pWs[mode][test_ids][row, bestmatches[row, parts.index(mpt)]]
                    
                    scores[mode][method] += [score]
                    
        m = ''
        for method in methods: m+=method
        m += '_'
        for mode in self.modes: m+=mode
        np.savez(join(self.path, 'results', m + '.npz'), scores, given_chars)

        for mode in self.modes:
            print '------------------------------%s-----------------------------------' % mode
            for m in range(len(methods)):
                for n in range(m+1, len(methods)):
                    print methods[m] + methods[n] , ttest_ind(scores[mode][methods[m]], scores[mode][methods[n]])
            print 
            for method in methods:
                print method, np.mean(scores[mode][method]), np.std(scores[mode][method])
            
if __name__ == '__main__':
    unittest.main()