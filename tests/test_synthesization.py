'''
Created on 31 Jul, 2014

@author: phan
'''
import unittest
import numpy as np
import gensim
from os.path import join
import Common as cm
from LDA import SimpleBOWCorpus, get_values, StyleModel
import LDA
from Synthesization import transfer_style_graph, stroke_estimation 
import Synthesization as sz
from descartes import PolygonPatch
from shapely.geometry import asPolygon, Polygon, Point, asMultiPolygon, box
from shapely.ops import cascaded_union
from matplotlib import pyplot
from matplotlib.colors import cnames
from scipy.spatial.distance import euclidean as euc
from scipy import signal
import SkeletonRetrieval as sr
from fast.Distance import hausdorff
import pickle, os

BLUE = '#6699cc'
GRAY = '#999999'
class Test(unittest.TestCase):
    def setUp(self):
        prefix = '' #'lower.'
        self.fname = 'AdventPro-Light.ttf.npz' #'Alegreya-Regular.ttf.npz' #'EG1.1.ttf.npz' #'CherrySwash-Regular.ttf.npz' #'EG.ttf.npz' ##str('AbrilFatface-Regular.ttf.npz')
        self.path = '/media/phan/BIGDATA/SMARTFONTS/'
        self.model_path = self.path #+ 'models/4/'
        self.modes = ['skel', 'outline', 'cap']
        self.use_external_brushes = True
        self.given_chars = 'ICV' #'A'
        self.test_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' #'abcdefghijklmnopqrstuvwxyz' #'ABCDEFGHIJKLMNOPQRSTUVWXYZ' #NOPQRSTUVWXYZ' #cm.capital_chars #'EUROGRAPHICS' #"UROAPHICS" #cm.capital_chars
        
        #prepare the modesl for cap, stroke (outline), and skeleton
        self.models = {}
        for mode in self.modes:
            self.models[mode] = StyleModel(self.model_path, mode, prefix)
        
        #read the specification of glyphs    
        tmpdata = np.load(join(self.model_path, 'word_specs.npz'))
        self.mains = tmpdata['mains']
        self.specs = tmpdata['specs']
        
        #Load the querying font (containing a few characters only)
        self.anns = {}
        self.points = {}
        self.segments = {}
        data = np.load(join(self.path,'data', self.fname))
        self.graphs = data['graphs']
        self.outlines = {}
        
        for ch in cm.all_chars:
            self.points[ch] = data['points'][cm.all_chars.index(ch)]
            self.segments[ch] = data['segments'][cm.all_chars.index(ch)]
            self.anns[ch] = data['annotations'][cm.all_chars.index(ch)]
            anns = []
            graph = self.graphs[cm.all_chars.index(ch)]
            for ann in self.anns[ch]:
                ann = np.array([graph.node[a]['pos'] for a in ann])
                anns.append(ann)
            self.anns[ch] = anns
                
            self.outlines[ch] = data['outlines'][cm.all_chars.index(ch)]
        
        #Find and load a reference font.    
        #self.ref_fname, self.bestscale,_ = sr.find_similar_fonts(self.path, self.fname, mode='u',chars=list(self.given_chars))
        #self.ref_fname, self.bestscale = 'Cardo-Regular.ttf.npz', np.array([1.2, 1.3])
        #self.ref_fname, self.bestscale = 'Asap-Bold.ttf.npz', 1.0  
        #self.ref_fname, self.bestscale = 'Adamina-Regular.ttf.npz', 0.9
        #self.ref_fname, self.bestscale = 'CutiveMono-Regular.ttf.npz', np.array([1.4,1.1])
        #'Arvo-Bold.ttf.npz', 1.4 #'CutiveMono-Regular.ttf.npz', 1.0  
        #'Arvo-Bold.ttf.npz', 1.4 #self.fname, 1.0 #'Asap-Bold.ttf.npz', 1.0 #
        self.ref_fname, self.bestscale = self.fname, 1.0
        
        print self.ref_fname, self.bestscale
        
        data = np.load(join(self.path,'data', self.ref_fname))
        self.ref_graphs = data['graphs']
        self.ref_anns = {} #data['annotations']
        for ch in cm.all_chars:
            self.ref_anns[ch] = data['annotations'][cm.all_chars.index(ch)]
            graph = self.ref_graphs[cm.all_chars.index(ch)]
            anns_ = self.ref_anns[ch]
            ref_anns = []
            for ann in anns_:
                ann = np.array([graph.node[a]['pos'] for a in ann]) * self.bestscale
                ref_anns.append(ann)
                
            self.ref_anns[ch] = ref_anns
                        
        #loading testing data (input). (the  query font but converted to document form)
        self.fnames = {}
        self.docs = {}
        prefix = 'test_pinkfloyd.' #'lower.' #'test.' #test_EG1.1.
        for mode in self.modes:
            ds = np.load(join(self.model_path, prefix + mode + '.pWs.npz'))
            self.fnames[mode] = list(ds['fnames'])
            self.docs[mode] = SimpleBOWCorpus(join(self.model_path, prefix +  mode + '.docword.fonts.txt'))

    def tearDown(self):
        pass
      

    def testSynthesization(self):
        #Find transfer rules
        bestwords = {}
        probs = {}
        for mode in self.modes:
            given_font = self.fnames[mode].index(self.fname) * 20 #+ 1
            bestwords[mode], probs[mode] = self.models[mode].stylize(self.docs[mode][given_font], self.given_chars)
        
        #bestwords['skel'] = bestwords['stroke']
        #probs['skel'] = probs['cap'] 
        print bestwords
        #Add dummy words for given chars
        for ch in self.given_chars:
            testch = cm.all_chars.index(ch)
            for part in range(self.mains[testch]):
                bestwords['outline'].append((ch + str(part).zfill(2)) + ':' + (ch + str(part).zfill(2)))
                probs['outline'].append(0.5)
            for part in range(self.mains[testch], self.specs[testch]):
                bestwords['cap'].append(((ch + str(part).zfill(2)) + ':' + (ch + str(part).zfill(2))))
                probs['cap'].append(0.5)
            for part in range(self.specs[testch]):
                bestwords['skel'].append((ch + str(part).zfill(2)) + ':' + (ch + str(part).zfill(2)))
                probs['skel'].append(0.5)  
                   
        fig = pyplot.figure(1)
        testchars = list(self.test_chars) #list('ERO')
        count = 1

        nrows = len(testchars) / 8 + 1
        ncols = min(len(testchars), 8)
        
        #for each character 
        for tx, testch in enumerate(testchars):
            print testch
            ax = fig.add_subplot(nrows, ncols, count)
            count += 1
            '''
            if testch in self.given_chars:
                results = [self.outlines[testch]]
               
            else:
            '''
            #prepare_skeletons works on multiple characters, that's why we need to wrap [anns]    
            new_ref_anns, anchors = sz.prepare_skeletons([self.ref_anns[testch]], smoothing=True)
            new_ref_anns[0] = list(new_ref_anns[0])
            #new_ref_anns = [self.ref_anns[testch]]
            methods = {'skel': 'skel', 'outline':'stretch', 'cap' : 'ICP'}
            results = [[] for _ in range(len(new_ref_anns[0]))]
            
            for mode in self.modes:
                #take out the related words only
                ws = [(bestwords[mode][w], probs[mode][w]) for w in range(len(bestwords[mode])) if bestwords[mode][w].startswith(testch)]
                print mode, ws
                for w, prob in ws: #for each word, transfer the style according to the rules
                    #H0 <--- T0
                    #ch1_part1:ch2_part2
                    ch1, ch2, part1, part2 = get_values(w)
                    print w
                    point = self.points[ch2][part2]
                    ref_ann = new_ref_anns[0][part1] 
                    method = methods[mode] #select the right method for synthesization
                    smooth = False
                    
                    #Allows taking brushes from svg.
                    if self.use_external_brushes:
                        meta = w[4:]
                    else:
                        meta = None
                        
                    #difference transfer methods.    
                    if method == 'ICP':
                        result = transfer_style_graph(self.anns[ch2][part2], ref_ann, point, method, smooth , meta=meta)
                        results[part1] = result #np.array(result.exterior)
                        
                    elif method == 'skel':
                        #continue
                        #only transfer when the prob is greater than some threshold
                        result = transfer_style_graph(self.anns[ch2][part2], ref_ann, None, method, smooth)
                        #ax.scatter(result[:,0], result[:,1],marker='o',c='red')
                        #d = hausdorff(result, ref_ann, mode=0)
                        #print d
                        '''
                        if d < 400:
                            new_ref_anns[0].pop(part1) # = None #result
                            new_ref_anns[0].insert( part1, result)
                        '''
                        
                    elif method == 'stretch':
                        result = transfer_style_graph(point[0:2], ref_ann, point[2:4], method, smooth, meta=meta)
                        results[part1] = result
                    
                    #ax.scatter(ref_ann[:,0], ref_ann[:,1],marker='o',c='blue')
                    #ax.scatter(point[:,0], ann[:,1],marker='o',c='blue')
                    #ax.scatter(newanns[part1][:,0], newanns[part1][:,1], marker='o',c=cm.color_names[part1])
            
            #for each part of the character, find the intersection (anchors) with other parts and wield two parts together
            polies = []
            nmain = self.mains[cm.all_chars.index(testch, )]
            #new_ref_anns, anchors = sz.prepare_skeletons([self.ref_anns[testch]], smoothing=True)
            #new_ref_anns[0] = list(new_ref_anns[0])
                  
            if len(results) == 1:
                truth = asPolygon(results[0][0])
                for ply in results[0][1:]:
                    this = asPolygon(ply)
                    if truth.contains(this):
                        truth = truth.difference(this)
                    else:
                        truth = truth.union(this)
                outputs = (truth, 'red')
            else:
                #for stx in range(len(results)):
                #    if stx >= nmain:
                #        ply = asPolygon(results[stx]).buffer(0)
                #        patch = PolygonPatch(ply, fc=cnames['red'], ec=cnames['red'], alpha=0.5, zorder=1)
                #        ax.add_patch(patch)
                        
                tabu = []
                for anchor in anchors[0]:
                    part1, apoint1, jtype1, part2, apoint2, jtype2 = anchor
                    #Avoid repetitive anchors...
                    #if (part1, part2, jtype1) in tabu or (part2, part1,jtype2) in tabu:
                    if (part1, part2) in tabu: #(part1, jtype1) in tabu or (part2, jtype2) in tabu:
                        continue
                    
                    tabu.extend([(part1, part2), (part2, part1)]) #tabu.append((part1, jtype1)) #((part1, part2,jtype1))
                    #tabu.append((part2, jtype2)) #((part2, part1,jtype2))
                    
                    if part1 < nmain or part2 < nmain:
                        print part1, part2, '|' ,jtype1,  jtype2
                        hull = sz.join_parts([results[part1], results[part2]], \
                                             [apoint1, apoint2],[jtype1, jtype2], 'pointy',smooth=False)
                        
                        if hull.is_valid:
                            polies.append(hull)
                
                
                combined_outline = cascaded_union(polies)
                outputs = (combined_outline, 'green')
            
            glyph, color = outputs    
            #glyph = glyph.buffer(10).buffer(-10).intersection(box(0.0, 0.0, 2200.0, 2200.0))
            glyph = glyph.buffer(20).buffer(-20)
            if glyph.geom_type == 'MultiPolygon':
                for poly in glyph:
                    patch = PolygonPatch(poly, fc=cnames[color], ec=cnames[color], alpha=0.5, zorder=1)
                    ax.add_patch(patch)
            elif glyph.geom_type == 'Polygon':
                patch = PolygonPatch(glyph, fc=cnames[color], ec=cnames[color], alpha=0.5, zorder=1)
                ax.add_patch(patch)
            
            sz.save_polygons_as_svg(glyph, join('.tmp', testch + '.svg'))            
            ax.autoscale()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            #ax.set_xlim([0, 3000])
            #ax.set_ylim([0, 2200])
            '''
            #ground truth characters
            ax = fig.add_subplot(122)
            truth = asPolygon(self.outlines[testchars][0])
            for ply in self.outlines[testchars][1:]:
                this = asPolygon(ply)
                if truth.contains(this):
                    truth = truth.difference(this)
                else:
                    #truth = truth.union(this)
                    ax.add_patch(PolygonPatch(this, fc=BLUE, ec=BLUE, fill=True, alpha=0.5, zorder=1))
            
            ax.add_patch(PolygonPatch(truth, fc=BLUE, ec=BLUE, fill=True, alpha=0.5, zorder=1))
            ax.autoscale_view()
            '''
            
        #pyplot.savefig(join(self.path, self.test_chars + '.png'),dpi='400',transparent=True)
        pyplot.show()    

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSynthesization']
    unittest.main()