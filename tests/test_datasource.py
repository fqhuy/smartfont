'''
Created on 20 Sep, 2014

@author: phan
'''
import unittest
import DataSource as ds
from os.path import join
import numpy as np 

class Test(unittest.TestCase):


    def setUp(self):
        self.path = '/media/phan/BIGDATA/SMARTFONTS/'
        self.sbc = ds.SimpleBOWCorpus(join(self.path, 'cap.docword.fonts.txt'))
        self.data = np.load(join(self.path, 'cap.pWs.npz'))
        self.fnames = list(self.data['fnames'])
        self.docidx = self.fnames.index('CherrySwash-Regular.ttf.npz', ) * 20 + 1

    def tearDown(self):
        pass


    def testSimpleBOWCorpus(self):
        self.sbc[self.docidx]


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()