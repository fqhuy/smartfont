'''
Created on 1 Sep, 2014

@author: phan
'''
import unittest
import Segmentation as seg
import numpy as np
import Common as cm
import SkeletonRetrieval as sr

from matplotlib import pyplot as plt
from os.path import join
from Graph import scale

class Test(unittest.TestCase):


    def setUp(self):
        #self.fname1 = 'Actor-Regular.ttf.npz'
        self.fname1 = 'Enriqueta-Regular.ttf.npz'
        
        self.fname2, self.bestscale = 'Amethysta-Regular.ttf.npz', 1.0
        #self.fname2 = 'ArchitectsDaughter.ttf.npz'
        self.path = '/media/phan/BIGDATA/SMARTFONTS/data'
        #self.fname2, self.bestscale,_ = sr.find_similar_fonts(self.path[:-5], self.fname1, mode='l')
        #self.templates = sr.find_similar_fonts(self.path[:-5], self.fname1) 
        
        self.data1 = np.load(join(self.path, self.fname1))
        self.data2 = np.load(join(self.path, self.fname2))
    
    def tearDown(self):
        pass


    def testSegmentationGraphAuto(self):
        print 'matching fonts: %s - %s - %f' % (self.fname1, self.fname2, self.bestscale)
        chars = 'Q'
        fig = plt.figure(2)
        idd = 0
        for ch in chars:
            idd += 1
            ax1 = fig.add_subplot(100 + len(chars) * 10 + idd)
            chx = cm.all_chars.index(ch, )
            graph = self.data1['graphs'][chx]
            ref_graph = scale(self.data2['graphs'][chx], self.bestscale)
            
            ref_anns = self.data2['annotations'][chx]
            #ref_graph = self.templates[chx][1]
            #ref_anns = self.templates[chx][2]
            anns = seg.segment_graph_auto(graph, ref_graph, ref_anns)
            
            for annx, ann in enumerate(anns):
                #if annx == 0:
                #    continue
                
                pos1 = np.array([graph.node[a]['pos'].tolist() for a in ann if a in graph.nodes()])
                #pos2 = np.array([ref_graph.node[a]['pos'].tolist() for a in ref_anns[annx] if a in ref_graph.nodes()])
                if len(pos1) > 0:
                    ax1.scatter(pos1[:,0], pos1[:, 1],c=cm.color_names[annx], marker='o')
                    #ax1.scatter(pos2[:,0], pos2[:, 1],c='b', marker='o')
                
        ax1.autoscale()    
        plt.show()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()