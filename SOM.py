# -*- coding: utf-8 -*-
"""
Created on Sat Jul 12 22:26:04 2014

@author: phan
"""
import numpy as np
from scipy.spatial.distance import euclidean as euc
from cgal.CGAL import extract_skeleton
import cgal.CGAL as cgal
from matplotlib import pyplot as plt
from os.path import join
from Common import all_chars
import networkx as nx
from shapely.geometry import asLineString, Point
import sys
import copy, math
from Geometry import ave_tangent, Angle, angle_between, ave_normal
max_int = sys.maxint

class SOM:
    def __init__(self, alpha, beta, r, dist=euc, rf=5):
        self.alpha = alpha
        self.beta = beta
        self.r = r
        self.rf = rf
        self.dist = dist
    
    def distance(self, v, w, V, W):
        vnbs = nx.neighbors(V, v) + [v]
        wnbs = nx.neighbors(W, w) + [w]
        
        cvec = V.node[v]['pos'] - W.node[w]['pos']
        if np.linalg.norm(cvec) > 400:
            return max_int
        
        return np.linalg.norm(cvec)
        
        total_length = np.linalg.norm(cvec) 
        vat = ave_normal([V.node[v]['pos'] for v in vnbs], Angle)
        wat = ave_normal([W.node[w]['pos'] for w in wnbs], Angle)
        total_length *= np.e**math.atan(abs((vat[1]/vat[0] - wat[1]/wat[0]) / (1 + (vat[1]/vat[0])*(wat[1]/wat[0])))) #np.e**(abs(angle_between(wat , vat)))
        return total_length
        
        taken = []
        for nv in vnbs:
            nw_closest = sorted(zip([euc(V.node[nv]['pos'], W.node[nw]['pos']) for nw in wnbs \
            if not nw in taken], range(len(wnbs))), key = lambda x: x[0])[0][1]
            taken.append(nw_closest)
                        
            vec = V.node[nv]['pos'] - W.node[wnbs[nw_closest]]['pos']
            relaxation_length = np.linalg.norm(cvec - vec)
            total_length += relaxation_length
            
        return total_length
        #return self.dist(v, w)
    
    def fit(self, X, Y):
        V = X.copy()
        W = Y.copy()
        count = 0
        while True:
            print 'iter: ', count, 'r', self.alpha
            self.update(W, V)
            self.update_params()
            self.update(V, W)
            self.update_params()
            if self.r < self.rf:
                break
            count += 1
            
        re = []
        for v, vd in V.nodes_iter(data=True):
            mn = max_int
            mnx = W.nodes()[0]
            for w, wd in W.nodes_iter(data=True):
                d = self.distance(v, w, V, W)
                #if d > 40:
                #    continue
                    #self.r:
                if mn > d:
                    mn = d
                    mnx = w
            if mn != max_int:
                re.append((v, mnx))
                    #break
        nx.draw(V,pos=dict([(n, nd['pos']) for n, nd in X.nodes_iter(data=True)]),node_color='blue', alpha=0.3, font_size=8)
        nx.draw(W,pos=dict([(n, nd['pos']) for n, nd in Y.nodes_iter(data=True)]),node_color='green', alpha=0.3, font_size=8)
        
        g2 = nx.Graph()    
        for v, w in re:
            g2.add_edge('v.' + v, 'w.' + str(w))
            g2.node['v.' + v]['pos'] = V.node[v]['pos']
            g2.node['w.' + str(w)]['pos'] = W.node[w]['pos']
    
        nx.draw(g2,pos=dict([(n, nd['pos']) for n, nd in g2.nodes_iter(data=True)]),node_color='yellow', node_size=50, font_size=8)        
        #plt.show()        
        return re
                
    def update(self, W, V):
        for v, vd in V.nodes_iter(data=True):
            wc = sorted(zip([self.distance(v, w, V, W) for w in W.nodes_iter()], \
            W.nodes()), key = lambda x: x[0])[0][1]
            #wc = W.node[wc]
            
            '''
            for j in range(len(W)):
                if euc(W.node[j]['pos'], wc['pos']) < self.r:
                    alpha_j = self.alpha
                else:
                    alpha_j = 0
                
                if alpha_j != 0:
                    delta_wj = alpha_j * (v - W[j])
                    W[j] += delta_wj
            '''
            nbs = W.neighbors(wc)
            for n in nbs:
                delta_wj = self.alpha * (vd['pos'] - W.node[n]['pos']) #W.node[wc]['pos']) #
                W.node[n]['pos'] += delta_wj
                
            
    def update_params(self):
        self.r *= self.beta
        self.alpha /= self.beta
        
def feature_vector(px, annotation, outline=None):
    '''
    calculate the feature vector of a single point
    '''
    #contour = asLineString(outline)
    #nbs = neighbours(px, annotation)
    #t = ave_tangent(nbs)
    pos = annotation[px]
    '''
    n_p0 = ave_normal(nbs, geoa)
    
    vec1 = n_p0 * length  + p[0]
    vec2 = -n_p0 * length  + p[0]

    p[1] = np.array(LineString([p[0], vec1]).intersection(contour))
    p[2] = np.array(LineString([p[0], vec2]).intersection(contour))
    0
    valid = True
    d = 0
    for i in [1,2]:
        if len(p[i].shape) == 2:
            p[i] = sorted(list(p[i]), key=lambda x: Point(x).distance(Point(p[0])))[0]
        elif len(p[i]) == 0:
            valid = False
    if valid:
        d = np.linalg.norm(p[2] - p[1])
        
    '''    
    #features = [pos[0], pos[1], t]
    return None
        
if __name__ == '__main__':
    #import networkx as nx
    from Geometry import simplify, sample_polyline
    '''
    from matplotlib import pyplot as plt
    n_points = 100
    X = np.random.rand(n_points, 2) * n_points
    Y = np.zeros((n_points, 2),dtype='float64')
    Y[:, 0] = np.array([np.random.normal(x, 2) for x in X[:,0]])
    Y[:, 1] = np.array([np.random.normal(x, 2) for x in X[:,1]])

    som = SOM(alpha=0.05, beta=0.98,  r=10, rf=2)
    V, W = som.fit(X, Y)
    
    print np.abs(X - Y)
    
    plt.scatter(V[:, 0], V[:, 1], marker='o')
    plt.scatter(W[:, 0], W[:, 1], marker='x')
    plt.xlim(0,n_points)
    plt.ylim(0,n_points)
    plt.show()
    print np.abs(V - W)
    '''
    ch = all_chars.index('B')
    path = '/media/phan/BIGDATA/SMARTFONTS/data'
    #outlines = np.load(join(path, 'Aclonica.ttf.npz'))['outlines']
    #outlines1 = np.load(join(path, 'Amiri-Bold.ttf.npz'))['outlines']
    #anns = np.load(join(path, 'Amiri-Bold.ttf.npz'))['annotations']
    g1 = np.load(join(path, 'Amiri-Bold.ttf.npz'))['graphs'][ch] #.tolist()
    g0 = np.load(join(path, 'Alice-Regular.ttf.npz'))['graphs'][ch] #.tolist()

    #g0 = extract_skeleton(outlines[ch])
    for node, data in g0.nodes_iter(data=True):
        if data['mode'] != cgal.SKELETON:
            g0.remove_node(node)
            
    #g1 = extract_skeleton(outlines[ch])
    for node, data in g1.nodes_iter(data=True):
        if data['mode'] != cgal.SKELETON:
            g1.remove_node(node)            
    '''
    ann = anns[ch]
    g1 = nx.Graph()
    idd = 0
    _name = lambda x, y: str(x) + '.' + str(y)
    for part in ann:
        points = part #sample_polyline(part, step=50)
        nodelist = [(_name(idd, px), dict(pos=p)) for px, p in enumerate(points)]
        g1.add_nodes_from(nodelist)
        g1.add_path([_name(idd, px) for px in range(len(points))])
        idd += 1
    '''
    som = SOM(alpha=0.0, beta=0.95,  r=20, rf=19)
    matches = som.fit(g1, g0)

    plt.show()
