#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')
sys.path.append('/media/phan/80ae5e79-4620-48ec-b0fc-05bf33feaa62/phan/workspace/coolfont/coolfont')

# We will use the inkex module with the predefined Effect base class.
import inkex
import os
import re
# The simplestyle module provides functions for style parsing.
from simplestyle import *
import cubicsuperpath, simplepath, cspsubdiv
from shapely.geometry import *
from shapely.ops import cascaded_union
from shapely.affinity import translate, rotate
from coolfont.cgal import CGAL
from os.path import join
import numpy as np
import networkx as nx
from scipy.spatial.distance import euclidean
from coolfont.fast import Distance
from coolfont import Segmentation
from coolfont import DataSource
from coolfont import DataSource__ as dsrc
from coolfont import Common as cm
from coolfont import Synthesization as sz
from coolfont import Visualization as vz
from coolfont import Geometry as geo
import GPy
from copy import deepcopy
from ast import literal_eval
import gensim
import simplestyle, simpletransform
import matplotlib as mlt
from coolfont import Feature as ft
from scipy.spatial.distance import euclidean, wminkowski
from StringIO import StringIO
from coolfont.Common import alphas
# import matplotlib as mlt
# alphas = {'skel' : 0.005, 'outline' : 0.0005, 'cap' : 0.00005}

log = open('/tmp/inkscape_font_editor_log.txt', 'wt')

def euc(arr1, arr2, w=None):
    if np.isnan(arr1).any() or np.isnan(arr2).any():
        return 1e-8
        
    #if w is None:    
    return euclidean(arr1, arr2)
    #else:
    #    return wminkowski(arr1, arr2, w=w, p=2)

def create_color_list(N):
    try:
        import MyColorList
        return MyColorList.hexlist[: min(N, len(MyColorList.hexlist)-1)]
        #cl = np.array(MyColorList.colist)[:N]
        #hsv = mlt.colors.hsv_to_rgb(cl[np.newaxis, ...])[0]
        #return [mlt.colors.rgb2hex(c) for c in hsv]
    except:
        H = np.linspace(0, 1.0, 20)
        S = np.linspace(0.3, 0.7, 20)
        V = np.linspace(0.2, 0.8, 20)
        cl = []
        for  h in H:
            for s in S:
                for v in V:
                    cl.append([h,s,v])
        
        cl = np.array(cl)[np.random.permutation(len(cl))]
        f = open('MyColorList.py', 'w')
        f.writelines('colist=' + str(cl.tolist()))

        
        hsv =mlt.colors.hsv_to_rgb(cl[np.newaxis, ...])[0]
        hexc = [mlt.colors.rgb2hex(c) for c in hsv]
        
        f.writelines('\nhexlist=' + str(hexc) )
        f.close()
        return hexc
        
        
    #colist = mlt.cm.Set2(np.linspace(0, 1.0, len(cm.capital_chars) * 10))

colist = create_color_list(len(cm.capital_chars) * 10)

class FontEditor(inkex.Effect):
    """
    Example Inkscape effect extension.
    Creates a new layer with a "Hello World!" text centered in the middle of the document.
    """
    def __init__(self):
        """
        Constructor.
        Defines the "--what" option of a script.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define string option "--what" with "-w" shortcut and default value "World".
        self.OptionParser.add_option('-i', '--init_chars', action = 'store',
          type = 'string', dest = 'init_chars', default = 'A',
          help = 'What would you like to greet?')

        #self.options.flat = 10.0
        self.OptionParser.add_option("-f", "--flatness",
                        action="store", type="float", 
                        dest="flat", default=10.0,
                        help="Minimum flatness of the subdivided curves")     
                        
        self.OptionParser.add_option("-r", "--retrain",
                        action="store", type="inkbool", 
                        dest="retrain", default=False,
                        help="retrain")    
                         
        self.OptionParser.add_option("-t", "--thinning",
                        action="store", type="float", 
                        dest="thinning", default=0.0,
                        help="Minimum flatness of the subdivided curves")   
                        
        self.OptionParser.add_option("-z", "--trim",
                        action="store", type="int", 
                        dest="trim", default=1,
                        help="Trimming")   
                        
        self.OptionParser.add_option("-s", "--skeleton_thresh",
                        action="store", type="float", 
                        dest="skeleton_thresh", default=0.5,
                        help="skeleton_thresh")
                        
        self.OptionParser.add_option("-e", "--merge_extend",
                        action="store", type="float", 
                        dest="merge_extend", default=200,
                        help="merge_extend")   
                        
        self.OptionParser.add_option("-o", "--merge_smooth",
                        action="store", type="float", 
                        dest="merge_smooth", default=0.0,
                        help="merge_smooth")  
                         
        self.OptionParser.add_option("-m", "--mode",
                        action="store", type="string", 
                        dest="mode", default='synthesize', #synthesize #snap #merge
                        help="action")   
        
        self.OptionParser.add_option("-c", "--chset", 
                                     action="store", type="string", 
                                     dest="chset", default="upper")
                                     
        self.OptionParser.add_option("-d", "--method", 
                                     action="store", type="string", 
                                     dest="method", default="bgplvm")    #bgplvm retrieval feat_retrieval
                                     
        self.ns = {'svg':'http://www.w3.org/2000/svg'}
        
    def getIndices(self, chars, parts):
        indices = []
        for px, p in enumerate(parts):
            for ch in chars:
                if ch in p:
                    indices.append(px)
                    break
                    
        return np.array(indices)   
        
    def addPolyElement(self, poly, parent,  idd, offset=None, style='', replace=False):
        '''
        add a polygon to a parent_element
        '''
        log.write("adding poly elements %d \n".format(idd))
        if offset is None:
            offset = np.array([0,0])
            
        poly = np.array(poly) - offset
        polydata = zip(['M'] + ['L'] * (len(poly) - 1), [list(point) for point in poly])
        
        segment1 = parent.find("./svg:path[@id='%s']" % idd, namespaces=self.ns)
        if segment1 is None or replace:
            segment1 = inkex.etree.SubElement(parent, 'path')
        elif style =='':
            style = segment1.get('style')
            
        segment1.set('d', simplepath.formatPath(polydata))
        segment1.set('id', idd)
        segment1.set('style', style)
        return segment1
        #segment_group.append(segment1)
        
    def formatPath(self, points):
        d = zip(['M'] + ['L'] * (len(points) - 1), [list(point) for point in points])
        return simplepath.formatPath(d) + ' Z '
        
    def getIdd(self, ch, part, posfix=''):
        return ch + str(part).zfill(2) + posfix
    
    def loadData(self):
        path = join(os.getcwd(), "data")
        self.modes = ['skel', 'outline', 'cap'] #['skel', 'outline', 'cap']
        self.methods = ['lda', 'retrieval', 'bgplvm', 'feat_retrieval']
        self.fnames = {}
        self.orig_pWs = {}
        self.pWs = {}
        self.parts = {}
        self.W = {}
        self.models = {}
        self.datfiles = {}
        self.feats = {}
        prefix = 'full3.'
        
        #self.docs = {}
        #bestmatches = {}
        for mode in self.modes:
            
            self.datfiles[mode] = join(path, prefix + mode + '.pWs.npz')
            ds = np.load(join(path, prefix + mode + '.pWs.npz'))
            self.fnames[mode] = list(ds['fnames'])
            if self.options.method == 'retrieval':
                self.feats[mode] = list(ds['all_feats'])
                
            self.parts[mode] = list(ds['parts'])
            self.W[mode] = list(ds['W'])
            self.pWs[mode] = ds['pWs']
            
            self.pWs[mode][np.isnan(self.pWs[mode])] = 0
            #self.pWs[mode] = (self.pWs[mode].T / self.pWs[mode].max(axis=1)).T
            #self.pWs[mode] = np.e ** (- alphas[mode] * (self.pWs[mode].T - self.pWs[mode].mean(axis=1)).T ** 2)
            if mode == 'cap':
                self.pWs[mode] = np.e ** (- alphas[mode] * self.pWs[mode] ** 2)
            else:
                self.pWs[mode] = 1 - (self.pWs[mode].T / self.pWs[mode].max(axis=1)).T
                
            self.models[mode] = {}
            for method in self.methods:
                if method + '_model' in ds:
                    try:
                        self.models[mode][method] = ds[method + '_model'].tolist()
                    except Exception as e:
                        pass
                    
    def inferRules(self, query, given_chars, mode,method='bgplvm'):
        '''
        extract pair-wise similarity features (PWS) from the given segments
        
        '''
        if not hasattr(self, 'pWs'):
            self.loadData()
            
        path = cm.PATH
        parts = self.parts[mode]
        vocab = self.W[mode]   
        n_parts = len(self.parts[mode])
        if method is not 'retrieval':
            Y_test = query[np.newaxis, ...]
            
        Y_train = self.pWs[mode] #.reshape((-1, n_parts * n_parts))
        id2word = dict([(i, w) for i, w in enumerate(self.W[mode])])
        
        def train_model(method_):
            if method_ == 'lda':
                #Y_train_ = np.e ** (- alphas[mode] * Y_train ** 2)
                mm = gensim.matutils.Dense2Corpus(\
                Y_train * 100, \
                #self.pWs[mode][train_ids].reshape(-1, n_parts ** 2) * 100, \
                documents_columns=False)
                
                model = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=6, alpha='auto', decay=0.1,
                                                  update_every=1, chunksize=5, passes=5)   
                self.models[mode] = model
                cm.modify_npz(self.datfiles[mode], [(method + '_model', model)])
                
            elif method_ == 'bgplvm':
                Q = 7
                num_inducing = 20
                kernel = GPy.kern.RBF(Q, 1., 1. / np.random.uniform(0, 1, (Q,)), ARD=True)   + GPy.kern.Bias(Q, np.exp(-2))
                model = GPy.models.BayesianGPLVM(Y_train , Q, kernel=kernel, num_inducing=num_inducing)
                model.optimize('bfgs', messages=1, max_iters=500, gtol=1e-4)
                self.models[mode] = model
                cm.modify_npz(self.datfiles[mode], [(method +'_model', model)])
                
            return model
        
        if not  'retrieval' in method:        
            if (not method in self.models[mode].keys()  or self.options.retrain):
                model = train_model(method)
            else:
                model = self.models[mode][method]
                if method == 'lda' and not isinstance(model, gensim.models.ldamodel.LdaModel):
                    model = train_model(method)
                elif method == 'bgplvm' and not isinstance(model, GPy.models.bayesian_gplvm.BayesianGPLVM):
                    model = train_model(method)
            
        Y_full = []
        if method == 'lda':
            Y_test[:, np.isnan(query)] = 0
            #Y_test = np.e ** (- alphas[mode] * Y_test ** 2)
            Y_test = Y_test * 100
            for test in Y_test:
                test_corpus = gensim.matutils.Dense2Corpus(test[np.newaxis, :], documents_columns=False)
                topic_weights = model.inference(test_corpus)
                Y_full.append( (model.state.get_lambda() * topic_weights[0].transpose()).sum(axis=0) )
                
        elif method == 'bgplvm':
            #Y_test[:, masks[mode]] = np.nan        
            for test in Y_test:
                X_star, _ = model.infer_newX(test[np.newaxis, :], optimize=True) #do_test_latents(Y[:2])
                Y_full.append( model.predict(np.array(X_star.mean))[0][0] )             
                
        elif method == 'feat_retrieval':
            idx = list(self.fnames[mode]).index('Amiri-Regular.ttf.npz')
            Y_full = [Y_train[idx]]
            '''
            #nmask = np.bitwise_not(masks[mode])
            nmask = np.bitwise_not(np.isnan(query))
            if nmask.sum() == 0:
                idx = list(self.fnames[mode]).index('Amiri-Regular.ttf.npz')
                Y_full = [Y_train[idx]]
            else:
                Y_full = []
                for qx, query in enumerate([query]):
                    dists =  [euc(query[nmask], item[nmask]) for item in Y_train]
                    best_id = np.argmin(dists)
                    #best_id = np.argmax(dists)
                    Y_full.append(Y_train[best_id])
            '''
            
        elif method == 'retrieval':
            dists = {'outline' : euclidean, 'cap' : ft.simple_hausdorff, 'skel' : euclidean}
            dist = dists[mode]
            scores = []
            for font in self.feats[mode]:
                score = 0
                for chx in range(len(font)):
                    for px in range(len(font[chx])):
                        if len(query[chx][px][0]) > 0:
                            #score += 1 - np.e ** (-alphas[mode] * dist(font[chx][px][0], query[chx][px][0]) ** 2)
                            score += dist(font[chx][px][0], query[chx][px][0]) 
                            
                scores.append(score)
                
            best_id = np.argmin(scores)
            Y_full.append( Y_train[best_id] )
            
        #calculate the score
        Y_full = np.array(Y_full)
        missing_chars = [ch for ch in np.array(cm.all_chars)[self.chrange] if ch not in given_chars]
        bestmatches = [[] for _ in range(len(Y_full))]#np.zeros((len(Y_full), n_parts), dtype=int)
        for bm in bestmatches:
            for p in range(n_parts):
                bm.append([])
                
        ids = self.getIndices(missing_chars, parts)
        missing_parts = [parts[idd] for idd in ids]
            
        for mpt in missing_parts:
            candidates = []
            for vx, v in enumerate(vocab):
                if (v[0] in given_chars or v[4] in given_chars) and mpt in v:
                    candidates.append(vx)
                    
            selected_vocab = [vocab[v] for v in candidates]        
            for yx, yfull in enumerate(Y_full):
                #if method == 'lda':
                bestword = selected_vocab[yfull[candidates].argmax()]
                prob = yfull[candidates].max()
                #else:
                #bestword = selected_vocab[yfull[candidates].argmin()]
                #prob = yfull[candidates].min()
                
                bestmatches[yx][parts.index(mpt)] =  (vocab.index(bestword), prob)
            
        return bestmatches
    
    def cubic2graph(self, pathdata, transform=None):
        #segment_data = segment.get('d')
        cubic_p = cubicsuperpath.parsePath(pathdata)
        p = deepcopy(cubic_p)
        cspsubdiv.cspsubdiv(p, self.options.flat)
        
        poly = []
        for sp in p[0]:
            poly.append ([sp[1][0], sp[1][1]])
        
        if transform is not None:
            if 'translate' in transform:
                mat = simpletransform.parseTransform(transform)                    
                offset =np.array([ mat[0][2], mat[1][2]])
                poly = np.array(poly) + offset
                
        #poly1 = np.asarray(asPolygon(poly).buffer(self.options.thinning).exterior)
        poly1 = np.asarray(asPolygon(poly).buffer(0).exterior)
        #poly1 = geo.sample_polyline(np.array(poly1), step=100)
        #poly1 =np.array(poly)
        graph = CGAL.extract_skeleton(poly1[np.newaxis,...])
        
        return graph, poly1
        
    def poly2graph(self, polydata):
        poly = simplepath.parsePath(polydata)
        poly1 = []
        for p in poly:
            poly1.append(p[1])
        
        poly1 = np.asarray(asPolygon(poly1).simplify(tolerance=0.01).buffer(0).exterior)
        #poly1 = np.asarray(asPolygon(poly1))
        graph = CGAL.extract_skeleton(poly1[np.newaxis,...])
        return graph, poly1
        
    def findSkeleton(self, graph):
        graph = deepcopy(graph)
        #graph without non skteleton vertices
        for node, data in graph.nodes(data=True): # nodes_iter in old networkx
            if data['mode'] == CGAL.NON_SKELETON:
                graph.remove_node(node)

        #finding two terminals of the stroke
        terms = [node for node, degree in graph.degree() if degree == 1] # degree_iter in old networkx
        ntm = len(terms)
        bi, bj = 0, 0
        mx = 0
        for i in range(ntm):
            for j in range(i + 1, ntm):
                if i == j:
                    continue
                #d = euc(graph.node[terms[i]]['pos'], graph.node[terms[j]]['pos'])
                d = nx.shortest_path_length(graph, terms[i], terms[j])
                if d > mx:
                    mx = d
                    bi, bj = i, j
        
        #extract the path connecting 2 terminals
        path = nx.shortest_path(graph, terms[bi], terms[bj])
        
        points = [list(graph.node[p]['pos']) for p in path]
        #i = 0
        #while x != y:
            
        return path, np.array(points)
        
    def parseGlyphs(self, svg):
        '''
        parse all the glyphs skeletons and segments in a SVG file
        '''
        glyphdata = svg.findall("./svg:g[@class='glyph']",namespaces=self.ns)
        glyphs = {}
        for glyph in glyphdata:
            #first char of the id is the character
            ch = glyph.attrib['id'][0]
            chx = cm.all_chars.index(ch)
            
            if len(glyph.getchildren()) < 2:
                continue
            
            skeleton_group = glyph.getchildren()[0]
            segment_group = glyph.getchildren()[1]
            
            skels = []
            templates = []
            for path in skeleton_group.getchildren():
                points = []
                
                #data = np.fromstring(path.get('data'), dtype=float, sep=' ').reshape((-1, 2))
                s = StringIO(path.get('data').replace(';', '\n'))
                data = np.loadtxt(s).reshape((-1, 2))
                if path.tag == inkex.addNS( 'path', 'svg' ):
                    d = simplepath.parsePath(path.get('d'))
                    for _, p in d:
                        points.append(p)
                elif path.tag == inkex.addNS('circle', 'svg'):
                    points.append(np.array([float(path.get('cx')), float(path.get('cy'))]))
                
                templates.append(data)
                skels.append(np.array(points))
                
            outlines = [[] for _ in range(cm.specs[chx])]
            strokes = [[] for _ in range(cm.specs[chx])]
            for path in segment_group.getchildren():
                poly = []
                if ch in self.init_chars:
                    p = cubicsuperpath.parsePath(path.get('d'))
                    cspsubdiv.cspsubdiv(p, self.options.flat)
                    for sp in p[0]:
                        #simpletransform.applyTransformToPoint(mat, sp[1])
                        poly.append ([sp[1][0], sp[1][1]])                 
                else:
                    p = simplepath.parsePath(path.get('d'))
                    transform = path.get('transform')
                    mat = None
                    if transform is not None:
                        mat = simpletransform.parseTransform(transform)
                        
                    for sp in p:
                        if mat is not None:
                            simpletransform.applyTransformToPoint(mat, sp[1])
                            
                        poly.append ([sp[1][0], sp[1][1]])    
                    
                
                partid = int(path.get('id')[1:3])
                data = []
                if path.get('data') is not None:
                    s = StringIO(path.get('data'))
                    data = np.loadtxt(s)
                    if partid < cm.mains[chx]:
                        data = data.reshape((2, data.shape[0] / 4, 2)).tolist()
                    else:
                        data = data.reshape((-1, 2))
                
                outlines[partid] = np.array(poly)
                strokes[partid] = data
                
            transform = glyph.get('transform')
            if not transform is None:
                mat = simpletransform.parseTransform(transform)
                offset = np.array([mat[0][2], mat[1][2]])
            else:
                offset = np.zeros(2,dtype=float)
                
            #              0         1        2            3           4             5          6
            glyphs[ch] = (offset, skels, skeleton_group, outlines, segment_group, templates, strokes)
        
        if len(glyphs) == 0:
			log.write("can't pass any glyph\n")    
        return glyphs
        
    def getPartIndices(self, chars, parts):
        '''
        get the indices of the parts that contain chars
        '''
        indices = []
        for px, part in enumerate(parts):
            for ch in chars:
                if ch in part:
                    indices.append(px)
                    break
                    
        return np.array(indices)    
    
    def prepareDatapoints(self, outline, skeleton):
        '''
        prepare the datapoints for a single stroke
        '''
        datapoints = [[],[],[],[]]
        poly = asPolygon(outline)
        for px in range(len(skeleton)):
            point = skeleton[px]
            if px < len(skeleton) - 1:
                vec = skeleton[px + 1] - skeleton[px]
            else:
                vec = skeleton[-1] - skeleton[-2]
                
            nvec = np.array([-vec[1], vec[0]]) / np.linalg.norm(vec)
            inters = [None, None]
            for i in [0, 1]:
                if i == 0:
                    s = 1
                else:
                    s = -1
                ray = asLineString([nvec * 300 * s + point, point])
                
                inter = np.array(ray.intersection(poly))
                if len(inter) != 0:
                    inters[i] = inter[0]
            #for i in [0, 1]:
            if inters[0] is not None and inters[1] is not None:
                for i in [0, 1]:
                    datapoints[0 + i].append(inters[i])
                    datapoints[2 + i].append(point)
        
       # for l in range(4):
       #     datapoints[l] = np.array(datapoints[l])
            
        return datapoints #np.array(datapoints)
        
    def simplify(self, path, points, graph, trim=1):
        if len(points) < 5:
            return path, points
            
        newpoints = np.array(asLineString(points).simplify(0.2))
        if trim > 0 and len(newpoints) - trim * 2 >= 3:
            newpoints = newpoints[trim:-trim]
            
        terms = []
        for point in [newpoints[0], newpoints[-1]]:
            dists = [euc(graph.node[node]['pos'], point) for node in path]
            idd = np.argmin(dists)
            terms.append(path[idd])
            
        newpath = path[path.index(terms[0]):path.index(terms[1])]

        newpoints = [list(graph.node[p]['pos']) for p in newpath]
 
        return newpath, np.array(newpoints)
        
    def extendStroke(self, dst_skel, n_steps, step_size):
        d1 = euc(dst_skel[0], dst_skel[1])
        d2 = euc(dst_skel[-1], dst_skel[-2])
        vecs = [(dst_skel[0] - dst_skel[1]) / d1, (dst_skel[-1] - dst_skel[-2]) / d2]
        new_skel = list(deepcopy(dst_skel))
        step = 0
        while step < n_steps:
            new_skel = [vecs[0] * d1 + new_skel[0]] + new_skel + [vecs[1] * d2 + new_skel[-1]]
            step += 1
                    
        return new_skel
        
    def effect(self):
        """
        Effect behaviour.
        Overrides base class' method and inserts "Hello World" text into SVG document.
        """
        svg = self.document.getroot()
        ns = {'svg':'http://www.w3.org/2000/svg'}
        
        #g = svg.find(".//svg:g[@id='layer1']", namespaces = ns)
        log.write("GIVEN CHARS:" + ",".join(self.options.init_chars) + "\n")
        
        # print("reading input glyphs")
        self.init_chars = sorted(self.options.init_chars)
        glyphs = self.parseGlyphs(svg)
        log.write("GIVEN GLYPHS:" + ",".join(glyphs.keys()) + "\n")
        
        if self.options.chset == 'upper':
            self.chrange = np.arange(26, 52)
        elif self.options.chset == 'lower':
            self.chrange = np.arange(0, 26)
            
        self.missing_chars = []
        self.all_chars = [cm.all_chars[c] for c in self.chrange]
        
        for ch in glyphs.keys():
            if not ch in self.init_chars:
                self.missing_chars.append( ch )
        # print "missing character is" 
        
        #self.missing_chars = self.missing_chars[:3]
        log.write("MISSING CHARS:" + ",".join(self.missing_chars) + "\n")
        
        if self.options.mode == 'snap':
            segments = [(idd, segment) for idd, segment in self.selected.iteritems()]
            if len(segments) == 0:
                return
            segment = segments[0][1]
            #segment = svg.find("./svg:g[@id='layer1']/svg:path", namespaces = ns)
            if segment.tag == inkex.addNS('path','svg'):
                #graph, poly = self.poly2graph(simplepath.formatPath(polydata))
                graph, poly = self.cubic2graph(segment.get('d'), transform=segment.get('transform'))
                path, _ = self.findSkeleton(graph)
                points = np.array([graph.node[p1]['pos'].tolist() for p1 in path]) * np.array([1, -1])
                points = geo.sample_polyline(points, step=100)
                #points = np.array(asLineString(points).simplify(0.05))[1:-1]
                
                #Snap the new segment to a skeleton part
                best_part, best_char, min_d = 0, '', 1e10
                for ch in self.init_chars:
                    
                    chx = cm.all_chars.index(ch)
                    skels = glyphs[ch][1]
                    offx, offy = glyphs[ch][0]
                    offset = np.array([offx, offy])

                    #Find the most probable part id    
                    for sx, skel in enumerate(skels):
                        d = Distance.hausdorff(skel +  offset, points , mode=1)
                        if d < min_d:
                            best_part = sx
                            best_char = ch
                            min_d = d
                
                offset = np.array(glyphs[best_char][0])
                #---now that we know which part the segment belongs, we just bring it to the right group
                if not best_char in self.init_chars:
                    self.init_chars += best_char
                    
                
                segment_group = glyphs[best_char][4]
                #simplepath.translatePath(cubic_p, -offset[0], -offset[1])
                poly = (np.array(poly) - offset) * np.array([1, -1])
                polydata = zip(['M'] + ['L'] * (len(poly) - 1), [list(point) for point in poly])
                idd =  best_char + str(best_part).zfill(2) + '_seg'
                
                segment1 = segment_group.find("./svg:path[@id='%s']" % idd, namespaces=ns)
                if  segment1 is None:
                    segment1 = inkex.etree.SubElement(segment_group, 'path') #deepcopy(segment)
                    #segment_group.append(segment1)
                    
                segment1.set('d', simplepath.formatPath(polydata))
                segment1.set('id', idd)
                
                segment1.set('style', simplestyle.formatStyle({'fill': \
                colist[best_part + list(cm.capital_chars).index(best_char) * 10 ], \
                'opacity' : '0.8'}))

                #---now modify the skeleton_group and add the new partial skeleton 
                points -= offset
                isstroke = best_part < cm.mains[cm.all_chars.index(best_char)]
                if isstroke:
                    tpl = glyphs[best_char][5][best_part]
                    if euclidean(tpl[0], points[0]) > euclidean(tpl[0], points[-1]):
                        points = points[::-1]
                #if isstroke:
                    skeleton_group = glyphs[best_char][2]
                    skel = skeleton_group.find("./svg:path[@id='%s_skel']" \
                    % (best_char + str(best_part).zfill(2)), namespaces=ns )
                    pathdata = zip(['M'] + ['L'] * (len(points) - 1) + ['Z'], [list(point) for point in points])
                    skel.set('d', simplepath.formatPath(pathdata))
                else:
                    skeleton_group = glyphs[best_char][2]
                    anchor = glyphs[best_char][1][best_part][0]
                    
                    skel = skeleton_group.find("./svg:circle[@id='%s_skel']" \
                    % (best_char + str(best_part).zfill(2)), namespaces=ns )
                    #n_mains = cm.mains[cm.all_chars.index(best_char)]
                    if skel is None:
                        print 'Could not find equivalent part ID'
                    else:
                        skel.set('cx', str(anchor[0]))
                        skel.set('cy', str(anchor[1]))
                     
                    
        elif self.options.mode == 'synthesize':
            
            # print "begin synthesization"        
            # First infer the copying rules for entire font
            fonts = {}
            skels = []
            
            #weuc = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[10] * (len(a1)/2), [1] * (len(a1)/2)]).T.flatten())
            #dists = {'outline' : euclidean, 'cap' : ft.simple_hausdorff, 'skel' : euclidean}
            weuc1 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[1.0] * (len(a1)/2), [0.05] * (len(a1)/2)]).T.flatten())
            weuc2 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[0.1] * (len(a1)/2), [1.0] * (len(a1)/2)]).T.flatten())
            dists = {'outline' : weuc1, 'cap' : ft.simple_hausdorff, 'skel' : weuc2}
            
            modes = ['skel', 'outline', 'cap']
            rules = {}
            
            for mode in modes: 
                fonts[mode] = [[] for _ in range(len(cm.all_chars))]
                for chx, ch in enumerate(cm.all_chars):
                    for pt in range(cm.specs[chx]):
                        fonts[mode][chx].append([])
            
            # print "segment input and prepare feature vectors"            
            log.write("segment input and prepare feature vectors\n")
            for ch in self.init_chars:
                chx = cm.all_chars.index(ch)
                glyph = glyphs[ch]

                #for each segment of the character
                for childx, child in enumerate(glyph[4].getchildren()):
                    d = child.get('d')
                    graph, poly = self.poly2graph(d)
                    part = child.get('id')[:3]
                    ch_, pt_ = part[0], int(part[1:])
                    chx_ = cm.all_chars.index(ch_)
                    
                    if pt_ >= cm.mains[chx_]:
                        fonts['cap'][chx_][pt_] = poly
                        continue 
                    
                    path, points = self.findSkeleton(graph)
                    #newpath = path
                    newpath, points = self.simplify(path, points, graph, trim=self.options.trim)
                    tpl = glyphs[ch_][5][pt_]
                    if euclidean(tpl[0], points[0]) > euclidean(tpl[0], points[-1]):
                        points = points[::-1]
                        newpath = newpath[::-1]
                        
                    points = self.extendStroke(points, int(self.options.merge_extend / 50), 50)
                    datapoints1 = [self.prepareDatapoints(poly, points)]
                    
                    for v1, v2, data in graph.edges(data=True): # edges_iter in old networkx
                        if data['mode'] == CGAL.CONTOUR:
                            graph.remove_edge(v1, v2)
                            
                    #extracting stroke from the graph
                    segments, _, datapoints = Segmentation.segment_graph(graph, \
                    None, [path], modes=['stroke'])
                    
                    #_, _, datapoints1 = Segmentation.segment_graph(graph, \
                    #None, [newpath], modes=['stroke'])
                    
                    if pt_ < cm.mains[chx_]:
                        #fonts['simplified_outline'][ch_][pt_] = datapoints1[1]
                        fonts['outline'][chx_][pt_] = datapoints[0] + datapoints1[0]
                        fonts['skel'][chx_][pt_] = points
            
            # print "inferring transferring rules"
            log.write("inferring transferring rules\n")
            for mode in modes:
                parts = dsrc.get_parts(mode, self.chrange)
                W = dsrc.get_feature_names(mode, self.chrange)
                feats = dsrc.get_features([('testfont', fonts[mode])], mode, self.chrange)
                chars = self.all_chars#[cm.all_chars[c] for c in self.chrange]
                pWs = np.array(dsrc.get_similarity_vec(feats, parts, W, chars, dists[mode]))
                
                #if np.bitwise_not(np.isnan(pWs)).sum() > 0:
                #    pWs /= pWs[np.bitwise_not(np.isnan(pWs))].max()
                #convert to "kernel" similarity measurement
                if mode == 'cap':
                    pWs = np.e ** (-alphas[mode] * pWs ** 2)
                elif np.bitwise_not(np.isnan(pWs)).sum() > 0:
                    pWs = 1 - pWs / pWs[np.bitwise_not(np.isnan(pWs))].max()
                    
                #mean = pWs[np.bitwise_not(np.isnan(pWs))].mean()
                #pWs = np.e ** (- alphas[mode] * (pWs - mean) ** 2)
                
                if self.options.method == 'retrieval':
                    rules[mode] = self.inferRules(feats[0], self.init_chars, mode, method='retrieval')[0]
                else:
                    rules[mode] = self.inferRules(pWs[0], self.init_chars, mode, method=self.options.method)[0]                    
            
            #--- transfer the skeleton style first
            # print "synthesizing skeletons"
            log.write("synthesizing skeletons\n")
            newskels = []
            mode = 'skel'
            #if True:
            for ch in self.missing_chars:
                log.write("MODE: " + mode + ", CHAR: " + ch + "\n")
                newskels.append([])
                skels = glyphs[ch][1]
                chx = cm.all_chars.index(ch)
                dst_ids = self.getPartIndices([ch],self.parts[mode])
                n_skels = cm.mains[chx]
                for sk in range(n_skels):
                    word_prob = rules[mode][dst_ids[sk]][1]
                    if word_prob > self.options.skeleton_thresh:
                        
                        word = self.W[mode][ rules[mode][dst_ids[sk]][0] ]
                        #print word, word_prob
                        #print word
                        act, ch1, pt1, src_ch_id, src_pt = dsrc.word2part(word, chars=self.all_chars, dst_ch = ch)
                        src_ch = self.all_chars[src_ch_id]
                        dst_skel = sz.transfer_skeleton_style(glyphs[src_ch][1][src_pt], glyphs[src_ch][5][src_pt], skels[sk], act)
                        #newskels[-1].append(dst_skel)
                        skel_group = glyphs[ch][2]
                        
                        #style = simplestyle.formatStyle({'fill' : 'none', 'stroke': \
                        #mlt.colors.cnames[cm.color_names[src_pt + \
                        #self.init_chars.index(src_ch) * 10 ]], 'stroke-width' : '5'})
                        style = ''
                        if len(dst_skel) > 1:
                            dst_skel = self.extendStroke(dst_skel, int(self.options.merge_extend / 100), 100)
                        
                        self.addPolyElement(dst_skel, skel_group , idd = self.getIdd(ch, sk, '_skel'), \
                        style=style, replace=False)
            
            
            #reload the skeletons
            # print "synthesizing stroke"
            log.write("synthesizing stroke\n")
            glyphs = self.parseGlyphs(svg) 
            #--- Now transfer the stroke style
            mode = 'outline'
            for ch in self.missing_chars:
                skels = glyphs[ch][1]
                chx = cm.all_chars.index(ch)
                #Number of strokes for each character
                n_strokes = cm.mains[chx]
                dst_ids = self.getPartIndices([ch],self.parts[mode])
                for st in range(n_strokes):
                    #Source 
                    word = self.W[mode][ rules[mode][dst_ids[st]][0] ]
                    prob = rules[mode][dst_ids[st]][1]
                    act, ch1, pt1, src_ch_id, src_part = dsrc.word2part(word, chars=cm.all_chars, dst_ch = ch)
                    src_ch = cm.all_chars[src_ch_id]
                    src_skel = fonts[mode][src_ch_id][src_part][4:6] #[0:2]
                    src_outline = fonts[mode][src_ch_id][src_part][6:8] #[2:4]
                    
                    #Destination 
                    dst_skel = skels[st]
                    #if len(src_outline) == 0:
                    #    src_outline = [src_skel, src_skel, src_skel, src_skel]
                    
                    if len(src_outline) == 0 or len(src_skel) <=1 or len(dst_skel) <= 1:
                        continue
                    if any([len(src_outline[i]) == 0 for i in range(2)]):
                        continue
                    
                    dst_outline = sz.transfer_stroke_style(src_skel, dst_skel, \
                    src_outline=src_outline, method='stretch', smooth=False, act=act)
                    
                    outline_group = glyphs[ch][4]
                    outline = np.concatenate([dst_outline[0], dst_outline[1][::-1]])
                    #outline = np.concatenate([src_outline[0], src_outline[1][::-1]])
                    
                    #print prob
                    style = simplestyle.formatStyle({
                    'fill': colist[src_part + list(cm.capital_chars).index(src_ch) * 10 ], \
                    'opacity' : 0.8, 'stroke-width' : 20})
                    # 'fill' : mlt.colors.rgb2hex(mlt.cm.jet([max(0.0, prob)])[0]) })
                    
                    #'opacity' : str(max(0.0, prob)) })
                    
                    segment = self.addPolyElement(outline, outline_group , idd = self.getIdd(ch, st, '_seg'), \
                    style=style, replace=False)
                    
                    s= StringIO('')
                    np.savetxt(s, np.array(dst_outline).reshape((2, -1)), fmt='%.2f')
                    segment.set('data', s.getvalue())
                    
            #return
            #--Finally the caps
            # print "synthesizing cap"
            log.write("synthesizing cap\n")
            mode = 'cap'
            for ch in self.missing_chars:
                chx = cm.all_chars.index(ch)
                #n_caps = cm.specs[chx] - cm.mains[chx]
                for st in range(cm.mains[chx], cm.specs[chx]):
                    anchors_ = cm.anchors[dsrc.part_id(ch, st)]
                    ch_, pt_, tm_ = anchors_[0][0][0], int(anchors_[0][0][1:]), anchors_[0][1]
                    if tm_ == 1: tm_ = -1
                    dst_anchor = glyphs[ch_][1][pt_][tm_]
                    
                    word, prob = rules[mode][list(self.parts[mode]).index(dsrc.part_id(ch, st))] #rules[mode][cm.parts.index(dsrc.part_id(ch, st))]
                    
                    word = self.W[mode][word]
                    act, ch1, pt1, src_ch_id, src_pt = dsrc.word2part(word, self.all_chars, dst_ch = ch)
                    
                    src_char = self.all_chars[src_ch_id]
                    src_anchor = glyphs[src_char][1][src_pt][0]
                    src_cap = np.array(glyphs[src_char][3][src_pt])
                    
                    if len(src_cap) == 0:
                        continue
                    
                    if act == 2:
                        src_cap[:, 1] = (src_cap[:, 1] - src_anchor[1]) * -1 + src_anchor[1]
                    elif act == 1:
                        src_cap[:, 0] = (src_cap[:, 0] - src_anchor[0]) * -1 + src_anchor[ 0]
                    
                    offset = dst_anchor - src_anchor
                    src_cap += offset
                    
                    style = simplestyle.formatStyle({'fill': \
                    #mlt.colors.cnames[cm.color_names[src_pt + self.init_chars.index(src_char) * 10 ]], 'opacity' : '0.5'})
                    colist[src_pt + list(cm.capital_chars).index(src_char) * 10 ], \
                    'opacity' : 0.8, 'stroke-width' : 20, \
                    'stroke' : mlt.colors.rgb2hex(mlt.cm.jet([max(0.0, prob)])[0]) })
                    
                    outline_group = glyphs[ch][4]
                    
                    segment = self.addPolyElement(src_cap, outline_group , idd = self.getIdd(ch, st, '_seg'), \
                    style=style, replace=False)
                    
                    s= StringIO('')
                    np.savetxt(s, np.array(src_cap).reshape((2, -1)), fmt='%.2f')
                    segment.set('data', s.getvalue())
            
            
        elif self.options.mode == 'merge':
            glyphs = self.parseGlyphs(svg)     
            for ch in self.missing_chars + self.init_chars:

                nmain = cm.mains[cm.all_chars.index(ch)]
                strokes = glyphs[ch][6]
                outlines = glyphs[ch][3]
                polies = []
                
                if ch in self.missing_chars:
                    new_ref_anns, anchors = sz.prepare_skeletons([glyphs[ch][1]], anchor_thresh=400, smoothing=False)
                    tabu = []
                    for anchor in anchors[0]:
                        part1, apoint1, jtype1, part2, apoint2, jtype2 = anchor
                        #Avoid repetitive anchors...
                        #if (part1, part2, jtype1) in tabu or (part2, part1,jtype2) in tabu:
                        if (part1, part2, jtype1, jtype2) in tabu or (part2, part1, jtype2, jtype1) in tabu: #(part1, jtype1) in tabu or (part2, jtype2) in tabu:
                            continue
                        
                        tabu.extend([(part1, part2, jtype1, jtype2), (part2, part1, jtype2, jtype1)]) #tabu.append((part1, jtype1)) #((part1, part2,jtype1))
                        #tabu.append((part2, jtype2)) #((part2, part1,jtype2))
                        
                        if (part1 < nmain or part2 < nmain) and \
                        (len(strokes[part1]) > 0 or len(strokes[part2]) > 0) and \
                        (len(outlines[part1]) > 0 and len(outlines[part2]) > 0):
                            
                            ply1 = strokes[part1]
                            ply2 = strokes[part2]
                            
                            if part1 >= nmain:
                                ply1 = outlines[part1][:-1]
                            if part2 >= nmain:
                                ply2 = outlines[part2][:-1]
                                
                            hull = sz.join_parts([ply1, ply2], \
                                             [apoint1, apoint2],[jtype1, jtype2], 'pointy', \
                                             extension=self.options.merge_extend, smooth=self.options.merge_smooth != 0)
                            if hull.is_valid and not hull.is_empty:
                                polies.append(hull)
                    #'''            
                    for part in range(len(outlines)):
                        if len(outlines[part]) > 0:
                            ply = asPolygon(outlines[part])
                            if ply.is_valid and not ply.is_empty:
                                polies.append(ply)
                    #'''
                #'''
                else:
                    for i in range(len(outlines)):
                        if len(outlines[i]) > 0:
                            ply = asPolygon(outlines[i])
                            if ply.is_valid and not ply.is_empty:
                                polies.append(ply)
                #'''
                if len(polies) > 0:
                    glyph = []
                    combined_outline = cascaded_union(polies)
                    glyph = combined_outline.buffer(self.options.thinning).buffer(-self.options.thinning)
                    #glyph = polies
                    
                    layer = svg.find("./svg:g[@id='%s']" % ch, namespaces=self.ns)
                    contours = layer.find("./svg:g[@id='%s_contours']" % ch, namespaces=self.ns)
                    if contours is None:
                        contours = inkex.etree.SubElement(layer, 'g')
                        contours.set('id', '%s_contours' % ch)
                    
                    if hasattr(glyph, 'geom_type'):
                        if glyph.geom_type == 'Polygon':
                            glyph = [glyph]
                        
    #                if glyph.geom_type == 'MultiPolygon':
                    for px, poly in enumerate(glyph):
                        polypath = contours.find("./svg:path[@id='%s_contour_%d']" % (ch, px), namespaces=self.ns)
                        if polypath is None:
                            polypath = inkex.etree.SubElement(contours, 'path')
                            
                        d = self.formatPath(np.array(poly.exterior) + np.array([0, 5000]))
                        for inter in poly.interiors:
                            d += self.formatPath(np.array(inter) + np.array([0, 5000]))
                            
                        polypath.set('id', '%s_contour_%d' % (ch, px))
                        polypath.set('d', d)
                        polypath.set('style', simplestyle.formatStyle({
                        'fill': '#99CCFF', 'opacity' : '1.0'}))
            
                    
# Create effect instance and apply it.
effect = FontEditor()
effect.affect()

log.close()
