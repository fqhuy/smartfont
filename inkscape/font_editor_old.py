#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')
sys.path.append('/home/phan/workspace/coolfont')

# We will use the inkex module with the predefined Effect base class.
import inkex
import os
# The simplestyle module provides functions for style parsing.
from simplestyle import *
import cubicsuperpath, simplepath, cspsubdiv
from shapely.geometry import *
from coolfont.cgal import CGAL
from os.path import join
import numpy as np
import networkx as nx
from scipy.spatial.distance import euclidean as euc
from coolfont.fast import Distance
from coolfont import Segmentation
from coolfont import DataSource
from coolfont import Common
from coolfont import Synthesization as sz
from coolfont import Visualization as vz
from copy import deepcopy
from ast import literal_eval
import gensim
import simplestyle
import matplotlib as mlt

class FontEditor(inkex.Effect):
    """
    Example Inkscape effect extension.
    Creates a new layer with a "Hello World!" text centered in the middle of the document.
    """
    def __init__(self):
        """
        Constructor.
        Defines the "--what" option of a script.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define string option "--what" with "-w" shortcut and default value "World".
        self.OptionParser.add_option('-i', '--init_chars', action = 'store',
          type = 'string', dest = 'init_chars', default = 'ACH',
          help = 'What would you like to greet?')

        #self.options.flat = 10.0
        self.OptionParser.add_option("-f", "--flatness",
                        action="store", type="float", 
                        dest="flat", default=10.0,
                        help="Minimum flatness of the subdivided curves")     
                        
        self.OptionParser.add_option("-t", "--thinning",
                        action="store", type="float", 
                        dest="thinning", default=0.0,
                        help="Minimum flatness of the subdivided curves")   
                        
        self.OptionParser.add_option("-m", "--mode",
                        action="store", type="string", 
                        dest="mode", default='snap', #synthesize
                        help="action")   
                       
        self.ns = {'svg':'http://www.w3.org/2000/svg'}
        
    def getIndices(self, chars, parts):
        indices = []
        for px, p in enumerate(parts):
            for ch in chars:
                if ch in p:
                    indices.append(px)
                    break
                    
        return np.array(indices)   
        
    def addPolyElement(self, poly, parent, idd, offset=None, style='', replace=False):
        '''
        add a polygon to a parent_element
        '''
        if offset is None:
            offset = np.array([0,0])
            
        poly = np.array(poly) - offset
        polydata = zip(['M'] + ['L'] * (len(poly) - 1), [list(point) for point in poly])
        
        segment1 = parent.find("./svg:path[@id='%s']" % idd, namespaces=self.ns)
        if segment1 is None or replace:
            segment1 = inkex.etree.SubElement(parent, 'path')
            
        segment1.set('d', simplepath.formatPath(polydata))
        segment1.set('id', idd)
        segment1.set('style', style)
        #segment_group.append(segment1)
        
    def getIdd(self, ch, part, posfix=''):
        return ch + str(part).zfill(2) + posfix
            
    def inferRules(self, query, given_chars):
        '''
        extract pair-wise similarity features (PWS) from the given segments
        
        '''
        path = Common.PATH
        self.modes = ['outline'] #['skel', 'outline', 'cap']
        self.fnames = {}
        self.orig_pWs = {}
        self.pWs = {}
        self.parts = {}
        self.W = {}
        #self.docs = {}
        bestmatches = {}
        for mode in self.modes:
            ds = np.load(join(path, mode + '.pWs.npz'))
            self.fnames[mode] = list(ds['fnames'])
            self.parts[mode] = ds['parts']
            self.W[mode] = list(ds['W'])
            #self.orig_pWs[mode] = ds['orig_pWs']
            self.pWs[mode] = ds['pWs']
            #self.feats = ds['all_feats']
            
            ds = np.load(join(path, mode + '_dict.npz'))
            self.dmat = ds['mat']
            
            
            n_parts = len(self.parts[mode])
            Y_test = query[np.newaxis, ...]
            Y_train = self.pWs[mode].reshape((-1, n_parts * n_parts))
            id2word = dict([(i, w) for i, w in enumerate(self.W[mode])])
            
            fmodel = join(path, 'full.' + mode + '.lda_model.txt')
            if not os.path.isfile(fmodel):
                mm = gensim.matutils.Dense2Corpus(\
                Y_train, \
                #self.pWs[mode][train_ids].reshape(-1, n_parts ** 2) * 100, \
                documents_columns=False)
                
                lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=6, 
                                                  update_every=1, chunksize=10, passes=5)   
                lda.save(fmodel)
                
            else:
                lda = gensim.models.ldamodel.LdaModel.load(fmodel)
                
            Y_full = []
            for test in Y_test:
                test_corpus = gensim.matutils.Dense2Corpus(test[np.newaxis, :], documents_columns=False)
                topic_weights = lda.inference(test_corpus)
                Y_full.append( (lda.state.get_lambda() * topic_weights[0].transpose()).sum(axis=0) )
                
            #calculate the score
            Y_full = np.array(Y_full)
            Y_full = Y_full.reshape(-1, n_parts, n_parts)
            ids = self.getIndices(given_chars, self.parts[mode])
            mask = np.zeros((n_parts, n_parts), dtype=bool)
            mask[ids, :] = True
            mask[ids, ids] = False
            Y_full[:, np.bitwise_not(mask)] = 0 #only consider words that have at least 1 part from given_chars
            bestmatches[mode] = np.argmax(Y_full, axis = 1)[0]
            
        return bestmatches
    
    def cubic2graph(self, pathdata):
        #segment_data = segment.get('d')
        cubic_p = cubicsuperpath.parsePath(pathdata)
        p = deepcopy(cubic_p)
        cspsubdiv.cspsubdiv(p, self.options.flat)
        
        poly = []
        for sp in p[0]:
            poly.append ([sp[1][0], sp[1][1]])
        
        poly1 = np.asarray(asPolygon(poly).buffer(self.options.thinning).exterior)
        #poly1 =np.array(poly)
        graph = CGAL.extract_skeleton(poly1[np.newaxis,...])
        
        return graph, poly1
        
    def poly2graph(self, polydata):
        poly = simplepath.parsePath(polydata)
        poly1 = []
        for p in poly:
            poly1.append(p[1])
        
        poly1 = np.asarray(asPolygon(poly1).buffer(0).exterior)
        graph = CGAL.extract_skeleton(poly1[np.newaxis,...])
        return graph, poly1
        
    def findSkeleton(self, graph):
        graph = deepcopy(graph)
        #graph without non skteleton vertices
        for node, data in graph.nodes_iter(data=True):
            if data['mode'] == CGAL.NON_SKELETON:
                graph.remove_node(node)

        #finding two terminals of the stroke
        terms = [node for node, degree in graph.degree_iter() if degree == 1]
        ntm = len(terms)
        bi, bj = 0, 0
        mx = 0
        for i in range(ntm):
            for j in range(ntm):
                d = euc(graph.node[terms[i]]['pos'], graph.node[terms[j]]['pos'])
                if d > mx:
                    mx = d
                    bi, bj = i, j
        
        #extract the path connecting 2 terminals
        path = nx.shortest_path(graph, terms[bi], terms[bj])
        return path
        
    def parseGlyphs(self, svg):
        '''
        parse all the glyphs skeletons and segments in a SVG file
        '''
        glyphdata = svg.findall("./svg:g[@class='glyph']",namespaces=self.ns)
        glyphs = {}
        for glyph in glyphdata:
            #first char of the id is the character
            ch = glyph.attrib['id'][0]
            if len(glyph.getchildren()) < 2:
                continue
            
            skeleton_group = glyph.getchildren()[0]
            segment_group = glyph.getchildren()[1]
            
            skels = []
            for path in skeleton_group.getchildren():
                points = []
                d = simplepath.parsePath(path.get('d'))
                for _, p in d:
                    points.append(p)
                
                skels.append(np.array(points))
                
            outlines = []
            
            for path in segment_group.getchildren():
                #points = []
                p = cubicsuperpath.parsePath(path.get('d'))
                cspsubdiv.cspsubdiv(p, self.options.flat)
                poly = []
                for sp in p[0]:
                    poly.append ([sp[1][0], sp[1][1]])
                outlines.append(poly)        
                
            transform = glyph.get('transform')
            if not transform is None:
                offset = literal_eval(transform[len('translate'):])
            else:
                offset = np.zeros(2,dtype=float)
                
            #              0         1        2            3           4
            glyphs[ch] = (offset, skels, skeleton_group, outlines, segment_group)
            
        return glyphs
        
    def getPartIndices(self, chars, parts):
        '''
        get the indices of the parts that contain chars
        '''
        indices = []
        for px, part in enumerate(parts):
            for ch in chars:
                if ch in part:
                    indices.append(px)
                    break
                    
        return np.array(indices)    
        
        
    def effect(self):
        """
        Effect behaviour.
        Overrides base class' method and inserts "Hello World" text into SVG document.
        """
        svg = self.document.getroot()
        ns = {'svg':'http://www.w3.org/2000/svg'}
        
        #g = svg.find(".//svg:g[@id='layer1']", namespaces = ns)
        glyphs = self.parseGlyphs(svg)
        self.init_chars = self.options.init_chars
        self.missing_chars = []
        self.all_chars = glyphs.keys()
        
        for ch in glyphs.keys():
            #if a glyph has outlines, it becomes the "key character"
            if not ch in self.init_chars:
                self.missing_chars.append( ch )
        
        if self.options.mode == 'snap':
            segments = [(idd, segment) for idd, segment in self.selected.iteritems()]
            segment = segments[0][1]
            #segment = svg.find("./svg:g[@id='layer1']/svg:path", namespaces = ns)
            if segment.tag == inkex.addNS('path','svg'):
                graph, poly = self.cubic2graph(segment.get('d'))
                path = self.findSkeleton(graph)
                
                points = np.array([graph.node[p1]['pos'].tolist() for p1 in path])
                pathdata = zip(['M'] + ['L'] * (len(points) - 1) + ['Z'], [list(point) for point in points])
    
                #Snap the new segment to a skeleton part
                best_part, best_char, min_d = 0, '', 1e10
                for ch in self.all_chars:
                    skels = glyphs[ch][1]
                    offx, offy = glyphs[ch][0]
                    offset = np.array([offx, offy])
                    for sx, skel in enumerate(skels):
                        d = Distance.hausdorff(skel +  offset, points , mode=1)
                        if d < min_d:
                            best_part = sx
                            best_char = ch
                            min_d = d
                            
                #---now that we know which part the segment belongs, we just bring it to the right group
                if not best_char in self.init_chars:
                    self.init_chars.append(best_char)
                    
                offset = np.array(glyphs[best_char][0])
                segment_group = glyphs[best_char][-1]
                #simplepath.translatePath(cubic_p, -offset[0], -offset[1])
                poly = np.array(poly) - offset
                polydata = zip(['M'] + ['L'] * (len(poly) - 1), [list(point) for point in poly])
                idd =  best_char + str(best_part).zfill(2) + '_seg'
                
                segment1 = segment_group.find("./svg:path[@id='%s']" % idd, namespaces=ns)
                if  segment1 is None:
                    segment1 = deepcopy(segment)
                    segment_group.append(segment1)
                segment1.set('d', simplepath.formatPath(polydata))
                segment1.set('id', idd)
                
                segment1.set('style', simplestyle.formatStyle({'fill': \
                mlt.colors.cnames[Common.color_names[best_part + \
                self.init_chars.index(best_char) * 10 ]], \
                'opacity' : '0.5'}))

                #---now modify the skeleton_group and add the new partial skeleton 
                skeleton_group = glyphs[best_char][2]
                skel = skeleton_group.find("./svg:path[@id='%s_skel']" \
                % (best_char + str(best_part).zfill(2)), namespaces=ns )
                
                simplepath.translatePath(pathdata, -offset[0], -offset[1])
                skel.set('d', simplepath.formatPath(pathdata))
        
        elif self.options.mode == 'synthesize':
            #First infer the copying rules for entire font
            fonts = []
            for i in range(len(Common.all_chars)):
                fonts.append([])
                
            for ch in self.init_chars:
                chx = Common.all_chars.index(ch)
                glyph = glyphs[ch]
                
                #for each segment of the character
                for childx, child in enumerate(glyph[4].getchildren()):
                    graph, poly = self.poly2graph(child.get('d'))
                    #if childx == 2:
                    #    vz.draw_graphs([graph])
                    path = self.findSkeleton(graph)
                    for v1, v2, data in graph.edges_iter(data=True):
                        if data['mode'] == CGAL.CONTOUR:
                            graph.remove_edge(v1, v2)
                    #extracting stroke from the graph
                    segments, _, datapoints = Segmentation.segment_graph(graph, \
                    None, [path], modes=['stroke'])
                    
                    fonts[chx].append(datapoints[0])
                    
            chrange = [Common.all_chars.index(ch) for ch in self.init_chars]
            parts, W = DataSource.gen_vocabs(np.arange(26, 52), 'outline')
            pWs = DataSource.prepare_dataset(Common.PATH, mode='outline', dist='emd', \
            chrange=chrange,files=[],prefix='tmp.',fonts=[('test_font',fonts)],W=W, parts=parts, normalized=False)
            rules = self.inferRules(pWs[0], self.init_chars)
            
            #--- Now transfer the style according to the rules
            for ch in self.missing_chars:
                skels = glyphs[ch][1]
                chx = Common.all_chars.index(ch)
                #Number of strokes for each character
                n_strokes = Common.mains[chx]
                dst_ids = self.getPartIndices([ch],self.parts['outline'])
                for st in range(n_strokes):
                    #Source 
                    src_id = rules['outline'][dst_ids[st]]
                    src_char = self.parts['outline'][src_id][0]
                    schx = Common.all_chars.index(src_char)
                    src_part = int(self.parts['outline'][src_id][1:])
                    src_skel = fonts[schx][src_part][0:2]
                    src_outline = fonts[schx][src_part][2:4]
                    
                    #Destination 
                    dst_skel = skels[st]
                    dst_outline = sz.transfer_style_graph(src_skel, dst_skel, \
                    src_outline=src_outline, method='stretch', smooth=False, meta=None)
                    
                    outline_group = glyphs[ch][4]
                    outline = np.concatenate([dst_outline[0], dst_outline[1][::-1]])
                    
                    style = simplestyle.formatStyle({'fill': \
                    mlt.colors.cnames[Common.color_names[src_part + \
                    self.init_chars.index(src_char) * 10 ]], 'opacity' : '0.5'})
                    
                    self.addPolyElement(outline, outline_group , idd = self.getIdd(ch, st, '_seg'), \
                    style=style, replace=False)
                    
                    
# Create effect instance and apply it.
effect = FontEditor()
effect.affect()
