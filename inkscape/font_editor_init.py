# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:21:38 2015

@author: phan
"""

#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/local/share/inkscape/extensions')
sys.path.append('/media/phan/80ae5e79-4620-48ec-b0fc-05bf33feaa62/phan/workspace/coolfont/coolfont')

from coolfont import Common as cm
import numpy as np
import networkx as nx
from os.path import join
import os
import simplepath
# We will use the inkex module with the predefined Effect base class.
import inkex
# The simplestyle module provides functions for style parsing.
from simplestyle import *
from shapely.geometry import *
from coolfont import Geometry as geo
from StringIO import StringIO
import matplotlib as mlt

class FontEditorInit(inkex.Effect):
    """
    Example Inkscape effect extension.
    Creates a new layer with a "Hello World!" text centered in the middle of the document.
    """
    def __init__(self):
        """
        Constructor.
        Defines the "--what" option of a script.
        """
        # Call the base class constructor.
        inkex.Effect.__init__(self)

        # Define string option "--what" with "-w" shortcut and default value "World".
        self.OptionParser.add_option('-i', '--init_chars', action = 'store',
          type = 'string', dest = 'init_chars', default = 'EOM',
          help = 'Initial characters to generate?')
          
        self.OptionParser.add_option('-f', '--fontname', action = 'store',
          type = 'string', dest = 'fontname', default = 'AguafinaScript-Regular.ttf.npz',
          help = 'template font?')
          
        self.OptionParser.add_option('-c', '--chrange', action = 'store',
          type = 'string', dest = 'chrange', default = 'upper',
          help = 'upper/lower/all characters?')
          
        path = join(os.getcwd(), "data")
        self.templates = []
        self.fontnames = []
        #if self.options.chrange == 'upper':
        chrange = np.arange(0, 26)
        
        tmpdata = np.load(join(path, 'word_specs.npz'))
        specs = tmpdata['specs'] #all parts, including the caps
        mains = tmpdata['mains'] #only the main parts (strokes)
        self.word_specs = zip(specs, mains)
        
        
        #Prepare all the templates
        if os.path.isfile(join(path, 'all_templates.npz')):
            data = np.load(join(path, 'all_templates.npz'))
            self.templates = data['all_templates']
            self.fontnames = list(data['fontnames'])
        else:
            files = sorted([f for f in os.listdir(join(path, 'data')) if f.endswith('.npz')])[:400]
            for f in files:
                data = np.load(join(path, 'data', f))
                if 'points' in data:
                    
                    #elif mode == 'outline':                
                    points = data['points']
                    specs_ = np.array([len(a) for a in points])
                    if np.all(specs_[chrange] >= specs[chrange]):
                        print f
                        anns = data['annotations']
                        graphs = data['graphs']
                        points = [[]] * len(cm.all_chars)
                        for ax, ann in enumerate(anns): #every character
                            graph = graphs[ax]
                            pss = []
                            for seg in ann: #every segment
                                try:
                                    ps = [list(graph.node[p]['pos']) for p in seg]
                                except:
                                    pass
                                pss.append(ps)
                                
                            points[ax] = pss       
                                       
                        self.templates.append(points)
                        self.fontnames.append(f)
            np.savez(join(path,'all_templates.npz'), all_templates=self.templates,fontnames=self.fontnames)
            
            
    
    def makeMarker(self,defs):
        marker = inkex.etree.SubElement(defs, 'marker')
        marker.set(inkex.addNS('stockid','inkscape'),'DotL')
        marker.set('orient', 'auto')
        marker.set('refY', '0.0')
        marker.set('refX', '0.0')
        marker.set('id', 'DotL')
        marker.set('style', 'overflow:visible')
        marker.set(inkex.addNS('isstock', 'inkscape'), 'true')
        
        path = inkex.etree.SubElement(marker, 'path')
        #path.set('id', 'path_circle')
        path.set('d', 'M -2.5,-1.0 C -2.5,1.7600000 -4.7400000,4.0 -7.5,4.0 C -10.260000,4.0 -12.5,1.7600000 -12.5,-1.0 C -12.5,-3.7600000 -10.260000,-6.0 -7.5,-6.0 C -4.7400000,-6.0 -2.5,-3.7600000 -2.5,-1.0 z ')
        path.set('style', "fill-rule:evenodd;stroke:#ff0000;stroke-width:1pt;stroke-opacity:1;fill:#ff0000;fill-opacity:1")
        path.set('transform', "scale(0.8) translate(7.4, 1)")
        
    def effect(self):
        """
        Effect behaviour.
        Overrides base class' method and inserts "Hello World" text into SVG document.
        """
        
        # Get script's "--what" option value.
        init_chars = list(self.options.init_chars)
        fontname = self.options.fontname
        template = self.templates[ self.fontnames.index(fontname) ]
        

        # Get access to main SVG document element and get its dimensions.
        svg = self.document.getroot()
        # or alternatively
        # svg = self.document.xpath('//svg:svg',namespaces=inkex.NSS)[0]
        defs = inkex.etree.SubElement(svg, 'defs')
        self.makeMarker(defs)

        # Again, there are two ways to get the attibutes:
        width  = self.unittouu(svg.get('width'))
        height = self.unittouu(svg.attrib['height'])

        xshift = 0
        for ch in init_chars:
            # Create a new layer.
            layer = inkex.etree.SubElement(svg, 'g')
            layer.set('id', ch)
            layer.set('class', 'glyph')
            #layer.set('sodipodi', 'true')        
            layer.set(inkex.addNS('label', 'inkscape'), '%s Layer' % (ch))
            layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
            
            #sodipodi:insensitive="true", freeze the layer
            #layer.set(inkex.addNS( 'insensitive','sodipodi'), 'true')
            #layer.set('style', 'display:none')
            #layer.set('sodipodi:insensitive', 'true')
            
            idx = cm.all_chars.index(ch)
            n_mains = self.word_specs[idx][1]
            skeldata = template[idx]
            
            tmp = np.concatenate(skeldata)
            #bbox = np.array([tmp.min(axis=0), tmp.max(axis=0)])
            
            layer.set('transform','translate(%d,0) scale(1,-1)' % xshift)
            xshift += tmp.max(axis=0)[0] + 100
            
            #The group of part skeletons
            skeleton_group = inkex.etree.SubElement(layer, 'g')
            skeleton_group.set('sodipodi', 'true')
            #skeleton_group.set(inkex.addNS( 'insensitive','sodipodi'), 'true')
            skeleton_group.set('id', ch + '_skels')
            
            #The group of part segments
            segment_group = inkex.etree.SubElement(layer, 'g')
            segment_group.set('id', ch + '_segs')
            #segment_group.set('sodipodi', 'true')
            #segment_group.set(inkex.addNS( 'insensitive','sodipodi'), 'true')
            
            sparts = []
            #---Add the skeleton stroke
            for px, part in enumerate(skeldata[:n_mains]):
                spart = geo.sample_polyline(part, step=100.)#n_samples=20)
                #spart = spart[1:-1]
                sparts.append(spart)
                skel = inkex.etree.Element(inkex.addNS('path','svg'))
                
                d = zip(['M'] + ['L'] * (len(spart) - 1) + ['Z'], [list(point) for point in spart])
                #simplepath.scalePath(d, 1, -1)
                #;marker-start:url(#DotL);marker-end:url(#DotL)
                skel.set('style', 'fill:none;stroke:%s;stroke-opacity:1;stroke-width:8;stroke-miterlimit:4;stroke-dasharray:8,16;stroke-dashoffset:0' % mlt.colors.cnames[ cm.color_names[px] ]) 
                skel.set('id', ch + str(px).zfill(2) + '_skel')
                skel.set('d', simplepath.formatPath(d))
                #skel.set('data', str(np.array([list(point) for point in spart], dtype=int).flatten())[1:-1])
                s = StringIO('')
                np.savetxt(s, spart, fmt='%.2f', newline=';')
                skel.set('data', s.getvalue())
                #skel.set('x', str(width / 2))
                #skel.set('y', str(height / 2))
                
                skeleton_group.append(skel)
                
            #---Add cap anchors    
            for px, part in enumerate(skeldata[n_mains:]):
                anchors_ = cm.anchors[ch + str(px + n_mains).zfill(2)]
                ch_, pt_, tm_ = anchors_[0][0][0], int(anchors_[0][0][1:]), anchors_[0][1]
                if tm_ == 1: tm_ = -1
                anchor = sparts[pt_][tm_]
                
                skel = inkex.etree.Element(inkex.addNS('circle','svg'))
                skel.set('id', ch + str(px + n_mains).zfill(2) + '_skel')
                skel.set('cx', str(anchor[0]))
                skel.set('cy', str(anchor[1]))
                skel.set('r', str(20))
                skel.set('style', 'fill:#FA8072')
                #skel.set('d', 'M %.4f %.4f L %.4f %.4f' % (anchor[0],anchor[1],anchor[0],anchor[1]))
                skel.set('data', str(anchor)[1:-1])
                
                skeleton_group.append(skel)                
                
        # Create text element
        #text = inkex.etree.Element(inkex.addNS('text','svg'))
        #text.text = 'Hello %s!' % (what)

        # Set text position to center of document.
        #text.set('x', str(width / 2))
        #text.set('y', str(height / 2))

        # Center text horizontally with CSS style.
        #style = {'text-align' : 'center', 'text-anchor': 'middle'}
        #text.set('style', formatStyle(style))

        # Connect elements together.
        
if __name__ == '__main__':
    # Create effect instance and apply it.
    effect = FontEditorInit()
    #effect.options.fontname = 'Alegreya-Regular.ttf'
    #effect.options.init_chars = 'EOM'
    #effect.options.chrange = 'upper'
    effect.affect()
