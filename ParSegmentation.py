# -*- coding: utf-8 -*-
"""
Created on Tue Jul  8 15:29:06 2014

@author: phan
"""

#!/usr/bin/python

import threading
import time
from Segmentation import segment_vector, segment_graph
import copy
import numpy as np
import Common as cm
from os.path import join
import multiprocessing

def segment_worker(thread_id, filename, character, outline, ann, mirror, \
search_vector_factor, angle_diff, nbsize, shearing):
    segments, caps = segment_vector(None, None, outline, ann, mirror=mirror, \
    search_vector_factor=search_vector_factor, angle_diff=angle_diff, nbsize=nbsize, \
    shearing=shearing)
    
    save(thread_id, segments, caps, None, ann, None , filename, character)
    
def segment_graph_worker(thread_id,filename, character, graph, outline, ann, mirror, \
search_vector_factor, angle_diff, nbsize, shearing):
    modes = None
    segments, caps, points = segment_graph(graph, outline, ann, modes)
    #mirror=mirror, \
    #search_vector_factor=search_vector_factor, \
    #angle_diff=angle_diff, nbsize=nbsize, shearing=shearing)
    
    if len(segments) == 0 or len(caps) == 0:
        return
    
    for seg in segments:
        if len(seg) == 0:
            return
    #for cap in caps:
    #    if len(cap) == 0:
    #        return
        
    save(thread_id, segments, caps, graph, ann, points, filename, character)
        
def save(thread_id, segment, cap, graph, ann, point, filename, character):
    #print 'saving segments'
    idx = cm.all_chars.index(character)
    changes = []
    changes.append((idx, 'points', point))
    changes.append((idx, 'graphs', graph))
    changes.append((idx, 'caps', cap))
    changes.append((idx, 'segments', segment))
    changes.append((idx, 'annotations', ann))
    changes = np.array(changes, dtype='object')
    np.savez(join('.tmp', str(thread_id) + '.npz'), filename=filename, changes=changes)
        
class SegmentationThread (threading.Thread):
    def __init__(self, ch, obj, thread_lock, threadID=0):
        super(SegmentationThread, self).__init__()
        self.threadID = threadID
        self.obj = obj
        self.ch = ch #copy.copy(ch)
        self.mirror = obj.chbxMirror.isChecked()
        self.search_vector_factor = obj.spnSearchRange.value()
        self.angle_diff = obj.spnAngleDiff.value()
        self.shear = obj.spnShear.value()
        self.nbsize = obj.spnNbsize.value()
        self.thread_lock = thread_lock
        #self.path = ch.pathData()
        self.daemon = True
        
    def run(self):
        print "Starting " + str(self.threadID) + ' - ' + str(self.ch.character) \
        + ' - ' + self.ch.filename
        obj = self.obj
        #ann = self.ch.annsData()
        outline = self.ch.outlineData()
        start = time.time()
        '''
        p = multiprocessing.Process(target=segment_worker, args = \
        (self.threadID, self.ch.filename, self.ch.character, outline, ann, self.mirror, \
        self.search_vector_factor, self.angle_diff, self.nbsize, self.shear))
        '''
        p = multiprocessing.Process(target=segment_graph_worker, args = \
        (self.threadID, self.ch.filename, self.ch.character, self.ch.graphData(), outline, self.ch.annsData(), self.mirror, \
        self.search_vector_factor, self.angle_diff, self.nbsize, self.shear))
                
        '''
        segments, caps = segment_vector(self.ch.filename, \
                                  str(self.ch.character), \
                                  outline, \
                                  ann, \
                                  mirror=self.mirror, \
                                  search_vector_factor=self.search_vector_factor, \
                                  angle_diff=self.angle_diff, \
                                  nbsize=self.nbsize, \
                                  shearing=self.shear)
        print segments, caps
        
        for sg in segments:
            print 'Error in segmentation of %s, %s' % (self.ch.filename, self.ch.character)
            if len(sg) == 0:
                return
        '''
        p.start()
        p.join()
        obj.threads[self.threadID] = -1
        print time.time() - start, 'seconds', str(self.threadID)
        #print 'Finished ' + str(self.threadID)

if __name__ == '__main__':
    import os, math #, multiprocessing, math
    path = '/media/phan/BIGDATA/SMARTFONTS'
    data_path = join(path, 'data')
    os.listdir(path)
    files = sorted([f for f in os.listdir(data_path) \
    if os.path.isfile(join(data_path, f)) and f.endswith('.npz')])
    specs = np.load(join(path, 'word_specs.npy'))
    mirror = False
    search_vector_factor = 400
    angle_diff = 10
    nbsize = 1
    shear = 0
    for f in files:
        print 'processing ', f
        data = np.load(join(data_path, f))
        if 'annotations' in data and 'graphs' in data:
            skip = False
            annotations = data['annotations']
            filename = join(data_path, f)
            graphs = data['graphs']
            outlines = data['outlines']
            chars = data['chars']
            specs_ = np.array([len(a) for a in annotations])
            if np.all(specs_ >= specs):
                tid = 0
                segments, caps, points = [], [], []
                for chx in range(len(chars)):
                    if chx >= 26 and chx <= 51:
                        seg, cap, pt = segment_graph(graphs[chx], outlines[chx], annotations[chx], \
                                                  mirror=mirror, \
                                                  search_vector_factor=search_vector_factor, \
                                                  angle_diff=angle_diff, nbsize=nbsize, shearing=shear)
                        if len(seg) == 0 or len(cap) == 0:
                            skip  = True
                        
                        for s in seg:
                            if len(s) == 0:
                                skip  = True
                                
                        for c in cap:
                            if len(c) == 0:
                                skip  = True      
                                
                        if skip: 
                            print 'Invalid Fonts! ', f
                            break                  
                    else:
                        seg, cap, pt = [], [], []
                        
                    segments.append(seg)
                    caps.append(cap)
                    points.append(pt)
                
                if not skip:    
                    cm.modify_npz(filename, [('segments', segments), ('caps', caps), ('points', points)])