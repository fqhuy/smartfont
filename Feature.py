# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 21:24:48 2014

@author: phan
"""
import numpy as np
from scipy import interpolate, signal
from sklearn.decomposition import PCA
from sklearn.neighbors import NearestNeighbors
from fast import histogram as fast_hist
from sklearn import cluster
from pyemd import emd

import scipy.spatial.distance as dist
import Segmentation as sg
from os.path import join
from shapely.geometry import *
from shapely.affinity import translate
import shapely
from Geometry import Angle as geoa
from Geometry import ave_normal
import Common as cm
from Common import neighbours
import geometry as geo
from scipy.spatial.distance import euclidean as euc
import time, math
from math import sin, cos
from gicp import gicp
import cv2
from fast.Distance import hausdorff

def build_dictionary(data, n_clusters):
    '''
    quantizing data
    '''
    X = data  # We need an (n_sample, n_feature) array
    k_means = cluster.KMeans(n_clusters=n_clusters, n_init=5, verbose=1,n_jobs=-1)
    k_means.fit(X)
    #values = k_means.cluster_centers_.squeeze()
    #labels = k_means.labels_
    return k_means.cluster_centers_

def simple_hausdorff(points1, points2):
    points1 = np.array(points1)
    points2 = np.array(points2)
    center1 = points1.mean(axis=0)
    center2 = points2.mean(axis=0)
    offset = center1 - center2
    points2 += offset
    return hausdorff(points1, points2, mode=1)

def simple_overlap(points1, points2):
    ply1 = asPolygon(points1)
    ply2 = asPolygon(points2)
    #points1 = np.array(points1)
    #points2 = np.array(points2)
    
    center1 = np.array(ply1.centroid)
    center2 = np.array(ply2.centroid)
    
    offset = center1 - center2
    translate(ply2, offset[0], offset[1])
    #ply1.overlaps(ply2)
    a = max(ply1.area, ply2.area )
    return ply1.intersection(ply2).area / a    

def overlap(points1, points2):
    if len(points1) == 0 or len(points2) == 0 or len(points1.shape) != 2 or len(points2.shape) !=2:
        print '#',
        return 0.5
    
    _points1 = np.asarray(points1)
    points2 = np.asarray(points2)
    flips = [(1,1),(1,-1),(-1,1)]
    #rots = []
    #for ang in [0]:#, np.pi/2, -np.pi/2]:
    #    rots.append( np.array([[cos(ang), -sin(ang)], [sin(ang), cos(ang)]]) )
        
    ds = []
    #for rot in rots:
    for flx, fly in flips:
        points1 = _points1.copy()
        #flip
        points1[:,0] *= flx
        points1[:,1] *= fly
        #rorate
        #points1 = rot.dot(points1.T).T
        #move
        move = (-points1.mean(axis=0) + points2.mean(axis=0))
        points1 += move
        corres = np.zeros((len(points1), 3),dtype='double')
        if len(points1) >= 5 and len(points2) >= 5:
            re = gicp.icp(points1, points2, corres, 200.0, 1e-2)
            points1 = np.hstack((points1, np.tile([0,1], points1.shape[0]).reshape(points1.shape[0], 2)))
            t_points1 = re.dot(points1.T).T[:,0:2]
        else:
            t_points1 = points1
        
        ply1 = asPolygon(t_points1)
        ply2 = asPolygon(points2)
        a = max(ply1.area, ply2.area )
        ds.append(ply1.intersection(ply2).area / a)
        
    return max(ds) #** 2
    
def earthmover(x, y, D):
    if D == None:
        nbins = len(x)
        D = np.ones((nbins, nbins),dtype='double') * nbins
        nbins = nbins / 2
        A = np.abs( np.repeat(np.arange(0, nbins, dtype='float64')[:,np.newaxis], nbins,axis=1 ) - np.repeat(np.arange(0, nbins, dtype='float64')[:,np.newaxis], nbins,axis=1 ).T) #/ 99.
        #qbins = nbins / 2
        B = np.zeros_like(A, dtype='double')
        for i in range(nbins):
            for j in range(i):
                B[i, j] = min( abs(i - j), nbins - abs(i - j))
                B[j, i] = B[i, j]
                    
        D[0:nbins, 0:nbins] = A
        D[nbins:nbins*2, nbins:nbins*2] = B
        
    return emd(x, y, D)

def histogram_intersection(x,y):
    assert(len(x) == len(y))
    return np.fmin(x,y).sum()
    
def normalized_euclidean(x, y, stds):
    assert(len(x) == len(y))
    sm = 0
    for d in range(len(x)):
        if stds[d] != 0:
            sm += ((x[d] + y[d])**2) / stds[d]**2
        
    return np.sqrt(sm)

def polyline_to_points(polyline, d = 2):
    '''
    '''
    first = np.asfarray(polyline[0])
    points = []
    for second in polyline[1:]:
        second = np.asfarray(second)
        vec = np.asfarray(second - first)
        norm = np.linalg.norm(vec)
        vec = (vec / norm) * d
        points.append(first.copy())
        while np.linalg.norm(first - second) > d:
            #print np.linalg.norm(first - second)
            first += vec
            points.append(first.copy())
            
        first = second.copy()
    
    return np.array(points)

def _calc_feature_skel(points, n_samples=51, normalized=False):
    '''
    calculate the feature vector of a simple polyline using curvature, normal vector
    '''
    if len(points) == 3:
        k = 2
    elif len(points) > 3:
        k = 2
    elif len(points) == 2:
        v = points[1] - points[0]
        v /= np.linalg.norm(v)
        a = np.arctan2([v[1]],[v[0]])
        return np.array([[0, a]] * n_samples)
    else:
        print 'invalid points, len=', len(points),
        return np.array([[-1, 0]] * n_samples)
    
    
    u = np.arange(0, 1.01, 0.02)
    newpoints = []
    for i in range(len(points) - 1):
        if not np.allclose(points[i], points[i + 1], atol=1e-5):
            newpoints.append(points[i])
            
    newpoints.append(points[-1])
    newpoints = np.array(newpoints)        
    weights = (1 - signal.gaussian(len(newpoints), 4)) + 1e-10
    
    #try:
    tck, v = interpolate.splprep([newpoints[:, 0], newpoints[:, 1]],w=weights, s=len(u), k=k)
    #except Exception, e:
    #    print e 
    #    return None
    if n_samples <= 0:
        u = v
    #u = np.arange(0, 1.01, 0.02)
    #de0 = np.array(interpolate.splev(u, tck, der=0))
    
    de1 = np.array( interpolate.splev(u, tck, der=1) ).T
    de2 = np.array( interpolate.splev(u, tck, der=2) ).T
    
    curvs = (de1[:, 0] * de2[:, 1] - de1[:, 1] * de2[:, 0]) / (de1[:, 0]**2 + de1[:, 1]**2) 
    norms =  1 / np.sqrt((de1[:, 0]**2 + de1[:, 1]**2)) * np.vstack([-de1[:, 1], de1[:, 0]])
    #angs = np.arctan2( norms[1] , norms[0] )
    angs = np.abs( np.arctan( norms[1] / norms[0] ) )
    #'''
    if normalized:
        curvs = np.array(curvs)
        curvs  = (1 - np.e ** (- 5e-2 * curvs ** 2)) * np.sign(curvs)
        #curvs /= cm.get_max_val('curvature')
        angs /= cm.get_max_val('angle')
        
    #'''    
    data = np.vstack([ curvs, angs]).T
    #data = np.vstack([ curvs, angs]).T
    return data

def _calc_dhist_feature_skel(points, dictionary, norm_params):
    feats = _calc_feature_skel(points)
    gamma = 1
    mean, minva, maxva = tuple(norm_params)
    for feat in feats:
        #curvature
        feat[0] = 1 - math.e ** (-0.5 * gamma * (feat[1] - mean) ** 2)
        #normals
        feat[1] = (feat[0] - minva) / (maxva - minva)    
    
    hist = fast_hist.calc_histogram(feats, dictionary,order=1)
    return hist

    #hist1,_ = np.histogram(data[:, 0], bins=50, range=(-50,50), density=True)
    #hist2,_ = np.histogram(data[:, 1], bins=50, range=(-np.pi, np.pi),density=False)
    #hist2 = hist2 / float(len(data[:, 1]))
    #hist = h.calc_histogram(data, dictionary, 2)
    #hist = np.histogram(features,bins=25,range=(0,100),normed=True)
    #hist = np.asfarray( np.concatenate([hist1, hist2]) ) 
    #return hist / float( hist.sum() )
    #return hist
    
def _calc_feature_thickness(points):
    '''
    calculate simple thickness feature
    '''
    feat = []
    for i in [0, 1]:
        if len(points[i]) > 1:
            outline = asLineString(points[i]).simplify(0.05)
        else:
            outline = asPoint(points[i][0])
        
        if len(points[i + 2]) > 1:
            skel = asLineString(points[i + 2]).simplify(0.05)
        else:
            skel = asPoint(points[i+2][0])
            
            
        if isinstance(skel, shapely.geometry.point.Point) or \
        isinstance(outline, shapely.geometry.point.Point):
            feat.extend([outline.distance(skel)] * 100)
        else:    
            u = 0.0
            while u < 1.0:
                point = outline.interpolate(u)
                v = skel.project(point)
                ppoint = skel.interpolate(v)
                d = euc(np.array(point), np.array(ppoint))
                feat.append(d)
                u += 0.01
            
    #feat -= feat.mean()
    #feat /= feat.std()
    return feat

    
def _calc_feature_stroke(points, mode='stroke', n_samples=0, normalized=False):
    '''
    extract a feature from strokes
    @mode stroke or cap.
    @points [left, right, mid_left, mid_right] for stroke mode. [point] for cap mode
    '''
    k = 2
    #n_samples = 0
    data = []
    weights = None #(1 - signal.gaussian(len(newpoints), 4)) + 1e-10
    s = 0 #len(u)
    if mode == 'stroke':
        for i in [0, 1]:
            #clean up repetitive points (why?)
            newpoints = []
            for j in range(len(points[i]) - 1):
                if not np.allclose(points[i][j], points[i][j + 1], atol=1e-5):
                    newpoints.append(np.concatenate([points[i][j], points[i + 2][j]]))
            
            newpoints.append(np.concatenate([points[i][-1], points[i + 2][-1]]))
            newpoints = np.array(newpoints)
            thickness = [euc(newpoints[p][0:2], newpoints[p][2:4]) for p in range(len(newpoints))]
            
            if len(newpoints) <= k:
                #data.append( np.vstack([ np.zeros(n_samples) + thickness[0], np.zeros(n_samples)]).T )
                thickness = np.zeros(n_samples) + thickness[0]
                curvs = np.zeros(n_samples)
            else:
                tck, v = interpolate.splprep([newpoints[:, 0], newpoints[:, 1]],w=weights, s=s, k=k)
                if n_samples <= 0:
                    u = v
                else:
                    u = np.arange(0, 1.0, 1. / n_samples)
                    
                tck1, u1 = interpolate.splprep([range(len(thickness)), thickness],w=weights, s=s, k=k)
                thickness = np.array( interpolate.splev(u, tck1, der=0)).T[:, 1]
                
                #de0 = np.array(interpolate.splev(u, tck, der=0))
                de1 = np.array( interpolate.splev(u, tck, der=1) ).T
                de2 = np.array( interpolate.splev(u, tck, der=2) ).T
                
                curvs = (de1[:, 0] * de2[:, 1] - de1[:, 1] * de2[:, 0]) / (de1[:, 0]**2 + de1[:, 1]**2) 
                #norms =  1 / np.sqrt((de1[:, 0]**2 + de1[:, 1]**2)) * np.vstack([-de1[:, 1], de1[:, 0]])
                #thickness = [euc(points[i][p], points[i + 2][p]) for p in range(len(points[i]))]
                
            if normalized:
                curvs = np.array(curvs)
                curvs  = (1 - np.e ** (-5e-2 * curvs  ** 2)) * np.sign(curvs)
                #curvs[curvs > cm.get_max_val('curvature')] = cm.get_max_val('curvature')
                #curvs /= cm.get_max_val('curvature')
                
                thickness[thickness > cm.get_max_val('thickness')] = cm.get_max_val('thickness')
                thickness /= cm.get_max_val('thickness')
                    
            data.append( np.vstack([ thickness, curvs]).T )        

        return np.concatenate(data)
            
    elif mode == 'cap':
        #simply take out the 
        if len(points) > 2:
            return points
        else:
            return None
    
def _calc_dhist_feature_graph(points, dictionary, mode='stroke',norm_params=None):
    '''
    simply extract the distances out of points array
    points = [left_outer, right_outer, left_inner, right_inner]
    '''
    #dists = []
    feats = _calc_feature_stroke(points, mode=mode)
    if norm_params!=None:
        gamma = 1
        mean, minva, thresh = tuple(norm_params)
        for feat in feats:
            feat[1] = 1 - math.e ** (-0.5 * gamma * (feat[1] - mean) ** 2)
            feat[0] = (min(feat[0], thresh) - minva) / (thresh - minva)
                
    hist = fast_hist.calc_histogram(feats, dictionary,order=1)
    #hist =  #np.histogram(feats,bins=100,range=(0,500),normed=True)
    return hist

def _calc_dhist_feature_vector(segment, polyline, step=20, scale=1):
    pos = 0
    points = []
    p0 = Point(polyline[0])
    polyline_ = asLineString(polyline)
    #print 'seg'
    while not p0.distance(Point(polyline[-1])) < cm.eps:
        p0 = polyline_.interpolate(pos)
        pos += step
        points.append(p0)
        
    length = polyline_.length
    contour = asLineString(segment)
    features = []
    for px in range(len(points)):
        #print px
        p = [np.array(points[px]), None, None]
        nbs = [np.array(j) for j in neighbours(points, px, scale)]
        #n_p0 = geo.ave_normal(np.ascontiguousarray(nbs, dtype='float64') )#ave_normal(nbs, geoa)
        n_p0 = ave_normal(nbs, geoa)
        
        vec1 = n_p0 * length  + p[0]
        vec2 = -n_p0 * length  + p[0]
        
        try:
            p[1] = np.array(LineString([p[0], vec1]).intersection(contour))
            p[2] = np.array(LineString([p[0], vec2]).intersection(contour))
        except:
            continue
            print 'err'
            
        valid = True
        for i in [1,2]:
            if len(p[i].shape) == 2:
                p[i] = sorted(list(p[i]), key=lambda x: Point(x).distance(Point(p[0])))[0]
            elif len(p[i]) == 0:
                #print 'Warning: missing points'
                #p[i] = p[0]
                valid = False
        if valid:
            features.append(np.linalg.norm(p[2] - p[1]))   
        
    hist = np.histogram(features,bins=100,range=(0,500),normed=True)
    return np.array(hist)
    
def _calc_dhist_feature(image, polyline, idd):
    '''
    image: segmented image
    polyline: skeleton of a stroke (part)
    '''
    d = 5
    first = np.asfarray(polyline[0])
    features = []
    image = image.copy()
    image[(image != idd)] = -1
    for second in polyline[1:]:
        second = np.asfarray(second)
        #print second
        vec = np.asfarray(second - first)
        norm = np.linalg.norm(vec)
        vec = (vec / norm)
        nvec1 = np.array([vec[1],-vec[0]])
        nvec2 = np.array([-vec[1],vec[0]])
        vec = vec * d
        
        while np.linalg.norm(first - second) > d:
            #print np.linalg.norm(first - second)
            first += vec
            #search along nvec1
            curr1 = first.copy()
            curr2 = first.copy()
            while image[int(curr1[1]),int(curr1[0])] != -1:
                #image[curr1[1],curr1[0]] = 9
                curr1 += nvec1
                if curr1[1] <=0 or curr1[0] <= 0 or \
                curr1[1] >= image.shape[0] or curr1[0] >= image.shape[1]:
                    break
            #search along nvec2
            while image[int(curr2[1]),int(curr2[0])] != -1:
                #image[curr2[1],curr2[0]] = 9
                curr2 += nvec2
                if curr2[1] <= 0 or curr2[0] <= 0 or \
                curr2[1] >= image.shape[0] or curr2[0] >= image.shape[1]:
                    break
                
            dis = np.linalg.norm(curr2 - curr1)
            features.append(dis)
            
        first = second.copy()
    hist = np.histogram(features,bins=25,range=(0,100),normed=True)
    return np.array(hist)

def calc_dhist_feature_skel(points, dictionary, norm_params):
    feats = []
    for seg in points:
        feat = _calc_dhist_feature_skel(seg, dictionary, norm_params)
        feats.append(feat)
    return feats    

def calc_dhist_feature_graph(points,dictionary,modes,norm_params):
    feats = []
    for segx, seg in enumerate(points):
        #feat = _calc_dhist_feature_graph(seg, dictionary, modes[segx])
        if modes[segx] == 'stroke':
            tmp = _calc_feature_stroke(seg, mode=modes[segx])
            if norm_params!=None:
                gamma = 1
                mean, minva, thresh = tuple(norm_params)
                for feat in tmp:
                    feat[1] = 1 - math.e ** (-0.5 * gamma * (feat[1] - mean) ** 2)
                    feat[0] = (min(feat[0], thresh) - minva) / (thresh - minva)
                    
            hist = fast_hist.calc_histogram(tmp, dictionary,order=1)
        else:
            hist = seg #= _calc_feature_stroke(seg, mode=modes[segx])
            
        feats.append(hist)
        
    return feats

def calc_dhist_feature_vector(segments, annotations):
    '''
    calculate histograms for all segments of a character
    '''
    start = time.time()
    feats = []
    assert(len(segments) == len(annotations))
    for ann in range(len(annotations)):
        feat = _calc_dhist_feature_vector(segments[ann], annotations[ann])
        feats.append(feat[0])
    #print '.', 
    print time.time() - start
    return feats
    
def calc_dhist_feature(image, annotations):
    image = (image != 0)
    markers = np.zeros(image.shape)
    sg.mark(markers, annotations)
    result = sg.segment_simple(image, markers)
    feats = []
    
    for ann in range(len(annotations)):
        feat = _calc_dhist_feature(result,annotations[ann],ann+1)
        feats.append(feat[0])
        #print feat[0]
    
    #feat = _calc_dhist_feature(result,annotations[2],3)    
    #feats = np.asfarray(feats)
    return feats

'''    
if __name__ == '__main__':
    from matplotlib import pyplot as plt
    from skimage.io import imread
    import scipy.spatial.distance as dist
    path = '/home/phan/workspace/coolfont/dataset/output/'
    fname = 'Abril Fatface'
    chars = 'HM'
    feats =  {}
    for ch in chars:
        image = imread(join(path, '%s_%s.pgm' % (fname, ch)), as_grey=True)
        annotations = np.load(join(path, '%s_%s.pgm.npy' % (fname, ch)))
        feat = calc_dhist_feature(image, annotations)
        feats[ch] = feat

    #plt.bar(range(0,100,1),feats['H'][0])
    #plt.show()
'''

if __name__ == '__main__':
    #from matplotlib import pyplot as plt
    import os
    path = '/media/phan/BIGDATA/SMARTFONTS'
    data_path = join(path, 'data')
    n_clusters = 100
    chrange = (26, 52) #(0, 26)
    mode = 'skel' #outline, cap
    #fonts = []
    #fontnames = []
    X = []
    os.listdir(path)
    files = sorted([f for f in os.listdir(data_path) \
    if os.path.isfile(join(data_path, f)) and f.endswith('.npz')])
    word_specs = np.load(join(path, 'word_specs.npz'))
    specs = word_specs['specs']
    mains = word_specs['mains']
    all_feats = []
    
    #files = ['Alegreya-Regular.ttf.npz']
    extracted_files = []
    for f in files:
        feats = [[] for _ in range(len(cm.all_chars))]
        data = np.load(join(data_path, f))
        if 'points' in data:
            points = data['points']
            anns = data['annotations']
            graphs = data['graphs']
            
            valid = True
            specs_ = np.array([len(a) for a in points])
            if np.all(specs_[chrange[0]:chrange[1]] >= specs[chrange[0]:chrange[1]]):
                for ch in range(chrange[0], chrange[1]):
                    for pt in points[ch]:
                        if len(pt) == 0:
                            valid = False
            else:
                valid = False
            
            if valid:
                print f
                chars = data['chars']
                for ch in range(chrange[0], chrange[1]):
                    skels = [[graphs[ch].node[node]['pos'] for node in ann] for ann in anns[ch]]
                    
                    modes = ['cap'] * len(points[ch])
                    for i in range(mains[ch]):
                        modes[i] = 'stroke'
                    feat = [[] for _ in range(len(points[ch]))]
                    
                    print chars[ch], 
                    for prt in range(len(points[ch])):
                        if mode == 'skel':
                            ft = _calc_feature_skel( skels[prt] )
                            
                        elif mode == 'outline':
                            ft = _calc_feature_stroke(points[ch][prt],mode=modes[prt])
                            
                        if len(ft) == 0:
                            print '!',
                            
                        if ft != None:
                            feat[prt] = ft 
                            
                        if ft != None and (mode == 'outline' or mode == 'skel'):
                            X.append(ft)
                                                
                    feats[ch] = feat
                print
             
        if sum([len(ft) == 0 for ft in feats[chrange[0]:chrange[1]]]) == 0:
            extracted_files.append(f)
            all_feats.append(feats)
            
    np.savez(join(path, mode + '.feats.npz'), feats=all_feats, files=extracted_files)   
    X = np.vstack(X)
    
    mat = np.zeros((n_clusters, n_clusters),dtype='double')
    
    if mode == 'skel':
        mean = X[:, 0].mean()
        maxva, minva = X[:, 1].max(), X[:, 1].min() 
        gamma = 1
        X[:, 0]  = 1 - math.e ** (-0.5 * gamma * (X[:, 0] - mean) ** 2)
        X[:, 1] = (X[:, 1]- minva) / (maxva - minva)
        
        dic = build_dictionary(X, n_clusters=n_clusters)
        for i in range(len(dic)):
            for j in range(i):
                mat[i, j] = euc(dic[i], dic[j])
                mat[j, i] = mat[i, j]
                
        np.savez(join(path, mode + '.dict.npz'), dict=dic,mat=mat,mean=mean,minva=minva,maxva=maxva)
        
    elif mode == 'outline':
        mean = X[:, 1].mean()
        minva = X[:, 0].min()
        thresh = 300
        X[:, 0] = (min(X[:, 0], thresh) - minva) / (thresh - minva)
        X[:, 1] = 1 - math.e ** (-0.5 * gamma * (X[:, 0] - mean) ** 2)     
        dic = build_dictionary(X, n_clusters=n_clusters)
        for i in range(len(dic)):
            for j in range(i):
                mat[i, j] = euc(dic[i], dic[j])
                mat[j, i] = mat[i, j]        
        np.savez(join(path, mode + '.dict.npz'), dict=dic,mat=mat,mean=mean,thresh=thresh,minva=minva)
        
'''
if __name__ == "__main__":
    from matplotlib import pyplot as plt
    #from os.path import join
    from Common import all_chars
    from pyemd import emd
    #import os
    #import numpy as np
    #import Feature as ft
    
    path = '/media/phan/BIGDATA/SMARTFONTS'
    f = 'Alice-Regular.ttf.npz'
    data_path = join(path, 'data')
    dictionary = np.load(join(path, 'skel_dict.npy'))
    data = np.load(join(data_path, f))
    
    ch = all_chars.index('Y')
    prt =  [4,5] 
    plt.figure()
    id = 0
    hists =  []
    colors = ['red','green','blue','magenta']
    for p in prt:
        id += 1
        X =  np.array( data['points'][ch][p][2] )
        #if id == 1:
        #    X[:, 0] *= -1
             
        hist = _calc_dhist_feature_skel(X, dictionary)
        hists.append(hist)
        feat = _calc_feature_skel( X )
        
        ax1 = plt.subplot(100 + len(prt) * 10 + id)
        #ax1.scatter(feat[:, 0], feat[:, 1], c = colors[id-1])
        #ax1.bar(np.arange(0,100,1), hist)
        
        #ax1.plot(np.arange(0, 1.01, 0.02), feat[:, 0], '-x')
        #plt.plot(np.arange(0, 1.01, 0.02), feat[:, 1], '-o')
        plt.plot(feat[:,2], feat[:,3], '-x')
        plt.plot(X[:,0], X[:,1], '-o')
        
        #plt.legend(['Linear', 'Cubic Spline', 'True'])
        #ax1.axis([0, 86, 0, 1])
        #plt.axis([-0.05, 6.33, -1.05, 1.05])
        #plt.title('Cubic-spline interpolation')
        ax1.autoscale()
        
    print histogram_intersection(hists[0], hists[1]) #emd.emd(list(hists[0]), list(hists[1]), list(np.repeat(1., 200)), list(np.repeat(1., 200)))
    #D = np.abs( np.repeat(hists[0][:, np.newaxis], hists[0].shape[0],axis=1) \
    #                                       - np.repeat(hists[1][:, np.newaxis], hists[1].shape[0], axis=1).T)
    print earthmover(hists[0], hists[1])
    print np.abs( np.cumsum(hists[0],axis=0) - np.cumsum(hists[1],axis=0)).sum() 
    
    plt.show()
'''