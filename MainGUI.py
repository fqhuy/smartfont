# -*- coding: utf-8 -*-
"""
Created on Fri Jul  4 22:04:26 2014

@author: phan
"""
import sys, os
from os.path import join
import numpy as np
from PyQt4 import QtGui
import Common as cm
#Line 173: needed QString
from PyQt4.QtCore import (QByteArray, QDataStream, QFile, QFileInfo,
        QIODevice, QPoint, QPointF, QRectF, Qt, SIGNAL, QRect, QTimer)
from PyQt4.QtGui import (QApplication, QCursor, QFont, QFontComboBox, QCheckBox, QSlider,
        QFontMetrics, QGraphicsItem, QGraphicsPixmapItem, QMainWindow, QDockWidget, QWidget,
        QGraphicsScene, QGraphicsTextItem, QGraphicsView, QGridLayout, QTableWidget,
        QHBoxLayout, QLabel, QMatrix, QMenu, QMessageBox, QPainter, QTableWidgetItem,
        QPen, QPixmap, QPrintDialog, QPrinter, QPushButton, QSpinBox, QGraphicsGridLayout,
        QStyle, QTextEdit, QVBoxLayout, QListWidget,QBrush, QColor, QGraphicsWidget,
        QGraphicsPolygonItem, QPolygonF, QHBoxLayout)
 
from Geometry import to_polygons, smoothen, Angle
from Graph import scale
from Segmentation import segment_vector, segment_graph_old
import Segmentation
from shapely.geometry import *
from cgal.CGAL import extract_skeleton
import cgal.CGAL as cgal
import networkx as nx
import threading
import ParSegmentation as pseg
from Segmentation import segment_graph, segment_graph_auto
from SkeletonRetrieval import retrieve_skeleton, find_similar_fonts

class PolygonItem(QGraphicsPolygonItem):
    def __init__(self, polygon, parent):
        super(PolygonItem, self).__init__(polygon, parent)
        
    def paint(self, painter, option, widget):
        qp = painter
        qp.setPen(QPen())
        qp.setBrush(self.brush())
        #size = self.size
        ply = self.polygon()
        for p in ply:
            qp.drawEllipse(p, 5, 5)
            
        return super(PolygonItem, self).paint(painter, option, widget)
                   
class GraphItem(QGraphicsItem):
    '''
    work with networkx graph
    '''
    def __init__(self, graph, parent):
        super(GraphItem, self).__init__(parent)
        self.graph = graph
        self.handle_size = 8
        self.controls = {}
        self.selections = []
        self.setZValue(2)
        for node, data in graph.nodes_iter(data=True):
            control = NodeItem(QPointF(*list(data['pos'])), size=self.handle_size, idd=node, parent=self)
            self.controls[node] = control
        self.updateRect()
        self.node_visibile = True

        
    def paint(self, painter, option, widget):
        qp = painter
        #qp.setPen(QtGui.QColor(168, 34, 3))
        #qp.setPen(QPen(QBrush(self.color), 10))
        qp.setPen(QPen(QBrush(Qt.green)))
        if hasattr(self, 'controls'):
            for u, v in self.graph.edges_iter():
                p1 = self.graph.node[u]['pos']
                p2 = self.graph.node[v]['pos']
                painter.drawLine(p1[0], p1[1], p2[0], p2[1])
                
    def boundingRect(self):
        if hasattr(self, 'rect'):
            return self.rect
        else:
            return QRectF(self.x(), self.y(), 0.0, 0.0)
    
    def updateRect(self):
        pos = []
        for _, data in self.graph.nodes_iter(data=True):
            pos.append((data['pos'][0], data['pos'][1]))
            
        pos = np.array(pos)
        xmax,xmin,ymax,ymin = pos[:,0].max(), pos[:,0].min(), pos[:,1].max(), pos[:,1].min()
        self.prepareGeometryChange()
        self.rect = QRectF(xmin - 5, ymin - 5, xmax - xmin + 5, ymax - ymin + 5)
        #self.parentItem().update()
    
    def smoothen_path(self, path):
        _nm = lambda vec: np.array(-vec[1], vec[0])
        newpath = []
        for node in path:
            nbs = nx.neighbors(self.graph, node)
            if len(nbs) >= 2:
                vec = self.graph.node[nbs[1]]['pos'] - self.graph.node[nbs[0]]['pos']
                mid = vec / 2. + self.graph.node[nbs[0]]['pos']
                ratio = np.linalg.norm(mid - self.graph.node[node]['pos']) / np.linalg.norm(vec)
                if ratio > 0.5:
                    continue
            newpath.append(node)
            
        return newpath
                    
    def select(self, node, add=False):
        self.selections.append(node.idd)
        self.controls[node.idd].color = Qt.red
        if len(self.selections) > 1:
            '''
            if len(self.selections) == 2:
                path = nx.shortest_path(self.graph, self.selections[0], self.selections[1],'d')
                self.parentItem().replacePolyline([self.graph.node[p]['pos'] for p in path])
                #self.clear()
            else:
            '''
            
            path = [self.selections[0]]
            for i in range(len(self.selections) - 1):
                path = path[:-1] + nx.shortest_path(self.graph, self.selections[i], self.selections[i + 1],'d')
                
            #newpath = path #self.smoothen_path(path)
            #ply = self.parentItem().replacePath([self.graph.node[p]['pos'] for p in newpath])
            self.parentItem().replacePath(path)
            if not add:
                self.clear()
                            
        node.update()
    '''
    def mousePressEvent(self, e):
        if e.button() == 2:
            self.clear()
    '''                
    def clear(self):
        for nx, node in enumerate(self.selections):
            self.controls[node].color = Qt.green
        
        self.selections = []
        self.update()
        #node.update()
        
    def setNodeVisible(self, value, nodes=None):
        #print 'setting visibile'
        if nodes == None:
            self.node_visibile = value
            for node in self.graph.nodes_iter():
                self.controls[node].setVisible(value)
                
        
class PolylineItem(QGraphicsItem):
    def __init__(self, points, color, parent, idd):
        super(PolylineItem, self).__init__(parent)
        self.idd = idd
        self.controls = []
        self.color = color
        self.handle_size = 15
        self.setFlags(self.flags()                  |
                    QGraphicsItem.ItemIsSelectable  |
#                    QGraphicsItem.ItemIsMovable     |
                    QGraphicsItem.ItemSendsGeometryChanges)               
#                   QGraphicsItem.ItemIsFocusable   )   
        
        
        for px, p in enumerate(points):
            self.addHandle(p)
        
        if len(self.controls) > 0:
            titem = QGraphicsTextItem(str(self.idd),parent=self.controls[0])
            titem.setPos(QPointF(0,0))
            titem.setFont(QFont('',40))
            titem.scale(1,-1)
            
        self.updateRect()
        
    def paint(self, painter, option, widget):
        qp = painter
        #qp.setPen(QtGui.QColor(168, 34, 3))
        qp.setPen(QPen(QBrush(self.color), 25))
        if hasattr(self, 'controls'):
            if len(self.controls) > 0:
                qp.drawPolyline(*[ c.pos() for c in self.controls])
        
        if self.isSelected():
            qp.setPen(QPen(QBrush(self.color),  5, Qt.DotLine))
            qp.drawRect(self.boundingRect())

    def updateRect(self):
        pos = []
        for control in self.controls:
            pos.append((control.x(), control.y()))
        pos = np.array(pos)
        if len(pos) > 0:
            xmax,xmin,ymax,ymin = pos[:,0].max(), pos[:,0].min(), pos[:,1].max(), pos[:,1].min()
            #return self.mapToScene( QRectF(xmin - 5, ymin - 5, xmax - xmin + 5, ymax - ymin + 5) ).boundingRect()
            self.prepareGeometryChange()
            #self.rect = self.controls[0].boundingRect().adjusted(-20, -20, 20, 20)
            self.rect = QRectF(xmin - 5, ymin - 5, xmax - xmin + 5, ymax - ymin + 5)
            self.parentItem().update()
        
    def remove(self, control):
        control.setParentItem(None)
        if control.idd == self.controls[-1].idd:
            if len(self.controls) >= 2:
                self.controls[-2].setSize(self.handle_size)
                self.controls[-2].setColor(Qt.black)
                
        if control.idd == self.controls[0].idd:
            if len(self.controls) >= 2:
                self.controls[1].setSize(self.handle_size)
                self.controls[1].setColor(Qt.white)
                
        for cx, c in enumerate(self.controls):
            if c.idd == control.idd:
                del self.controls[cx]
                break
        
        self.update()
        
    def moveBy(self, dx, dy):
        for control in self.controls:
            control.setPos(control.x() + dx, control.y() + dy)
            
    def scaleBy(self, sx, sy):
        for control in self.controls:
            control.setPos(control.x() * sx, control.y() * sy)        
        
    def boundingRect(self):
        if hasattr(self, 'rect'):
            return self.rect
        else:
            return QRectF(self.x(), self.y(), 0.0, 0.0)
        
    def mousePressEvent(self, e):
        pass
        #if e.button() == 1:
        #    self.addHandle(e.pos())

    def addHandle(self, pos):
        if len(self.childItems()) == 0:
            idd = 0
        else:
            idd = self.childItems()[-1].idd + 1
            
        control = HandleItem(pos, size=self.handle_size, idd=idd, parent=self)
        
        self.childItems()[0].setColor(Qt.white)
        self.childItems()[-1].setColor(Qt.black)
        
        if len(self.childItems()) > 2:
            for i in self.childItems()[1:-1]:
                i.setSize(self.handle_size/1.5)
                i.setColor(self.color)
                i.setVisible(False)
                
        self.controls.append(control)
        self.updateRect()

    def itemChange(self, change, value):
        '''
        if change == QGraphicsItem.ItemSelectedHasChanged:
            #self.setZVal
            if hasattr(self.parentItem(), 'graph'):
                self.parentItem().graph.clear()
        '''
            
        return super(PolylineItem, self).itemChange(change, value)
        
    def points(self):
        return np.array([[c.x(), c.y()] for c in self.controls])
        
    def align(self, control_idd):
        control = self.controls
        '''
        if len(self.controls) == 2:
            midx = control[0].x() - control[1].y() 
            if abs( midx ) < 5:
                control[0].setX(control[0].x() - midx/2)
                control[1].setX(control[1].x() + midx/2)
                
            midy = control[0].y() - control[1].y() 
            if abs( midy ) < 5:
                control[0].setY(control[0].y() - midy/2)
                control[1].setY(control[1].y() + midy/2) 
        '''
        
class PathItem(PolylineItem):
    def __init__(self, path, graph, color, parent, idd):
        ply =  [graph.node[p]['pos'] for p in path if graph.has_node(p)]
        ann = [QPointF(p[0],p[1]) for p in ply]
        super(PathItem, self).__init__(ann, color, parent, idd)
        self.path = path
    
    def path(self):
        return self.path            
    #def remove(self, control):
    #    super(PathItem, self).remove(control)
        
        
class HandleItem(QGraphicsItem):
    def __init__(self, position, size, idd, parent, color=Qt.green):
        super(HandleItem, self).__init__(parent)
        self.setPos(position)
        self.size = size
        self.color = color
        self.idd = idd
        #self.setZValue(10)
        self.setFlags(self.flags()                  |
#                    QGraphicsItem.ItemIsSelectable  |
                    QGraphicsItem.ItemIsMovable     |
                    QGraphicsItem.ItemSendsGeometryChanges |
                    QGraphicsItem.ItemIsFocusable   )
        self.rect = QRectF(0,0,size,size)
                    
    def boundingRect(self):
        size = self.size
        return self.rect.adjusted(-size,-size,0,0)
        
    def paint(self, painter, option, widget):
        qp = painter
        qp.setPen(QtGui.QColor(168, 34, 3))
        qp.setBrush(QBrush(self.color, Qt.SolidPattern))
        size = self.size
        qp.drawEllipse(self.boundingRect())
        
    #def mouseReleaseEvent(self, e):
    #    if e.button() == 1:
    #        self.parentItem().align(self)

    def mousePressEvent(self, e):
        print 'clicked'
        if e.button() == 1:
            self.parentItem().parentItem().deselectOthers(None)
            self.parentItem().setSelected(True)
            self.parentItem().parentItem().bringToFront(self.parentItem().idd)
            #self.parentItem().update()
            
        elif e.button() == 2:
            self.parentItem().remove(self)
            
    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionChange:
            self.parentItem().updateRect()
        return super(HandleItem, self).itemChange(change, value)
        
    def setSize(self, size):
        self.prepareGeometryChange()
        self.size = size
        self.rect = QRectF(0,0,size,size)
        
    def setColor(self, color):
        self.color = color
        self.update()
    
class NodeItem(HandleItem):
    def __init__(self, position, size, idd, parent, color=Qt.green):
        super(NodeItem, self).__init__(position, size, idd, parent, color)
        
    def mousePressEvent(self, e):
        print 'clicked'
        if e.button() == 1:
            if e.modifiers() == Qt.ControlModifier:
                self.parentItem().select(self,add=True)
            else:
                self.parentItem().select(self)
            
        if e.button() == 2:
            self.parentItem().clear()
                                
class CharacterItem(QGraphicsItem):
    '''
    Class to render a single character.
    it has 3 layers: The outline of the character, the segments and the annotations
    '''
    def __init__(self, path, fontname, character, thread_lock=None, parent=None):
        super(CharacterItem, self).__init__(parent=None)
        filename = join(path, fontname + '.npz')
        self.character = character
        self.filename = filename
        self.fontname = fontname
        self.path = path
        self.bound = QRectF(0,0,100,100)
        self.thread_lock = thread_lock
        self.parent = parent
        #self.scene = scene
        self.setFlags(self.flags()                   |
        #            QGraphicsItem.ItemIsSelectable  |
        #            QGraphicsItem.ItemIsMovable     |
                     QGraphicsItem.ItemIsFocusable   )
        
        outline, segments, annotations, caps, graph = self.load(filename, character)
        
        self.outline = []
        if outline != None:
            self.outline_data_ = outline
            temp_  = np.vstack(outline)
            xmin, xmax = temp_[:,0].min(),  temp_[:,0].max()
            ymin, ymax = temp_[:,1].min(),  temp_[:,1].max()
            self.bound = QRectF(xmin, ymin, xmax - xmin, ymax - ymin)
            for contour in outline:
                ply = QPolygonF([QPointF(p[0],p[1]) for p in contour])
                self.outline.append(QGraphicsPolygonItem(ply,parent=self))
        
        self.segments = []
        self.caps = []
        self.setSegments(segments, caps)
        
        ''' Graph '''
        #if graph == None:        
        #    graph = extract_skeleton(outline)
        self.graph_data_ = graph
        if graph != None:
            nocontour = graph.copy()
            for u, v, data in nocontour.edges_iter(data=True):
                if data['mode'] == cgal.CONTOUR:
                    nocontour.remove_edge(u, v)
            self.graph = GraphItem(nocontour, parent=self)
        
            ''' Anns '''        
            self.anns = []
            if annotations is not None:
                for ann in annotations:

                    if isinstance(ann, np.ndarray):
                        if len(ann.shape) != 1:
                            self.addPolyline(ann)
                        else:
                            self.addPath(ann)
                    elif isinstance(ann, list):
                        self.addPath(ann)

        
    def bringToFront(self, polyline_idd):
        for ann in self.anns:
            if ann.idd == polyline_idd:
                ann.setZValue(1)
            else:
                ann.setZValue(0)
                
    def boundingRect(self):
        #if hasattr(self, 'bound'):
        return self.bound 
        #else:
        #    return QRectF(0,0,0,0)
        
    def paint(self, painter, option, widget):
        style=Qt.SolidLine
        pen = QPen(style)
        pen.setColor(Qt.white)
        pen.setWidth(0.5)
        painter.setPen(pen)
        painter.drawRect(self.bound)
        
        #for sgt in self.segments:
        #    sgt.update()
            
    def deselectOthers(self, item=None):
        if item is None:
            for c in self.childItems():
                c.setSelected(False)
        else:        
            for c in self.childItems():
                if c is not item:
                    c.setSelected(False)
                    
    def currentItem(self):
        for ply in self.childItems():
            if ply.isSelected():
                return ply
                
    def currentItemIndex(self):
        for ax, a in enumerate(self.childItems()):
            if a.isSelected():
                return ax
        
    def load(self, filename, character):
        cache = self.parent.fontcache
        if filename in cache:
            data = cache[filename]
        else:
            data = None
            if os.path.isfile(filename):
                if self.thread_lock != None:
                    self.thread_lock.acquire()
                    data = np.load(filename)
                    self.thread_lock.release()
                else:
                    data = np.load(filename)
            
            #preload all
            tmp = {}
            for f in data.files:
                tmp[f] = data[f]
            
            data = tmp
            if data is None: return None
            
            self.parent.fontcache[filename] = data
        
        outline, annotations, segments, caps, graph = None, None, None, None, None
        idx = 0
        if 'chars' in data:
            chars = data['chars']
            
            #chars = [ch for ch in str(chars)]
            idx  = list(chars).index(character)
        else:
            return None
            
        if 'outlines' in data:
            outlines = data['outlines']
            if len(outlines) == len(chars):
                outline = outlines[idx]
                
        if 'graphs' in data:
            graphs_ = data['graphs']
            if len(graphs_) == len(chars):
                #if graphs_[idx] != []:
                if isinstance(graphs_[idx], np.ndarray):
                    graph = graphs_[idx].tolist()
                else:
                    graph = graphs_[idx]
            
        if 'annotations' in data:
            annotations_ = data['annotations']
            #annotations = [[]] * len(chars)
            if len(annotations_) == len(chars):
                annotations = annotations_[idx]
                
        if 'segments' in data:
            segments_ = data['segments']
            if len(segments_) == len(chars):
                segments = segments_[idx]
                
        if 'caps' in data:
            caps_ = data['caps']
            if len(caps_) == len(chars):
                caps = caps_[idx]
        else:
            caps = []
                
        return (outline, segments, annotations, caps, graph)
        
    def removePolyline(self, ply):
        self.anns[-1].setParentItem(None)
        self.anns.pop()
        
    def addPolyline(self, ply):
        colorList = list(QColor.colorNames())
        color = QColor(colorList[len(self.anns) + 10])
        if ply is None:
            ann = [QPointF(200,200), QPointF(0,0)]
        else:
            ann = [QPointF(p[0],p[1]) for p in ply]
        if len(self.anns) == 0:
            idd = 0
        else:
            idd = self.anns[-1].idd + 1
        self.anns.append(PolylineItem(points=ann, color=color, parent=self, idd=idd))

        for a in self.childItems():
            a.setSelected(False)
        self.anns[-1].setSelected(True)
        return self.anns[-1]
    
    def addPath(self, path):
        colorList = list(QColor.colorNames())
        color = QColor(colorList[len(self.anns) + 10])
        if len(self.anns) == 0:
            idd = 0
        else:
            idd = self.anns[-1].idd + 1
                    
        self.anns.append(PathItem(path, self.graph_data_, color=color, parent=self, idd=idd))
        for a in self.childItems():
            a.setSelected(False)
        self.anns[-1].setSelected(True)
        return self.anns[-1]        
                
    def replacePath(self, path):
        #ply =  [self.graph_data_.node[p]['pos'] for p in path]
        for ax, a in enumerate(self.anns):
            if a.isSelected():
                idd = a.idd
                a.setParentItem(None)
                #ann = [QPointF(p[0],p[1]) for p in ply]
                colorList = list(QColor.colorNames())
                color = QColor(colorList[idd + 10])
                self.anns[ax] = PathItem(path, self.graph_data_, color=color, parent=self, idd=idd)#PolylineItem(points=ann, color=color, parent=self, idd=idd)
                self.anns[ax].setZValue(0)
                self.anns[ax].setSelected(True)
                return self.anns[ax]
            
        return self.addPath(path)
        
    def setPolylines(self, plies):
        for ann in self.anns:
            ann.setParentItem(None)
            del ann
            
        for ply in plies:
            self.addPolyline(ply)
        
    def setSegments(self, segments, caps):
        if segments != None:
            colorList = list(QColor.colorNames())
            for sgt in self.segments:
                sgt.setParentItem(None)
                
            for cap in self.caps:
                cap.setParentItem(None)
                #del sgt
            self.caps = []    
            self.segments = []
            for sgt in range(len(segments)):
                color = QColor(colorList[len(self.segments) + 10])
                ply = QPolygonF([QPointF(p[0],p[1]) for p in segments[sgt]])
                segment = PolygonItem(ply, parent=self)
                cl = QColor(color)
                cl.setAlpha(70)
                segment.setBrush(QBrush(cl,Qt.SolidPattern))
                self.segments.append(segment)
                '''
                if len(caps) > 0:
                    #print self.filename
                    p11, p12 = tuple(caps[sgt][0])
                    p21, p22 = tuple(caps[sgt][1])
#                    try:
                    cap1 = QPolygonF([QPointF(segments[sgt][p11][0], segments[sgt][p11][1]), \
                    QPointF(segments[sgt][p12][0], segments[sgt][p12][1])])
                    cap2 = QPolygonF([QPointF(segments[sgt][p21][0], segments[sgt][p21][1]), \
                    QPointF(segments[sgt][p22][0], segments[sgt][p22][1])])
#                    except IndexError:
#                        print ''
                    cap1 = QGraphicsPolygonItem(cap1, parent=self)
                    pen1 = QPen(QBrush(Qt.red), 10)
                    #print pen1.width()
                    cap1.setPen(pen1)
                    cap2 = QGraphicsPolygonItem(cap2, parent=self)
                    cap2.setPen(QPen(QBrush(Qt.red), 10))
                    
                    self.caps.append(cap1)
                    self.caps.append(cap2)
                '''
            #for cap in caps:
            self.caps_ = caps
    
    def scalePolyline(self, sx, sy):
        for ann in self.anns:
            ann.scaleBy(sx, sy)
            
    def movePolyline(self, dx, dy):
        for ann in self.anns:
            ann.moveBy(dx, dy)        
            
    def keyPressEvent(self, e):
        if e.text() == 'e':
            self.removePolyline(None)
        if e.text() == 'q':
            self.addPolyline(None)
        if e.text() == 's':
            self.snap()
        if e.key() == Qt.Key_H and e.modifiers() == Qt.ControlModifier:
            self.graph.setNodeVisible(not self.graph.node_visibile)
                        
        if e.key() >= Qt.Key_0 and e.key() <= Qt.Key_9:
            self.deselectOthers()
            if int(e.text()) < len(self.anns):
                self.anns[int(e.text())].setSelected(True) 
        
        if e.key() == Qt.Key_Up:
            self.movePolyline(0, 10)
            
        if e.key() == Qt.Key_Down:
            self.movePolyline(0, -10)

        if e.key() == Qt.Key_Left:
            self.movePolyline(-10, 0)

        if e.key() == Qt.Key_Right:
            self.movePolyline(10, 0)
                
        if e.key() == Qt.Key_Equal and e.modifiers() == Qt.ControlModifier:
            self.scalePolyline(1.1, 1.1)
                
        if e.key() == Qt.Key_Minus and e.modifiers() == Qt.ControlModifier:
            self.scalePolyline(0.9, 0.9)
            
        if e.text() == '*':
        
            segs = self.segmentsData()
            caps = self.capsData()
            
            newsegs = []
            newcaps = []
            for i in range(len(segs)):
                newsg, newcap = smoothen(segs[i], caps[i], tol = 5)
                #smoothen(self.outline_data_, segs[i], caps[i], tol = 5)
                newsegs.append(newsg)
                newcaps.append(newcap)
                print i, 'changes:', len(segs[i]), len(newsg)

            self.setSegments(newsegs, newcaps)
                
    def mousePressEvent(self, e):
        
        if e.button() == 1:
            pass
            '''
            for child in self.childItems():
                if child.isSelected():
                    child.addHandle(self.mapToItem(child, e.pos()))
            '''
        else:
            self.deselectOthers(None)
            
    def capsData(self):
        if hasattr(self, 'caps_'):
            return self.caps_
        else:
            return []
                
    def segmentsData(self):
        data = []
        for sgt in self.segments:
            data.append([np.array([p.x(), p.y()]) for p in sgt.polygon()])
        return np.array(data)
        
    def outlineData(self):
        return self.outline_data_
        
    def annsData(self):
        data = []
        for a in self.anns:
            if hasattr(a, 'path'):
                data.append(np.array(a.path))
            else:
                data.append(a.points())
        return np.array(data)
    
    def pathData(self):
        data = []
        for a in self.anns:
            data.append(a.path)
            
        return data
    
    def graphData(self):
        return self.graph_data_
        
    def snap(self):
        for child in self.childItems():
            if child.isSelected():
                #contours = self.outline_data_
                contours = asMultiLineString(self.outline_data_)
                for p in child.controls:
                    p_ = Point(p.x(), p.y())
                    if contours.distance(p_) < 50:
                        newp = contours.interpolate(contours.project(p_))
                        p.setPos(newp.x, newp.y)
                    
                    
class CharacterWidget(QGraphicsWidget):
    def __init__(self, path, fontname, character):
        super(CharacterWidget, self).__init__()
        self.item = CharacterItem(path, fontname, character, parent = self)
        self.scale(0.05,-0.05)
        
    def setPolylines(self, plies):
        for ply in plies:
            ply[:,0] *= 0.05
            ply[:,1] *= -0.05
        self.item.setPolylines(plies)

class MainGUI(QtGui.QMainWindow):
    def __init__(self, root):
        super(MainGUI, self).__init__()
        self.fontcache = {}
        self.root = root
        self.thread_lock = threading.Lock() 
        self.threads = []        
        self.initUI()
        self.mode = 'l'

    def initUI(self):
        #right panel for choosing character and fonts to synthesize
        rightLayout = QVBoxLayout()
        gridLayout = QGridLayout()
        
        rightPanel = QWidget()
        rightPanel.setLayout(rightLayout)
        
        self.chbxMirror = QCheckBox("upper")
        gridLayout.addWidget(self.chbxMirror, 0, 1)
        segmentButton = QPushButton("&Segment")
        self.connect(segmentButton, SIGNAL("clicked()"), self.segment)
        gridLayout.addWidget(segmentButton, 0, 0)
             
        saveButton = QPushButton('&Save')
        self.connect(saveButton, SIGNAL("clicked()"), self.save)
        gridLayout.addWidget(saveButton, 1, 0)
        
        chooseButton = QPushButton('&Retrieve Skel.')
        self.connect(chooseButton, SIGNAL("clicked()"), self.retrieveSkel)
        gridLayout.addWidget(chooseButton, 1, 1)
            
        #slider to change the scope of finding corresponding edges
        self.spnSearchRange = QSpinBox()
        self.spnSearchRange.setRange(0, 500)
        self.spnSearchRange.setPrefix('Search=')
        self.spnSearchRange.setValue(4)
        gridLayout.addWidget(self.spnSearchRange, 2, 0)
        
        #slider to change the smoothness of edges.
        self.spnAngleDiff = QSpinBox()
        self.spnAngleDiff.setRange(0, 180)
        self.spnAngleDiff.setPrefix('Angle=')
        self.spnAngleDiff.setValue(25)
        gridLayout.addWidget(self.spnAngleDiff, 2, 1)
        
        #shearing factor
        self.spnShear = QSpinBox()
        self.spnShear.setRange(-45, 45)
        self.spnShear.setPrefix('Shear=')
        gridLayout.addWidget(self.spnShear, 3, 0)
        
        #nbsize
        self.spnNbsize = QSpinBox()
        self.spnNbsize.setRange(0, 5)
        self.spnNbsize.setPrefix('Nbsize=')
        self.spnNbsize.setValue(1)
        
        
        gridLayout.addWidget(self.spnNbsize, 3,1)
        rightLayout.addLayout(gridLayout)
        
        #list of fonts
        self.classList = QListWidget()
        self.files = sorted([f[:-4] for f in os.listdir(self.root) \
        if os.path.isfile(join(self.root, f)) and f.endswith('.npz')])[:600]
        self.classList.addItems(self.files)
        self.classList.setCurrentRow(0)
        
        #self.specs = np.load(join(self.root[:-5], 'word_specs.npy'))[:52]
        tmpdata = np.load(join(self.root[:-5], 'word_specs.npz'))
        self.specs = (tmpdata['specs'][:52], tmpdata['mains'])
        
        rightLayout.addWidget(self.classList)
        self.connect(self.classList, SIGNAL("itemSelectionChanged()"), self.listChanged)
        
        #the list of characters
        ncols = 9
        nrows = int(len(cm.all_chars) / ncols + 1)
        charList = QTableWidget(nrows, ncols)
        charList.setCurrentCell(0, 0)
        self.charTable = charList
        for col in range(ncols):
            charList.setColumnWidth(col, 25)
            for row in range(nrows):
                if col * nrows + row >= len(cm.all_chars): break
                item = QTableWidgetItem(cm.all_chars[col * nrows + row])
                charList.setItem(row, col, item)
        self.connect(self.charTable, SIGNAL("itemSelectionChanged()"), self.itemSelectionChanged)
        rightLayout.addWidget(charList)
        
        #Add the panel to a dock for flexibility
        rightDock = QDockWidget("Fonts and Characters", self)
        rightDock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        rightDock.setWidget(rightPanel)
        self.addDockWidget(Qt.RightDockWidgetArea, rightDock)
        
        #The main widget which displays the character being edited
        self.view = QGraphicsView()
        self.view.scale(0.3,-0.3)
        self.scene = QGraphicsScene(self)
        self.scene.setBackgroundBrush(QBrush(Qt.white,Qt.SolidPattern))
        ch = CharacterItem(self.root, str(self.classList.currentItem().text()), 'q', self.thread_lock, parent=self)
        self.ch = ch
        self.scene.addItem(ch)
        self.view.setScene(self.scene)
        self.setCentralWidget(self.view)

        #Bottom dock shows all available charaters, rendered..
        self.view_1 = QGraphicsView()
        self.scene_1 = QGraphicsScene(self)
        self.view_1.setScene(self.scene_1)
        layout = QGraphicsGridLayout()
        self.previewLayout = layout
        layout.setSpacing(40)
        #add all characters' outline to view_1
        '''
        for i in [0,1,2]:
            for j in range(20):
                ch = CharacterWidget(self.root,
                                     str(self.classList.currentItem().text()),
                                     cm.all_chars[i * 20 + j])
                self.scene_1.addItem(ch)
                layout.addItem(ch, i, j)
        ''' 
        form = QGraphicsWidget()
        form.setLayout(layout)
        self.scene_1.addItem(form)

        bottomDock = QDockWidget("Glyph" , self)
        bottomDock.setAllowedAreas(Qt.BottomDockWidgetArea)
        bottomDock.setWidget(self.view_1)        
        self.addDockWidget(Qt.BottomDockWidgetArea, bottomDock)

        #Menus, status bar, tool bar and stuff
        exitAction = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        self.statusBar().showMessage('Welcome to SmartFont!')
        
        self.timer = QTimer(self)
        self.timer.start(1000)
        self.connect(self.timer, SIGNAL("timeout()"), self.update)
        #menubar = self.menuBar()
        #fileMenu = menubar.addMenu('&File')
        #fileMenu.addAction(exitAction)
        #toolbar = self.addToolBar('Exit')
        #toolbar.addAction(exitAction)
        
        self.setGeometry(0, 0, 1200, 800)
        self.setWindowTitle('SmartFont 1.0')    
        self.showMaximized()
    
    def refresh(self):
        self.fonts = []
        if self.chbxMirror.isChecked():
            self.mode = 'u'
        else:
            self.mode = 'l'
        #if os.path.isfile(join(self.root[:-5], mode + 'fonts.npy')):
        #    os.remove(join(self.root[:-5],mode + 'fonts.npy'))
        count = 0 
        for i in range(self.classList.count()):
            item = self.classList.item(i)
            #print str(item.text())
            data = np.load(join(self.root, str(item.text()) + '.npz'))
            if self.mode == 'u':
                chars = cm.capital_chars
            elif self.mode == 'l':
                chars = cm.lower_chars
                               
            if 'annotations' in data:
                anns = data['annotations']
                specs_ = np.array([len(a) for a in anns][:52])
                
                if not np.any(specs_ >= self.specs[0]):
                    item.setBackground(QBrush(Qt.white))
                elif not np.all(specs_ >= self.specs[0]) and np.any(specs_ >= self.specs[0]):
                    item.setBackground(QBrush(Qt.gray))
                elif np.all(specs_ >= self.specs[0]):
                    item.setBackground(QBrush(Qt.darkGray))
                    
                valid = True
                for dsx, ds in enumerate(anns):
                    if cm.all_chars[dsx] in chars:
                        if sum([len(d) > 0 for d in ds]) < self.specs[0][dsx]:
                            valid = False
                            #print cm.all_chars[dsx], 
                            
                if valid:
                    count += 1
                    item.setBackground(QBrush(Qt.green))
        print '(%d) valid fonts for %s' % (count, self.mode)        
            #if 'points' in data:
                #sgt = data['points']
                #if all([len(d) != 0 for d in data['points']][:52]):
                #    item.setBackground(QBrush(Qt.green))
                #else:
                #    item.setBackground(Qt.darkGreen)
        
        ncols = self.charTable.rowCount()
        nrows = self.charTable.columnCount()
        data = np.load(join(self.root, str(self.classList.currentItem().text()) + '.npz')) 
        #print 'loading ', str(self.classList.currentItem().text())
        if 'annotations' in data:       
            for row in range(nrows+1):
                for col in range(ncols+1):
                    item = self.charTable.item(row, col)
                    if item != None:
                        if len(data['annotations'][cm.all_chars.index(item.text())]) != 0:
                            item.setBackground(Qt.gray)
                        else:
                            item.setBackground(Qt.white)
                    
        else:
            for col in range(ncols+1):
                for row in range(nrows+1):
                    item = self.charTable.item(row, col)
                    if item != None:
                        item.setBackground(Qt.white)
                                    
    def update(self):
        self.statusBar().showMessage('Running Threads: ' + str(np.sum(np.array(self.threads) == 1)))
        if np.sum(np.array(self.threads) == 1) == 0:
            tmpfiles = os.listdir('.tmp')
            
            for f in tmpfiles:
                data = np.load(join('.tmp', f))
                original_file = str(data['filename'])
                orig_data = np.load(original_file)
                kwargs = {}
                for ff in orig_data:
                    kwargs[ff] = list(orig_data[ff])
                    
                for change in data['changes']:
                    if not change[1] in kwargs:
                        kwargs[change[1]] = [[]] * len(cm.all_chars)
                    kwargs[change[1]][change[0]] = change[2]
                    
                np.savez(original_file, **kwargs)
                os.remove(join('.tmp', f))
        
    def listChanged(self):
        self.saveToCache()
        self.scene.removeItem(self.ch)
        ch = CharacterItem(self.root, str(self.classList.currentItem().text()),
                           self.charTable.currentItem().text(), self.thread_lock, parent= self)
        self.scene.addItem(ch)
        self.ch = ch

        '''
        layout = QGraphicsGridLayout()
        layout.setSpacing(40)
        for item in self.scene_1.items():
            self.scene_1.removeItem(item)
        #add all characters' outline to view_1
        for i in [0,1,2]:
            for j in range(20):
                ch = CharacterWidget(self.root,
                                     str(self.classList.currentItem().text()),
                                     cm.all_chars[i * 20 + j])
                self.scene_1.addItem(ch)
                layout.addItem(ch, i, j)
        form = QGraphicsWidget()
        form.setLayout(layout)
        self.previewLayout = layout
        self.scene_1.addItem(form)
        '''
        #self.scene.update()
        
    def itemSelectionChanged(self):
        self.saveToCache()
        self.scene.removeItem(self.ch)
        ch = CharacterItem(self.root, str(self.classList.currentItem().text()),
                           self.charTable.currentItem().text(), self.thread_lock, parent=self)
        self.scene.addItem(ch)
        self.ch = ch
        #self.scene.update()
        
    def segment(self):
        self.save()
        if len(self.ch.anns) > 0:
            '''
            idd = len(self.threads)
            thread = pseg.SegmentationThread(self.ch, self, self.thread_lock,idd )
            status = 1
            self.threads.append(status)
            thread.start()
            '''
            modes = ['cap'] * len(self.ch.pathData())
            nmains = self.specs[1][ cm.all_chars.index( self.ch.character )]
            for i in range(nmains):
                modes[i] = 'stroke'
            
            print modes
            segments, caps, all_points = segment_graph_old(self.ch.graphData(), self.ch.outlineData(), self.ch.pathData(), modes)
            if all([len(sg) == 0 for sg in segments]):
                return
            
            self.saveToCache(caps=caps, segments=segments, all_points=all_points)
            #Segmentation.save(self.root, self.ch.filename, self.ch.character, segments, caps, all_points)
    
    def saveToCache(self, segments=None, caps=None, all_points=None):
        ch =  self.ch
        fname = ch.filename
        cache = self.fontcache[fname]
        chx = cm.all_chars.index(ch.character)
        cache['annotations'][chx]  = ch.annsData()
        #cache['graphs'][chx] = ch.graphData()
        if segments is None:
            cache['segments'][chx] = ch.segmentsData()
        else:
            cache['segments'][chx] = segments
            
        if caps is None:
            cache['caps'][chx] = ch.capsData()
        else:
            cache['caps'][chx] = caps
            
        if all_points is not None:
            cache['all_points'] = all_points
    
    def save(self):
        cache = self.fontcache[self.ch.filename]
        data = zip(cache.keys(), cache.values())
        cm.modify_npz(self.ch.filename, data)
        
    def save_(self):
        ch = self.ch
        #self.thread_lock.acquire()
        original = np.load(ch.filename)
        outlines, segments, annotations, graph = [], [], [], []
    
        if 'chars' in original.files:
            chars = list(original['chars'])
        else:
            chars = list(cm.all_chars)
            
        idx = chars.index(ch.character)
            
        if 'annotations' in original.files:
            annotations = list(original['annotations'])
            
        if len(ch.anns) > 0:
            annotations[idx] = ch.annsData()
            
        if 'graphs' in original.files:
            graphs = list(original['graphs'])
        else:
            graphs = [[]] * len(chars)
        #if len(ch.anns) > 0:
        
        #graphs[idx] = ch.graphData()
                            
        if 'segments' in original.files:
            segments_ = original['segments']
            if len(segments_) == len(chars):
                segments = list(segments_)
        else:
            segments = [[]] * len(chars)
            
        data = ch.segmentsData()
        if len(data) > 0:
            segments[idx] = data
            
        if 'caps' in original.files:
            caps_ = original['caps']
            if len(caps_) == len(chars):
                caps = list(caps_)
        else:
            caps = [[]] * len(chars)
            
        data = ch.capsData()
        if data is not None:
            if len(data) > 0:
                caps[idx] = data
        else:
            caps[idx] = []
                        
        if 'outlines' in original.files:
            outlines = original['outlines']
            
        if 'points' in original:
            points = original['points']
        else:
            points = [[]] * 62
            
        np.savez(ch.filename, outlines=outlines,
                 segments=segments, annotations=annotations, chars=chars, caps=caps, graphs=graphs, points = points)
                 
        #self.thread_lock.release()
        
        #if ch_ is None:       
        for row in range(self.previewLayout.rowCount()):
            for col in range(self.previewLayout.columnCount()):
                current_item = self.previewLayout.itemAt(row, col)
                if current_item.item.character == self.ch.character:
                    current_item.setParentItem(None)
                    self.scene_1.removeItem(current_item)
                    self.previewLayout.removeItem(current_item)
                    ch = CharacterWidget(self.root,
                                         str(self.classList.currentItem().text()),
                                         cm.all_chars[row * 20 + col])
                    self.scene_1.addItem(ch)                    
                    self.previewLayout.addItem(ch, row, col)
                    return            

    def retrieveSkel(self):
        if not hasattr(self, 'fonts'):
            self.fonts = []
        
        if self.chbxMirror.isChecked():
            if self.mode == 'l':
                self.font = []
                self.mode = 'u'
        else:
            if self.mode == 'u':
                self.fonts = []
                self.mode = 'l'
            
        testfont = join(self.root, str(self.classList.currentItem().text()) + '.npz')
        similar_font, bestscale, fonts = find_similar_fonts(self.root[:-5], testfont, self.fonts, mode=self.mode)
        if len(self.fonts) == 0:
            self.fonts = fonts
            
        print 'transferring: %s %s %f' % (testfont, similar_font, bestscale)
        data1 = np.load(testfont)
        data2 = np.load(join(self.root, similar_font))
        
        graphs = data1['graphs']
        anns = list(data1['annotations'])
        if len(anns) == 0:
            anns = [[] for _ in range(len(cm.all_chars))]
            
        ref_graphs = data2['graphs']
        ref_anns = data2['annotations']
        
        if self.chbxMirror.isChecked():
            chars = cm.capital_chars
        else:
            chars = cm.lower_chars
            
        #anns = [[] for _ in range(len(cm.all_chars))]
        for ch in chars:
            print ch,
            chx = cm.all_chars.index(ch, )
            graph = graphs[chx]
            ref_graph = scale(ref_graphs[chx], bestscale)
            ref_ann = ref_anns[chx]
            anns[chx] = None
            anns[chx] = np.array( segment_graph_auto(graph, ref_graph, ref_ann) )
        print

        cm.modify_npz(join(self.root , str(self.classList.currentItem().text()) + '.npz'), \
        [('annotations', anns)])
        
    def closeEvent(self, e):
        if not np.any(np.array(self.threads) == 1):
            print "All threads completed. Exit now!"
            e.accept()
        else:
            print "Threads are still running!"
            e.ignore()
            
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_S and e.modifiers() == Qt.ControlModifier:
            ''' Resegment entire font '''
            self.refresh()
            #filename = join(self.root, str(self.classList.currentItem().text()) + '.npz')
            #indices = [cm.all_chars.index(ch) for ch in cm.capital_chars]
            #for idx in indices:
            
        if e.key() == Qt.Key_Q and e.modifiers() == Qt.ControlModifier:
            self.retrieveSkel()
            
        if e.key() == Qt.Key_Plus:
            self.view.scale(1.1, 1.1)
            
        if e.key() == Qt.Key_Minus:
            self.view.scale(0.9, 0.9)
            
def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainGUI('/media/phan/BIGDATA/SMARTFONTS/data')
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()            
