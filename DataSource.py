# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 13 13:38:27 2014

@author: phan
"""
import os
import copy
import numpy as np
import Common as cm
import Feature as ft
import Segmentation as sg
#import scipy.spatial.distance as dist
from skimage.io import imread
from os.path import join
import math
#from emd import emd
from Common import neighbours
import multiprocessing
from numpy.testing import assert_almost_equal

class SimpleBOWCorpus():
    def __init__(self, fname):
        f = open(fname, 'r')
        self.n_docs = int(f.readline())
        self.n_words = int(f.readline())
        self.n_nonzeros = int(f.readline())
        self.docs = []
        
        doc_id, word_id, count = tuple([int(tok) for tok in f.readline().split(' ') if tok != ''])
        
        for i in range(self.n_docs):
            doc = []
            next_id = doc_id
            while next_id == doc_id:
                doc.append((word_id - 1, count))
                line = f.readline()
                if line == '': break
                doc_id, word_id, count = tuple([int(tok) for tok in line.split(' ')])

            self.docs.append(doc)
        f.close()

    def __getitem__(self,index):
        return self.docs[index]
        
def gen_vocabs(chrange, mode):
    mains = cm.mains
    specs = cm.specs
    W = [] #vocabulary
    parts = []
    for chx in chrange:
        if mode == 'outline':
            prange = range(0, mains[chx])
        elif mode == 'cap':
            prange = range(mains[chx], specs[chx])
        elif mode == 'skel':
            prange = range(0, mains[chx])
            
        for p in prange:
            parts.append(cm.all_chars[chx] + str(p).zfill(2))

    for p in parts:
        for q in parts:
            W.append(p + ':' + q)
            #W.append(p + '>' + q)
            #W.append(p + '^' + q)
    return parts, W
            
def get_values(word):
    '''
    Q01:N02
    '''
    return word[0], word[4], int(word[1:3]), int(word[5:7])   
     
def gen_virtual_corpus(path,fmt='uci', mode='outline', n_virtual_doc=1,n_virtual_word=100,vocab=None):
    ds = np.load(join(path, mode + '.pWs.npz'))
    parts = ds['parts']
    pWs = ds['pWs']
    #check validity of documents
    docs = []
    for d in range(len(pWs)):
        invalid = False
        for p in range(len(parts)):
            pW = pWs[d,p] / (sum(pWs[d,p]) + 1e-5)
            if math.isnan(sum(pW[:-1])):
                invalid = True
        if not invalid:
            docs.append(pWs[d])
    docs = np.asarray(docs)
    #fnames = ds['fnames']
    W = ds['W']
    '''
    vocab = open(join(path, 'vocab.fonts.txt'),'w')
    for w in W:
        vocab.write(w + '\n')
    vocab.close()
    '''
    
    W = dict(zip(W,range(len(W))))
    if vocab == None:
        vocab = []
    
    if fmt == 'uci':
        out = open(join(path, mode + '.docword.fonts.txt'),'w')
        out.write('%d\n' % (len(docs) * n_virtual_doc)) #Number of documents
        data = []
        
        #for every font
        for d in range(len(docs)):
            print 'doc ', d
            for r in range(n_virtual_doc):
                print '.', 
                for p in range(len(parts)):
                #assert(np.any(pWs[d,p]==np.nan))
                    vsample = np.zeros(len(parts))
                    pW = docs[d,p] / (sum(docs[d,p]) + 1e-6)
                    
                    #TODO: TAKE CARE OF MISSING CHARACTERS, NOT SURE WHY IT DOESN'T WORK WITH FULL NO. OF CHARACTERS.
                    if sum(pW) == 0:
                        vsample = np.zeros(len(pW))
                    #print pW
                    elif not math.isnan(sum(pW[:-1])) and sum(pW) - 1 < 0:
                        vsample = np.random.multinomial(n_virtual_word,pW)
                    else:
                        print '(not p!)' + str(pW.sum())
                    
                    for s in range(len(vsample)):
                        word = '%s:%s' % (parts[p], parts[s])
                        wordcount = vsample[s]
                        if wordcount > 0:
                            if word not in vocab:
                                vocab.append(word)
                                widx = len(vocab) - 1
                            else:
                                widx = vocab.index(word)
                            data.append('%d %d %d\n' % (d * n_virtual_doc + r + 1, widx + 1, wordcount))
        
        out.write('%d\n' % len(vocab)) #Vocabularies                    
        out.write('%d\n' % len(data))
        for line in data:
            out.write(line)
        out.close()
        
        fvocab = open(join(path, mode + '.vocab.fonts.txt'),'w')
        for w in vocab:
            fvocab.write(w + '\n')
        fvocab.close()

def prepare_dataset(path, mode='outline', dist='emd', chrange=np.arange(26, 52), files = [], \
prefix='', W=None, parts=None, fonts=[], normalized=False):
    '''
    dist = emd, hint
    mode = outline, skel or cap
    '''
    data_path = join(path, 'data')
    #fonts = []
    fontnames = []
    #os.listdir(path)
    if files == [] and fonts == []:
        files = sorted([f for f in os.listdir(data_path) \
        if os.path.isfile(join(data_path, f)) and f.endswith('.npz')])

    tmpdata = np.load(join(path, 'word_specs.npz'))
    specs = tmpdata['specs'] #all parts, including the caps
    #specs = tmpdata['mains'] #experimenting with main parts only
    mains = tmpdata['mains'] #only the main parts (strokes)
    word_specs = zip(specs, mains)
    
    if mode == 'outline':
        tmpdata = np.load(join(path, mode + '_dict.npz'))
        dictionary = tmpdata['clusters']
        dmat = tmpdata['mat']
        #this is for scaling features (thickness and curvatures...)
        norm_params = (tmpdata['mean'], tmpdata['minva'], tmpdata['thresh'])
    elif mode == 'skel':
        tmpdata = np.load(join(path, mode + '_dict.npz'))
        dictionary = tmpdata['dict']
        dmat = tmpdata['mat']
        norm_params = (tmpdata['mean'], tmpdata['minva'], tmpdata['maxva'])
    else:
        dictionary = None
        dmat = None
        norm_params = None
        
    if len(fonts) == 0:
        for f in files:
            data = np.load(join(data_path, f))
            if 'points' in data:
                #elif mode == 'outline':                
                points = data['points']
                specs_ = np.array([len(a) for a in points])
                if np.all(specs_[chrange] >= specs[chrange]):
                    #print f
                    if mode == 'skel':
                        anns = data['annotations']
                        graphs = data['graphs']
                        points = [[]] * len(cm.all_chars)
                        for ax, ann in enumerate(anns): #every character
                            graph = graphs[ax]
                            pss = []
                            for seg in ann: #every segment
                                try:
                                    ps = [graph.node[p]['pos'] for p in seg]
                                except:
                                    pass
                                pss.append(ps)
                            points[ax] = pss       
                                   
                    fonts.append((f, points))
                    fontnames.append(f)

    #fonts = fonts[-40:]            
    #print 'Folder contains %d valid fonts' % len(fonts)
    if len(fonts) == 0: 
        return

    if W == None or parts == None:
        W = [] #vocabulary
        parts = []
        for chx in chrange:
            if mode == 'outline':
                prange = range(0, mains[chx])
            elif mode == 'cap':
                prange = range(mains[chx], specs[chx])
            elif mode == 'skel':
                prange = range(0, mains[chx])
                
            for p in prange:
                parts.append(cm.all_chars[chx] + str(p).zfill(2))
    
        for p in parts:
            for q in parts:
                W.append(p + ':' + q)
                #W.append(p + '>' + q)
                #W.append(p + '^' + q)
    
    #print '%d words in total' % len(W)
    
    if len(fonts) > 500:
        chunksize = len(fonts) / 4
        procs = []
        for i in range(4):
            #par_pws(fonts[i * chunksize: (i+1) * chunksize], parts, chrange, word_specs, dictionary, dmat, W, mode, prefix, i, dist)
            frm = i * chunksize
            if i == 3:
                to = len(fonts)
            else:
                to = (i + 1) * chunksize
                
            p = multiprocessing.Process(target=par_pws, \
                                        args=(fonts[frm:to], parts, \
                                              chrange, word_specs, dictionary, dmat, norm_params, \
                                              W, mode, prefix, i, dist))
            procs.append(p)
            p.start()
        for p in procs:
            p.join()
        
        fnames = []
        pWs = []    
        for i in range(4):
            data = np.load(join(path, str(i) + '.' + prefix + mode + '.pWs.npz'))
            fnames.append(data['fnames'])
            pWs.append(data['pWs'])
            W = data['W']
            parts = data['parts']
            
        np.savez(join(path, prefix + mode + '.pWs.npz'), W=W, parts=parts, \
                 fnames=np.concatenate(fnames), pWs = np.concatenate(pWs, axis=0))
    else:
        return par_pws(fonts, parts, chrange, word_specs, dictionary, dmat, norm_params, W, mode, \
        prefix, -1, dist, normalized=normalized, path=path)
 
def par_pws(fonts, parts, chrange, word_specs, dictionary, dmat, norm_params, W, mode, prefix, \
idd, dist, normalized=False, path=None):
    #probability of seeing a word in a document
    pWs = []
    all_feats = []
    orig_pWs = []
    fnames = []
    for fname, points in fonts:
        #print 'font ' + fname
        pW = np.zeros((len(parts), len(parts)),dtype='float32')
        feats = [[] for _ in range(len(cm.all_chars))]
        for chx in range(len(cm.all_chars)):
            if mode == 'cap':
                feats[chx] = [[] for _ in range(word_specs[chx][0] - word_specs[chx][1])]
            elif mode == 'outline':
                feats[chx] = [[] for _ in range(word_specs[chx][1])]
            elif mode == 'skel':
                feats[chx] = [[] for _ in range(word_specs[chx][1])]
            
        for chx in chrange:
            #print cm.all_chars[chx]
            #feats[chx] = [[] for 0_ in range(word_specs[chx][1])]
            #modes = ['cap'] * len(points[chx])
            #for i in range(mains[chx]):
            #    modes[i] = 'stroke'
            
            if mode == "outline":
                modes = ['stroke'] * len(points[chx])
                feats[chx] = ft.calc_dhist_feature_graph(points[chx][:word_specs[chx][1]], dictionary, modes=modes,norm_params=norm_params)
            elif mode == 'cap':
                modes = ['cap'] * len(points[chx])
                feats[chx] = ft.calc_dhist_feature_graph(points[chx][word_specs[chx][1]:], None, modes=modes,norm_params=norm_params)
            elif mode == "skel":
                feats[chx] = ft.calc_dhist_feature_skel(points[chx][:word_specs[chx][1]], dictionary=dictionary, norm_params=norm_params)
        
        #TODO: only work with capital chars here....
        all_feats.append(feats)
        
        if chrange[0] >= 26:
            feats = feats[26:52]
        else:
            feats = feats[0:26]
            
        feats = reduce(lambda x,y: x + y, feats)
        if len(feats) != len(parts):
            print 'invalid font: ' + fname
            continue

        for row in range(len(parts)):
            #if len(feats[row]) != 0:
            #print '.',
            for col in range(row):
                if row == col or len(feats[row]) == 0 or len(feats[col]) == 0:
                    pW[row, col] = 0.
                else:
                    if dist == 'emd':
                        d = ft.earthmover(feats[row], feats[col], dmat)
                        
                    elif dist == 'hint':
                        d = ft.histogram_intersection(feats[row],feats[col])
                        
                    elif dist == 'overlap':
                        d = ft.overlap(feats[row], feats[col])
                        
                    elif dist == 'auto':
                        if feats[row].ndim == feats[col].ndim == 2:
                            d = ft.overlap(feats[row], feats[col])
                        elif feats[row].ndim != feats[col].ndim:
                            d = 1e-10 # reversed later
                        elif feats[row].ndim == feats[col].ndim == 1:
                            d = ft.earthmover(feats[row], feats[col], dmat)
                        
                    if d == 0: 
                        d = 1e-5
                    pW[row, col] = d
                    pW[col, row] = d
        
        orig_pWs.append(pW)
        #print 'normalizing...'
        if normalized:
            for row in range(len(parts)):
                
                if mode == 'cap':
                    if np.sum(pW[row]) == 0:
                        continue
                    
                    pW[row] /= np.sum(pW[row]) #normalize a row
                    #pass #nothing to do, just normalize the row is enough
                
                if mode == 'skel' or mode == 'outline':
                    mask = np.array([True] * len(pW[row]))
                    #Mask out zeros row..
                    mask = np.array([i != 0.0 for i in pW[row]])
                    if sum(mask) == 0:
                        continue
                    
                    mask[row] = False
                    stride = pW[row][mask]
                    
                    #maxval = max(stride) + min(stride)
                    #pW[row] = maxval - pW[row] #revert the probability due to the metric 
                    pW[row][mask] = 1. / stride
                    pW[row] /= np.sum(pW[row]) #normalize a row
                    pW[row, row] = 0
                    
                assert_almost_equal(pW[row].sum(), 1, decimal=5)
            
        pWs.append(pW)
        fnames.append(fname)
    
    if idd == -1:
        #np.savez(join(path,prefix + mode + '.pWs.npz'),pWs=pWs, parts=parts,fnames=fnames,W=W)
        np.savez(join(path, prefix + mode + '.pWs.npz'),pWs=pWs, \
        orig_pWs = orig_pWs, all_feats=all_feats, parts=parts,fnames=fnames,W=W)
        return pWs
    else:
        np.savez(join(path,str(idd) + '.' + prefix + mode + '.pWs.npz'),pWs=pWs,parts=parts,fnames=fnames,W=W)
        return pWs
           
if __name__ == '__main__':
    '''
    Check the path carefully before running anything !!!!
    '''
    
    path = '/media/phan/BIGDATA/SMARTFONTS'
    chrange = np.arange(26, 52) #np.array([cm.all_chars.index(ch) for ch in list('E')]) #np.array([cm.all_chars.index('A', )]) #np.arange(26, 52) #np.array([28, 40]) # #np.arange(26, 42)
    #files = ['AdventPro-ExtraLight.ttf.npz', 'AdventPro-Light.ttf.npz'] #['Adamina-Regular.ttf.npz','CherrySwash-Regular.ttf.npz','Alegreya-Italic.ttf.npz'] #['CherrySwash-Regular.ttf.npz'] #['Alegreya-Italic.ttf.npz', 'Adamina-Regular.ttf.npz', 'Amiri-Bold.ttf.npz', 'Amaranth-Regular.ttf.npz'] #['Amaranth-Regular.ttf.npz', 'Adamina-Regular.ttf.npz', 'Andada-Italic.ttf.npz', 'Asap-Regular.ttf.npz'] #['Adamina-Regular.ttf.npz', 'ABeeZee-Regular.ttf.npz','Alegreya-Black.ttf.npz']
    #files = ['ComingSoon.ttf.npz']
    files = []
    prefix = 'new_test.' #'test_EG.'
    dists = {'skel':'emd', 'outline' : 'emd', 'cap' : 'overlap'}
    for mode in ['outline', 'cap','skel']:
        print 'running mode ' + mode
        if prefix != '':
            tmpdata = np.load(join(path, mode + '.pWs.npz'))
            fvocab = open(join(path, mode + '.vocab.fonts.txt'),'r')
            vocab = [line[:-1] for line in fvocab.readlines()]
            fvocab.close()
            W = tmpdata['W']
            parts = tmpdata['parts']
        else:
            W = None
            parts = None
            vocab = None
        
        preprare_dataset(path, mode=mode, dist=dists[mode], chrange=chrange,files=files,prefix=prefix,W=W, parts=parts)
        #gen_virtual_corpus(path,fmt='uci', mode=prefix + mode, n_virtual_doc = 20,n_virtual_word = 100, vocab=vocab)
