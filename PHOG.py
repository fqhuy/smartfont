# -*- coding: utf-8 -*-
"""
Created on Mon Jul  7 22:38:41 2014

@author: phan
"""
import numpy as np
from os.path import join
import os
import Common as cm
from skimage.feature import hog as skhog
from skimage.io import imread
from skimage.transform import pyramid_reduce, pyramid_expand
from matplotlib import pyplot as plt

def build_indices(data_shape=None, interval=5, maxScale=25):
    interval = 5
    #image  = np.array(data_shape) #np.zeros(data_shape)
    #scaled = image.copy()
    scales = [0.0] * (maxScale + 1)
    iscales = [0.0] * (maxScale + 1)
    for i in range(interval):
        scale = pow(2.0, -i / float(interval))
        scale_ = pow(2.0, i / float(interval))
        #if scale < 1:
        #    scaled = np.round(image * scale)
            
        scales[i] = scale_
        iscales[i] = scale
        j = 1
        while i + j * interval <= maxScale:
            #scaled = np.round(scaled * 0.5)
            scales[i + interval * j] = scale_ * 2 * j
            iscales[i + interval * j] = scale * 1 / (2. * j)
            j = j + 1
            
    return scales, iscales
    
def phog1(iroot, oroot, filename):
    im = imread(join(iroot, filename) ,as_grey = True) / 255.
    im = pyramid_expand(im)
    alllevels = []
    scales, _ = build_indices(im.shape, maxScale=20)
    for level in range(1, 6):
        scaled = pyramid_reduce(im, scales[level])
        data = skhog(scaled, orientations = 8, pixels_per_cell=(8,8), \
        cells_per_block=(2,2), visualise=False, normalise = True)
        data = data.reshape((data.shape[0],data.shape[1],32))
        #sz = np.sqrt((data.shape[0] / 32))
        #data = data.reshape(sz, sz, 32)
        #data = np.pad(data, (pd, pd, (0,0)),'constant')
        alllevels.append(data)
    
    return alllevels
    #np.savez(join(oroot, filename + '.phog.npz'), phog=alllevels)
    
def hog(im):
    im = pyramid_expand(im)
    padx, pady = 0, 0
    pd = 64
    if im.shape[1] < pd:
        padx = int((pd - im.shape[1] + 0.5) / 2)
    if im.shape[0] < pd:
        pady = int((pd - im.shape[0] + 0.5) / 2)
        
    if padx != 0 or pady !=0:
        im = np.pad(im, (pady, padx), 'constant')  
        
    data = skhog(im, orientations = 8, pixels_per_cell=(8,8), \
    cells_per_block=(2,2), visualise=False, normalise = True)
    
    #plt.imshow(viz)
    #plt.show()
    return data.reshape((data.shape[0], data.shape[1], 32))
      
def phog(im):
    #im = imread(join(iroot, filename) ,as_grey = True) / 255.
    try:
        im = pyramid_expand(im)
    except ValueError:
        return None
        
    padx, pady = 0,0
    if im.shape[1] < 64:
        padx = int((64 - im.shape[1] + 0.5) / 2)
    if im.shape[0] < 64:
        pady = int((64 - im.shape[0] + 0.5) / 2)
        
    if padx != 0 or pady !=0:
        im = np.pad(im, (pady, padx), 'constant')    
        
    alllevels = []
    scales, _ = build_indices(im.shape, maxScale=10)
    for level in range(0, 11):
        if level > 0:
            scaled = pyramid_reduce(im, scales[level])
        else:
            scaled = im
            
        #try:
        data = skhog(scaled, orientations = 8, pixels_per_cell=(8,8), \
        cells_per_block=(2,2), visualise=False, normalise = True)
        #except ValueError:
        #    print 'error'
        data = data.reshape((data.shape[0],data.shape[1],32))
        alllevels.append(data)
    return alllevels
    
def batch_phog(dataroot, files):
    for f in files:
        print f
        if os.path.isfile(join(dataroot, f + '.phog.npy')):
            if np.load(join(dataroot, f + '.phog.npy')).shape == (62, 11):
                continue
        #    continue
        
        data = []
        valid = True
        bitmaps = np.load(join(imgroot, f + '.npz'))['bitmaps']
        for ch in range(len(cm.all_chars)):
            d = phog(bitmaps[ch])
            if d is None:
                print 'file %s, character %s is invalid' % (f, ch)
                valid = False
                break
            
            data.append(d)
            
        if valid:
            np.save(join(dataroot, f + '.phog.npy'), data)
            
if __name__=='__main__':
    import multiprocessing
    ttfroot = '/media/phan/BIGDATA/SMARTFONTS/ttf'
    imgroot = '/media/phan/BIGDATA/SMARTFONTS/img'
    dataroot = '/media/phan/BIGDATA/SMARTFONTS/data'
    allfiles = sorted([f for f in os.listdir(ttfroot) \
    if os.path.isfile(join(ttfroot, f)) and f.endswith('.ttf')])
    part = len(allfiles) / 4
    for i in range(4):
        if i < 3:
            files = allfiles[i * part : (i + 1) * part]
        else:
            files = allfiles[i * part : len(allfiles)]    
        p = multiprocessing.Process(target=batch_phog, args=(dataroot, files))
        #batch_phog(dataroot, files)
        p.start()
    