# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 16:32:25 2014

@author: phan
"""
import sys, math
from Feature import polyline_to_points
from skimage.io import imread
import numpy as np
import os.path
import gensim
import Common as cm
import LDA
from LDA import get_values
import Segmentation as sg
from DataSource import SimpleBOWCorpus
from scipy.spatial.distance import euclidean as euc
from scipy import interpolate
from scipy import signal

from Segmentation import neighbours
from Geometry import Angle as geoa
from Geometry import angle_between, sample_polyline
from shapely.geometry import *
from shapely.ops import cascaded_union
from shapely.affinity import translate, rotate
from sklearn.linear_model import Ridge, Lasso, RidgeCV, Lars
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from matplotlib import pyplot as plt
import Common as cm
#import ICP
from gicp import gicp
from fast.Distance import hausdorff
from sklearn.decomposition import PCA
import copy
from math import sin, cos
from os.path import join
import svgfig
from svg.path import parse_path
from ast import literal_eval as make_tuple

HEAD = 0
TAIL = 1
MID = 2

def prepare_skeletons(skeletons, smoothing=False, rearranging=False, anchor_thresh=400):
    '''
    prepare the skeletons before synthesization
    @skeletons multiple characters
    '''
    
    newskeletons = []
    allanchors = []
    #first simplify the skeletons
    for skeleton in skeletons:
        #for stx in range(len(skeleton)):
        #lines = [np.array( asLineString(part).simplify(0.2).coords ) for part in skeleton]
        #newskeletons.append(lines)
        newskeletons.append(skeleton)
    
    for skx, skeleton in enumerate(newskeletons):
        terms = []
        anchors = {}
        dss = []
        newskeleton = []
        for stx in range(len(skeleton)): #for each stroke
            ds = [0]
            anchors[stx] = []
            terms.append((skeleton[stx][0], skeleton[stx][-1]))
            if len(skeleton[stx]) > 1:
                d = 0
                for px in range(len(skeleton[stx])-1):
                    d += euc(skeleton[stx][px], skeleton[stx][px+1])
                    ds.append(d)
                ds = [d_ / ds[-1] for d_ in ds]
                
            dss.append(ds)
            
        #find all anchors
        #for each pair of terminals
        for tx, ts in enumerate(terms):
            #try to look for closest points in other strokes
            for i in [0, 1]:
                min_d = 1e10
                _stx, _px = 0, 0
                for stx in range(len(skeleton)):
                    if tx != stx:
                        for px in range(len(skeleton[stx])):
                            if euc(ts[i], skeleton[stx][px]) < min_d:
                                _stx = stx
                                _px = px
                                min_d = euc(ts[i], skeleton[stx][px])
                                
                        for j in [0, -1]:
                            if (len(skeleton[stx]) == 1) and euc(ts[i], skeleton[stx][j]) < anchor_thresh:
                                anchors[tx].append((i, stx, 0.5 ))
                                
                                break
                            elif euc(ts[i], skeleton[stx][j]) < anchor_thresh:
                                anchors[tx].append((i, stx, abs(j) ))
                                
                if min_d < anchor_thresh and _px != len(skeleton[_stx]) - 1 and _px != 0:
                    anchors[tx].append((i, _stx, dss[_stx][_px]))
                    anchors[_stx].append((dss[_stx][_px], tx, i))
        
        tcks = []
        if smoothing:
            for stx in range(len(skeleton)):
                us = list(np.arange(0, 1.01, 1. / 20))
                if len(skeleton[stx]) <= 2:
                    newskeleton.append(skeleton[stx])
                    tcks.append([])
                    continue
                
                newstroke, tck, _ = smooth_skeleton(skeleton[stx],get_norms=False, w=None,s=0.0,u=us) #w='gaussian',s=0,u=us)
                tcks.append(tck)
                newskeleton.append(newstroke)
        
        if rearranging:
            for stx in range(len(skeleton)):#np.arange(len(skeleton)-1, -1, -1):
                line2 = []
                line1 = []
                newstroke = newskeleton[stx]
                for anchor in anchors[stx]:
                    a,b,c = anchor
                    point1 = np.array(interpolate.splev([a], tcks[stx]))[:, 0]
                    line1.append(point1)
                    #plt.scatter([point1[0]], [point1[1]],s=150)
                    point2 = np.array(interpolate.splev([c], tcks[b]))[:, 0]
                    line2.append(point2)
                    #plt.scatter([point2[0]], [point2[1]],s=100,c='red')
                    
                if len(line1) == 1:
                    newskeleton[stx] += (line2[0] - line1[0])

                elif len(line1) > 1:
                    #ls1 = asLineString(line1)
                    #ls2 = asLineString(line2)
                    line1 = np.ascontiguousarray([np.array(asLineString(line1).interpolate(d, True)) for d in np.arange(0, 1.01, 0.1)])
                    line2 = np.ascontiguousarray([np.array(asLineString(line2).interpolate(d, True)) for d in np.arange(0, 1.01, 0.1)])
                    corres = np.zeros((len(line1), 3),dtype='double')
                    tran = gicp.icp(line1, line2, corres, 100.0, 0.0001)
                    newstroke = np.hstack((newstroke, np.tile([0,1], newstroke.shape[0]).reshape(newstroke.shape[0], 2)))
                    newstroke = tran.dot(newstroke.T).T
                    newskeleton[stx] = newstroke[:, 0:2]
                    
                    #t,c,k = tuple(tcks[stx])
                    c = np.array(tcks[stx][1]).T
                    c = tran.dot(np.hstack((c, np.tile([0,1], c.shape[0]).reshape(c.shape[0], 2))).T).T[:, 0:2]
                    tcks[stx] = (tcks[stx][0], c.T, tcks[stx][2])                
        
        #replace original skeletons with new ones.
        if smoothing or rearranging:
            newskeletons[skx] = np.array(newskeleton)
        
        anchors_ = [] 
        for stx in range(len(skeleton)):
            for anchor in anchors[stx]:
                a,b,c = anchor
                #point1 = np.array(interpolate.splev([a], tcks[stx]))[:, 0]
                #point2 = np.array(interpolate.splev([c], tcks[b]))[:, 0]  
                if a == 0:
                    jtype1 = HEAD
                elif a == 1:
                    jtype1 = TAIL
                else:
                    jtype1 = MID
                    
                if c == 0:
                    jtype2 = HEAD
                elif c == 1:
                    jtype2 = TAIL
                else:
                    jtype2 = MID       
                
                if jtype1 == MID:
                    if len(tcks) > 0:
                        point1 = np.array(interpolate.splev([a], tcks[stx]))[:, 0]
                    else:
                        point1 = skeleton[stx][-a]
                else:
                    point1 = skeleton[stx][-a]
                    
                if jtype2 == MID:
                    if len(tcks) > 0:
                        point2 = np.array(interpolate.splev([c], tcks[b]))[:, 0]
                    else:
                        point2 = skeleton[stx][-c]                 
                else:
                    point2 = skeleton[b][-c]
                                      
                anchors_.append((stx, point1, jtype1, b, point2, jtype2))
                
        allanchors.append(anchors_)
        
    return newskeletons, allanchors

def smooth_skeleton(stroke, k = 2, n = 50, get_norms=True, w=None,s=0.0,u=None,step=100):
    '''
    return a smoothened skeleton
    '''
    skeleton = np.asarray(stroke)
    if len(skeleton) == 2:
        skeleton = np.array([skeleton[0], skeleton.mean(axis=0), skeleton[1]])
        
    if n == 0:
        length = sum([euc(skeleton[p], skeleton[p+1]) for p in range(0, len(stroke) -1)])
        n = length / step
        
    if u == None: u = np.arange(0, 1.01, 1. / n)
    if s == None: s = len(u)
    if w == None:
        weights = None #signal.ricker(len(skeleton), 4)#
        
    elif w == 'gaussian':
        weights = signal.gaussian(len(skeleton), 6)
    elif w == 'igaussian':
        weights = (1 - signal.gaussian(len(skeleton), 6)) + 1e-10
    else:
        weights = w
    #weights -= weights.min()
    #weights += 1e-10
    try:
        tck, _ = interpolate.splprep([skeleton[:, 0], skeleton[:, 1]], w=weights, s=s, k=k)
    except Exception, e:
        print e
        return np.array([]), None, None, None
        
    points = np.array(interpolate.splev(u, tck, der=0)).T
    if get_norms:
        de1 = np.array( interpolate.splev(u, tck, der=1) ).T
        norms =  1 / np.sqrt((de1[:, 0]**2 + de1[:, 1]**2)) * np.vstack([-de1[:, 1], de1[:, 0]])
        return np.asarray(points), tck, u, norms.T
    else:
        return np.asarray(points), tck, u

def spline2svgdata(tcks):

    paths = []
    for tcks in lines:
        data = 'M %f,%f S ' % (tcks[1][0][0], tcks[1][1][1])
        for i in range(1, len(tcks[1][0])-1, 1):
            data += '%f,%f %f,%f ' % (tcks[1][0][i], tcks[1][1][i], tcks[1][0][i+1], tcks[1][1][i+1])
        
        #print data
        path = svgfig.Path(data)
        paths.append(path)
        
    return paths
    
def save_svg(lines, meta, mode='spline'):
    '''
    save polylines to svg
    @mode spline or polyline
    '''
    if mode == 'spline':
        paths = []
        for tcks in lines:
            data = 'M %f,%f S ' % (tcks[1][0][0], tcks[1][1][1])
            for i in range(1, len(tcks[1][0])-1, 1):
                data += '%f,%f %f,%f ' % (tcks[1][0][i], tcks[1][1][i], tcks[1][0][i+1], tcks[1][1][i+1])
            
            #print data
            path = svgfig.Path(data)
            paths.append(path)
        p = svgfig.SVG('g', paths[0].SVG(), paths[1].SVG(), id='paths')
        
    elif mode == 'polyline':
        data = 'M %f,%f ' % (lines[0][0], lines[0][1])
        for p in lines[1:]:
            data += 'L %f,%f ' % (p[0], p[1])
        data += ' z'
        #print data
        p = svgfig.SVG('g', svgfig.Path(data).SVG(), id='paths')
        
    p.save(join('.tmp', meta + '.brush.svg'))

def save_polygons_as_svg(polygons, filename):
    '''
    save polylines to svg
    @mode spline or polyline
    '''
    if hasattr(polygons, 'geom_type'):
        if polygons.geom_type == 'Polygon':
            polygons = [polygons]

    svg = svgfig.SVG('g', id='paths')
    for poly in polygons:
        lines = [np.array(poly.exterior) * np.array([0.05, -0.05])]
        for inter in poly.interiors:
            lines.append(np.array(inter) * np.array([0.05, -0.05]))
        
        data = ''    
        for line in lines:
            data += 'M %f,%f ' % (line[0][0], line[0][1])
            for p in line[1:]:
                data += 'L %f,%f ' % (p[0], p[1])
            data += ' z '
        
        svg.append(svgfig.Path(data).SVG())
    
    svg.save(filename)
    
def load_svg(meta, mode='spline', folder='.tmp'):
    '''
    load the stroke if available
    '''
    svg = svgfig.load(join(folder, meta + '.brush.svg'))
    tcks = [[], []]
    #select the first group as the brush
    for subx, sub in enumerate(svg.sub):
        if sub.t == 'g':
            idd = subx
            break

    move = np.array([0, 0])
    mat = np.array([[1,0,0],[0,1,0],[0,0,1]])
    if svg.sub[idd].attr.has_key('transform'):
        trans = svg.sub[idd].attr['transform']
        if 'translate' in trans:
            move[0], move[1] = make_tuple(trans[len('translate'):])
            mat = np.array([[1,0,move[0]],[0,1,move[1]],[0,0,1]])
            
        if 'matrix' in trans:
            a,b,c,d,e,f = make_tuple(trans[len('matrix'):])
            mat = np.array([[a,c,e],[b,d,f],[0,0,1]])
        
    if mode == 'spline':    
        for i in [0, 1]:
            path = parse_path(svg.sub[idd].sub[i]['d'])
            ps = []
            for j in np.arange(0,1.01, 0.1):
                ps.append(([path.point(j).real, path.point(j).imag]))
            ps = np.array( ps )
            ps += move
            tcks[i], _ = interpolate.splprep([ps[:,0], ps[:,1]])
        
        return tcks
    
    elif mode == 'polyline':
        pss = []
        #print path.d
        for px in svg.sub[idd].sub:
            if px.t == 'path':
                path = svgfig.pathtoPath(px)
                ps = []
                cupos = np.array([0, 0])
                for item in path.d:
                    if len(item) == 4:
                        pos = np.array(item[1:3])
                        if item[0] == 'm' or item[0] == 'l':
                            cupos += pos
                            pos = mat.dot( np.array(list(cupos) + [1]).T ).T[0:2] #cupos.copy() + move
                            
                        elif item[0] == 'M' or item[0] == 'L':
                            if item[3] == False:
                                pos = mat.dot( np.array(list(pos) + [1]).T ).T[0:2]
                            cupos = pos.copy()
                            
                        ps.append(pos.tolist())
                        
                ps = np.array(ps) #* np.array([scale, -scale])
                pss.append(ps)
        
        if len(pss) == 1:
            return pss[0]
        else:
            return pss
     
def load_svg1(meta, mode='spline'):
    '''
    load the stroke if available
    '''
    svg = svgfig.load(join('.tmp', meta + '.brush.svg'))
    tcks = [[], []]
    #select the first group as the brush
    for subx, sub in enumerate(svg.sub):
        if sub.t == 'g':
            idd = subx
            break

    move = np.array([0, 0])
    mat = np.array([[1,0,0],[0,1,0],[0,0,1]])
    if svg.sub[idd].attr.has_key('transform'):
        trans = svg.sub[idd].attr['transform']
        if 'translate' in trans:
            move[0], move[1] = make_tuple(trans[len('translate'):])
            mat = np.array([[1,0,move[0]],[0,1,move[1]],[0,0,1]])
            
        if 'matrix' in trans:
            a,b,c,d,e,f = make_tuple(trans[len('matrix'):])
            mat = np.array([[a,c,e],[b,d,f],[0,0,1]])
        
    if mode == 'spline':    
        for i in [0, 1]:
            path = parse_path(svg.sub[idd].sub[i]['d'])
            ps = []
            for j in np.arange(0,1.01, 0.1):
                ps.append(([path.point(j).real, path.point(j).imag]))
            ps = np.array( ps )
            ps += move
            tcks[i], _ = interpolate.splprep([ps[:,0], ps[:,1]])
        
        return tcks
    
    elif mode == 'polyline':
        pss = []
        #print path.d
        for px in svg.sub[idd].sub:
            if px.t == 'path':
                path = svgfig.pathtoPath(px)
                ps = []
                cupos = np.array([0, 0])
                for item in path.d:
                    if len(item) == 4:
                        pos = np.array(item[1:3])
                        if item[0] == 'm' or item[0] == 'l':
                            cupos += pos
                            pos = mat.dot( np.array(list(cupos) + [1]).T ).T[0:2] #cupos.copy() + move
                            
                        elif item[0] == 'M' or item[0] == 'L':
                            if item[3] == False:
                                pos = mat.dot( np.array(list(pos) + [1]).T ).T[0:2]
                            cupos = pos.copy()
                            
                        ps.append(pos.tolist())
                        
                ps = np.array(ps) #* np.array([scale, -scale])
                pss.append(ps)
        
        #if len(pss) == 1:
        #    return ps
        #else:
        return pss
        
def stroke_estimation(src_points, u, w=None, s=0.0, meta=None):
    '''
    estimate a stroke from the noisy data src_points
    '''
    points = [[],[]]
    tcks = []
    if meta != None:
        if os.path.isfile(join('.tmp', meta + '.brush.svg')):
            tcks = load_svg(meta)
        
    for i in [0, 1]:
        #Trick to avoid the terminals, which are jaggy
        outline = np.asarray(src_points[i]) #[2:-2]
        skel = np.asarray(src_points[i + 2]) #[2:-2]
        
        y = [euc(outline[px], skel[px]) for px in range(len(outline))]
        x = np.zeros(len(y),dtype='double')
        d = 0
        for px in range(len(skel)-1):
            x[px] = d
            d += euc(skel[px], skel[px+1]) 
        x[-1] = d
        
        if w == 'igaussian':
            w = (1 - signal.gaussian(len(x), 6)) + 1e-10
        
        if len(tcks) != 2:    
            tck, _ = interpolate.splprep([x, y], w=w, s=s, k = 2)
            tcks.append(tck)
        else:
            tck = tcks[i]
            
        de0 = np.array(interpolate.splev(u, tck, der=0)).T
        points[i] = de0[:, 1]
    
    if meta != None:
        if not os.path.isfile(join('.tmp', meta + '.brush.svg')):    
            save_svg(tcks, meta)
    
    return points, tcks

def extend_part(part, distance, direction=0):
    '''
    extend a part along the skeleton
    @direction HEAD or TAIL
    '''
    newpart = copy.deepcopy(part)
    if direction == 0:
        for i in [0, 1]:
            vec = np.array(part[i][0]) - np.array(part[i][1])
            vec /= np.linalg.norm(vec)
            newpart[i].insert(0, vec * distance + newpart[i][0])
    else:
        for i in [0, 1]:
            vec = np.array([0., 0.])
            for j in range(-4, -2):
                vec += -np.asfarray(part[i][j]) + np.asfarray(part[i][j + 1])
            vec /= 3
            #vec = part[i][-1] - part[i][-2] 
            vec /= np.linalg.norm(vec)
            newpart[i].insert(len(newpart[i]), vec * distance + newpart[i][-1])
    return newpart
       
def join_parts(parts, anchors, jtypes, mode='pointy', extension=200, smooth=True):
    '''
    create the joints between parts
    '''
    px, py = 0, 1
    ''' detect terminals '''
    newparts = [parts[px], parts[py]]
    if jtypes[px] != MID:
        if isinstance(parts[px],list):
            newparts[px] = None
            newparts[px] = extend_part(parts[px], extension, jtypes[px])
        
    if jtypes[py] != MID:
        if isinstance(parts[py], list):
            newparts[py] = None
            newparts[py] = extend_part(parts[py], extension, jtypes[py])
    
    polygons = []
    for part in parts:
        if isinstance(part, list) : 
            polygons.append(asPolygon(part[0] + list(reversed(part[1]))).buffer(0))
        else:
            ply = part #sample_polyline(part, 50)
            polygons.append(asPolygon(ply).buffer(0))
    
    '''        
    if (jtypes[px] == HEAD or jtypes[px] == TAIL) and \
    (jtypes[py] == HEAD or jtypes[py] == TAIL) and \
    isinstance(parts[px], list) and \
    isinstance(parts[py], list):
        #print '$'
        return cascaded_union(polygons)            
    #'''
                    
    '''
    if isinstance(parts[px], list) and isinstance(parts[py], list):
        if (euc(parts[px][0][-1], parts[py][0][-1]) < 200 and euc(parts[px][1][-1], parts[py][1][-1]) < 200) or \
        (euc(parts[px][0][0], parts[py][0][0]) < 200 and euc(parts[px][1][0], parts[py][1][0]) < 200):
            return cascaded_union(polygons)
    ''' 
    newpolygons = []
    for part in newparts:
        if isinstance(part, list):
            newpolygons.append(asPolygon(part[0] + list(reversed(part[1]))).buffer(0))
        else:
            newpolygons.append(asPolygon(part).buffer(0))
    #try:
    diff = newpolygons[py].symmetric_difference(newpolygons[px]) #.buffer(-1)
    if not hasattr(diff, '__len__'):
        diff = [diff]
    
    #remove artifacts (small remains after intersecting with original polygons)    
    plist = []
    for df in diff:
        OK = True
        for ply in polygons:
            tmp = df.intersection(ply)
            if (tmp.area > 500):
                OK = False
                break
        if OK:
            plist.append(df)
        #else:
        #    break
                
    #check how many "mask" we have. each mask will help remove redundants polygon from the outline        
    if len(plist) > 1:
        diff = MultiPolygon(plist)
    elif len(plist) == 1:
        diff = plist[0]
    else:
        #diff = Polygon()
        
        #print '#', 
        #if not (isinstance(parts[px], list) and isinstance(parts[py], list)):
        if isinstance(parts[px], list) and jtypes[px] != MID:
            p1_ = 0
            p2_ = 1
        elif isinstance(parts[py], list) and jtypes[py] != MID: 
            p1_ = 1
            p2_ = 0
        else:
            p1_ = 0
            p2_ = 1    
            
        t = -jtypes[p1_]    
        inter = newpolygons[p1_].intersection(polygons[p2_]).buffer(0)
        if not inter.geom_type == 'MultiPolygon':
            inter = inter.convex_hull
            
        if inter.geom_type == 'Polygon':
            newpolygons = polygons + [MultiPoint( np.vstack([parts[p1_][0][t], parts[p1_][1][t], \
            np.array(inter.exterior.coords)]) )] #.convex_hull]
        else:
            pass
            #print '!'
            #TODO: exper
            #newpolygons = polygons
            
        diff = Polygon()
        
        
    merged_outline =  cascaded_union(newpolygons).difference(diff)
    #merged_outline = cascaded_union(polygons + [merged_outline])
    
    #Find points near the merged region then smooth them.
    if smooth and merged_outline.geom_type == 'Polygon':
        for px, part in enumerate(newparts):
            if not isinstance(part, list):
                spart = sample_polyline(part, 25)
                polygons[px] = asPolygon(spart) #(asPolygon(part).buffer(0))
        
        interiors = merged_outline.interiors            
        merged_outline = np.array(merged_outline.exterior.coords)
        
        nbss = []
        nrps = []
        for px, point in enumerate(merged_outline):
            needrepair = True
            for poly in polygons:
                if not needrepair:
                    break
                #merging region detected.
                #if poly.distance(asPoint(point)) < 1e-5:
                for coord in poly.exterior.coords:
                    if euc(np.array(coord), point) < 5:
                        needrepair = False
                        break
            '''
            if needrepair:
                for nrp in nrps:
                    if euc(point, nrp) < 100:
                        needrepair = False
            '''    
            if needrepair:
                nbss.append(np.array(neighbours(merged_outline, px, 3, get_id=True)))
                nrps.append(point)
                    
        for nbs in nbss:
            points = merged_outline[nbs]
            #interpolate.splprep([points[:,0], points[:, 1]],w='gau')
            newpoints, _, _ = smooth_skeleton(points, get_norms=False, n=len(points), w='igaussian', s=len(points))
            merged_outline[nbs] = newpoints[:-1]
        
        #print 'Need to repair %d regions' % len(nbss)  
        merged_outline = asPolygon(merged_outline)
        for inte in interiors:
            merged_outline.difference(inte)
            
        return merged_outline
    
    return merged_outline

#merge parts using convex hull & local smoothing
def merge_parts1(parts, anchors):
    nbs = [[],[]]
    for anchor in anchors:
        for px, part in enumerate(parts):
            for point in part:
                if euc(point, anchor) < 200:
                    nbs[px].append(point)
            
    for px in range(len(parts)):
        tmp = np.array(nbs[px])
        #plt.scatter(tmp[:, 0], tmp[:, 1], c=cm.color_names[px])
    #polygons = [asPolygon(part) for part in parts]            
    #hull = MultiPoint(nbs).convex_hull
    #re = cascaded_union(polygons + [hull])
    return Polygon()
    
def merge_parts(parts, method='merge', skels=None, d_threshold=100):
    '''
    merging polygons
    method = join, merge, auto
    '''
    def _neighbours(plist, px, size):
        i = px
        nbs = [plist[px]]
        nbids = [px]
        while i < len(plist) and i - px < size:
            if euc(nbs[-1], plist[i]) < 50:
                nbs.append(plist[i])
                nbids.append(i)
            else:
                break
            i += 1
        i = px
        while i > 1 and px - i < size:
            i -= 1
            if euc(plist[i], nbs[0]) < 50:
                nbs = [plist[i]] + nbs
                nbids = [i] + nbids
            else:
                break
        return nbs, nbids
    
    px, py = 0, 1 
    
    if method == 'join':
        return join_parts(parts)
        
    polygons = [asPolygon(part[0] + list(reversed(part[1]))) for part in parts]
    #for px in range(len(polygons)):
    #    for py in range(px):
        #if not polygons[px].distance(polygons[py]) < 100:
        #   continue
        
    #remove all intersecting points
    
    #mparts = [part[0] + part[1] for part in parts]
    for i in [0, 1]:
        tabu = np.repeat(True, len(parts[py][i]))
        for pid, point in enumerate(parts[py][i]):
            if polygons[px].contains(asPoint(point)):
                tabu[pid] = False
                
        parts[py][i] = list(np.array(parts[py][i])[tabu])
        tabu = np.repeat(True, len(parts[px][i]))
        for pid, point in enumerate(parts[px][i]):
            if polygons[py].contains(asPoint(point)):
                tabu[pid] = False
                
        parts[px][i] = list(np.array(parts[px][i])[tabu])   
        polygons = [asPolygon(part[0] + list(reversed(part[1]))) for part in parts]
    
    mparts = [part[0] + part[1] for part in parts]

    #fitting splines on to neighbouring points
    curves = [None, None]
    for i in [0, 1]:
        min_dist = 1e10
        min_id = 0
        for pid, point in enumerate(parts[px][i]):
            d = polygons[py].distance(asPoint(point))
            if d < min_dist:
                min_id = pid
                min_dist = d
        
        min_dist = 1e10
        min_id1 = 0        
        nbs1, nbids1 = _neighbours(parts[px][i], min_id, 5)
        for pid, point in enumerate(mparts[py]):
            d = euc(parts[px][i][min_id], point)
            if d < min_dist:
                min_id1 = pid
                min_dist = d
        #remove points used for interpolation
        parts[px][i] = [parts[px][i][idd] for idd in range(len(parts[px][i])) if not idd in nbids1] 
                
        if min_id1 >= len(parts[py][0]):
            nbs2, nbids2 = _neighbours(parts[py][1], min_id1 - len(parts[py][0]), 5)
            min_id1 -= len(parts[py][0])
            #parts[py][1] = [parts[py][1][idd] for idd in range(len(parts[py][1])) if not idd in nbids2] 
        else:
            nbs2, nbids2 = _neighbours(parts[py][0], min_id1, 5)
            #parts[py][0] = [parts[py][0][idd] for idd in range(len(parts[py][0])) if not idd in nbids2] 
        
        #remove points used for interpolation
        if nbids1.index(min_id) < len(nbs1)/ 2:
            nbs1 = list(reversed(nbs1))
        
        if nbids2.index(min_id1) < len(nbs2)/ 2:
            nbs2 = list(reversed(nbs2))

        nbs = nbs1 + nbs2
        nbs = np.array([p.tolist() for p in nbs])
        mask = np.array([euc(nbs[nb], nbs[nb+1]) > 1  for nb in range(len(nbs)-1)])
        nbs = nbs[mask]
        curves[i], _, _ = smooth_skeleton(nbs, n=10, w='igaussian',get_norms=False)
        #curves[i], _ = smooth_skeleton(nbs, n=10, w=(1 - signal.gaussian(len(nbs), 4)) + 1e-10, get_norms=False)
        
        #tmp = np.array(parts[px][i])
        #plt.scatter(tmp[:,0], tmp[:, 1], c=cm.color_names[i])
        #tmp = np.array(parts[py][i])
        #plt.scatter(tmp[:,0], tmp[:, 1], c=cm.color_names[i])        
        
    #polygons = [asPolygon(part[0] + list(reversed(part[1]))) for part in parts]    
    cpoly = asPolygon(np.concatenate([curves[0], np.flipud(curves[1])]))
    #'''
    newpoly =  cascaded_union([polygons[px], cpoly, polygons[py]]) #polygons[px].union(cpoly).union(polygons[py])
    if newpoly.geom_type == 'MultiPolygon':
        for poly in newpoly:
            if poly.area > 10000:
                newpoly = poly
                break
    #'''        
    return newpoly #polygons[1]

def transfer_stroke_style(src_skel, dst_skel, src_outline=None, method='stretch', smooth=False, act=None):
    src_outline = copy.deepcopy(src_outline)
    src_skel = copy.deepcopy(src_skel)
    if method == 'stretch':
        src_points = src_outline + src_skel
        dst_points = [[], []]
        dst_skel, _, u, norms = smooth_skeleton(dst_skel, k=2, n=20, get_norms=True,w='gaussian',s=2.0)
        #dst_skel, _, u, norms = smooth_skeleton(dst_skel, k=2, n=40, get_norms=True,w=None,s=0.0)
        src_points, _ = stroke_estimation(src_points, u, w=None, meta=None, s=0.0)
        for i in [0, 1]:
            if act == 1:
                src_points[i] = src_points[i][::-1]
                
            for p in range(0, len(src_points[i])):
                width = src_points[i][p]
                if i == 0:
                    dst_points[i].append( norms[p] / np.linalg.norm(norms[p]) * width + dst_skel[p])
                else:
                    dst_points[i].append( -norms[p] / np.linalg.norm(norms[p]) * width + dst_skel[p])
        ppp = dst_points
        
    return ppp

    
def transfer_skeleton_style(src_skel, src_tpl_skel, dst_tpl_skel, transform=None, method = 'simple'):
    '''
    deform the src_tpl_skel to look like src_skel (user -provided)
    '''
    npoints = len(dst_tpl_skel)
    #dst_tpl_skel,_,_ = smooth_skeleton(dst_tpl_skel, k=2, step=50, s=npoints/2, get_norms=False)
    #npoints = len(dst_tpl_skel)
    if len(src_skel) != npoints or len(src_tpl_skel) != npoints:
        src_tpl_skel = sample_polyline(src_tpl_skel, n_samples = npoints)
        src_skel = sample_polyline(src_skel, n_samples = npoints)
    
    if method == 'simple':
        #try:
        deform =  src_skel - src_tpl_skel    
        #except:
        #    pass
        new_skel = dst_tpl_skel + deform
        new_skel,_,_ = smooth_skeleton(new_skel,  k = 2, n = 0, step=50, get_norms=False, w=None,s=0.0,u=None)
        
        
    elif method == 'projection':
        src_tpl_ply = asLineString(src_tpl_skel)    
        deform = []
        
        #calculate the "time" parameter for each point
        ds = np.zeros(len(src_tpl_skel), dtype=float)
        for px in range(1, len(ds)):
            ds[px] = ds[px - 1] + euc(src_tpl_skel[px], src_tpl_skel[px - 1])
        #ds /= ds[-1]
        
        for px, point in enumerate(src_skel):
            t = src_tpl_ply.project(Point(point))
            for dx, d in enumerate(ds):
                if t <= d:
                    idd = dx - 1
                    break
    
            if idd == len(ds) - 1:
                idd -= 1
                
            vec = src_tpl_skel[idd + 1] - src_tpl_skel[idd]
            nvec = np.array([-vec[1], vec[0]])
            s = 1
            if np.dot(point, nvec) - np.dot(nvec, src_tpl_skel[idd]) < 0:
                s = -1
                
            deform.append( s * euc(point , np.array(src_tpl_ply.interpolate(t))))
        
        deform = np.array(deform)
        
        #
        '''
        if transform == 1:
            #deform *= np.array([-1, 1])
            pass
        elif transform == 2:
            #deform *= np.array([1, -1])
            deform *= -1
        '''
        #new_skel = dst_tpl_skel + deform
        #new_skel,_,_ = smooth_skeleton(new_skel,  k = 2, n = 0, get_norms=False, w=None,s=2.5,u=None)
        new_skel = []    
        for px in range(len(dst_tpl_skel)-1):
            vec = dst_tpl_skel[px + 1] - dst_tpl_skel[px]
            nvec = np.array([-vec[1], vec[0]]) / euc(dst_tpl_skel[px + 1] , dst_tpl_skel[px])
            newpoint = dst_tpl_skel[px] + nvec * deform[px]
            new_skel.append(newpoint)
            if px == len(dst_tpl_skel) - 1:
                new_skel.append(dst_tpl_skel[-1] + nvec * deform[px])
        
    return np.array(new_skel)

def transfer_cap_style(src_cap, src_anchor, dst_anchor, affine_trans):
    pass

def transfer_style_graph(src_skel, dst_skel, src_outline=None, method='stretch', smooth=False, meta=None):
    '''
    transfer outline using either 'stretch' or 'ICP' or 'skel'
    '''
    #plt.scatter(dst_skel[:, 0], dst_skel[:, 1], c='r')
    ppp = []
    if method == 'stretch':
        src_points = src_outline + src_skel
        dst_points = [[], []] #, [dst_skel[0]], [dst_skel[0]]]  
        #plt.scatter(dst_skel[:, 0], dst_skel[:, 1], c='b')
        dst_skel, _, u, norms = smooth_skeleton(dst_skel, get_norms=True,w='gaussian',s=0.0)
        #plt.scatter(dst_skel[:, 0], dst_skel[:, 1], c='r')
        src_points, _ = stroke_estimation(src_points, u, w=None, meta=meta)
        for i in [0, 1]:
            for p in range(1, len(src_points[i])):
                width = src_points[i][p]
                if i == 0:
                    dst_points[i].append( norms[p] / np.linalg.norm(norms[p]) * width + dst_skel[p])
                else:
                    dst_points[i].append( -norms[p] / np.linalg.norm(norms[p]) * width + dst_skel[p])
        ppp = dst_points
            
    if method == 'ICP' or method == 'skel':
        if meta != None:
            if os.path.isfile(join('.tmp', meta + '.brush.svg')):
                #src_outline = None
                src_outline = load_svg(meta, mode='polyline')
                
            if not os.path.isfile(join('.tmp', meta + '.brush.svg')):    
                save_svg(src_outline, meta, mode='polyline')
                
        b_data = np.ascontiguousarray(dst_skel, dtype='double')
        
        #search over different flips
        min_dist = 1e10
        min_points = []
        cases = [np.array([1, 1]), np.array([1, -1]), np.array([-1, 1])]
        rots = []
        for ang in [0, np.pi/2, -np.pi/2]:
            rots.append( np.array([[cos(ang), -sin(ang)], [sin(ang), cos(ang)]]) )
        
        #t_a_data = np.array([[0,0],[1,1]])    
        for rot in rots:   
            for case in cases:
                a_data = np.ascontiguousarray(src_skel, dtype='double') * case
                corres = np.zeros((len(a_data), 3),dtype='double')
                a_data = rot.dot(a_data.T).T
                move = (-a_data.mean(axis=0) + b_data.mean(axis=0))
                a_data += move
                
                #prevent gicp from crashing
                if len(a_data) < 5 or len(b_data) < 5:
                    dist = hausdorff(b_data, a_data, mode=0)
                    if dist < min_dist:
                        min_dist = dist
                        min_points = np.ascontiguousarray(src_outline, dtype='double') * case #+ move
                    continue
                
                #try:
                t1 = gicp.icp(a_data, b_data, corres, 200.0, 0.01)
                #except:
                #    pass
                
                #dist = hausdorff(b_data, t1.dot( np.hstack((a_data, np.tile([0,1], \
                #    a_data.shape[0]).reshape(a_data.shape[0], 2))).T ).T[:, 0:2], mode=1)
                dist = np.sum(corres[:, 2])
                if dist < min_dist:
                    min_dist = dist
                    if method == 'ICP':
                        src_outline_ = src_outline * case
                        src_outline_ = rot.dot(src_outline_.T).T 
                        src_outline_ = np.ascontiguousarray(src_outline_, dtype='double') + move
                        
                        min_points = (t1.dot( np.hstack((src_outline_, np.tile([0,1], \
                            src_outline_.shape[0]).reshape(src_outline_.shape[0], 2))).T ).T[:, 0:2] )
                        
                        #t_a_data = (t1.dot( np.hstack((a_data, np.tile([0,1], \
                        #    a_data.shape[0]).reshape(a_data.shape[0], 2))).T ).T[:, 0:2] )
                        
                    elif method == 'skel':
                        min_points = (t1.dot( np.hstack((a_data, np.tile([0,1], a_data.shape[0]).reshape(a_data.shape[0], 2))).T ).T[:, 0:2] )
                                                
            #if method == 'ICP':
            #    plt.scatter(t_a_data[:, 0], t_a_data[:, 1],c='red')
            #    plt.scatter(b_data[:, 0], b_data[:, 1],c='green')
            
        ppp = min_points    
            
    return ppp
        
def transfer_style_old(src_bitmap, src_idd, src_skel, dst_idd, dst_skel):
    '''
    transfer a stroke style from src_bitmap to dst_skel
    total: a polygon that is ready to render
    '''
    image = (src_bitmap != 0)
    markers = np.zeros(image.shape)
    sg.mark(markers, src_skel)
    result = sg.segment_simple(image, markers)
    
    image = result.copy()
    image[(image != src_idd + 1)] = -1
    half1 = []
    half2 = []
    src_points = polyline_to_points(src_skel[src_idd],d=5)
    dst_points = polyline_to_points(dst_skel[dst_idd],d=5)
    ratio = len(dst_points) / float(len(src_points))
    
    for spx, sp in enumerate(src_points[1:-1]):
        first = src_points[spx - 1]
        second = src_points[spx + 1]
        #print second
        vec = np.asfarray(second - first)
        norm = np.linalg.norm(vec)
        vec = (vec / norm)
        
        nvec1 = np.array([vec[1],-vec[0]]) 
        nvec2 = np.array([-vec[1],vec[0]]) 
        #vec = vec * d

        #while np.linalg.norm(first - second) > d:
            #print np.linalg.norm(first - second)
        first = sp
        #search along nvec1
        curr1 = first.copy()
        curr2 = first.copy()
        while image[int(curr1[1]),int(curr1[0])] != -1:
            #image[curr1[1],curr1[0]] = 9
            curr1 += nvec1
            if curr1[1] <=0 or curr1[0] <= 0 or \
            curr1[1] >= image.shape[0] or curr1[0] >= image.shape[1]:
                break
        half1.append(curr1 - first + dst_points[int(spx * ratio)])
        #search along nvec2
        while image[int(curr2[1]),int(curr2[0])] != -1:
            #image[curr2[1],curr2[0]] = 9
            curr2 += nvec2
            if curr2[1] <= 0 or curr2[0] <= 0 or \
            curr2[1] >= image.shape[0] or curr2[0] >= image.shape[1]:
                break
        half2.append(curr2 - first + dst_points[int(spx * ratio)])
        
    half2.reverse()    
    total = np.array(half1 + half2) / 2.
    return total
