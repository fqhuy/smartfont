'''
Created on 1 Sep, 2014

@author: phan
'''
from matplotlib import pyplot as plt
import Common as cm
import numpy as np
from scipy import interpolate
from scipy import signal
from scipy.spatial.distance import euclidean as euc

def draw_graphs(graphs):
    fig = plt.figure(1)
    ax1 = fig.add_subplot(111)
    idd = 0
    for graph in graphs:
        #if idd == 3: continue
        #print cm.color_names[idd]
        points = np.array([graph.node[node]['pos'].tolist() for node in graph.nodes()])
        ax1.scatter(points[:, 0], points[:, 1],c=cm.color_names[idd],s=80)
        idd += 1
        
    ax1.autoscale()
    plt.show()
    
def draw_curvatures(points):
    fig = plt.figure(1)
    ax1 = fig.add_subplot(111)
    
    k = 2
    #u = np.arange(0, 1.01, 0.02)
    #ds = [euc(points[p],points[p+1]) for p in range(len(points)-1)]
    #total = sum(ds)
    
    tck, u = interpolate.splprep([points[:, 0], points[:, 1]], s=len(points), k=k)
    
    de0 = np.array( interpolate.splev(u, tck, der=0) )
    de1 = np.array( interpolate.splev(u, tck, der=1) ).T
    de2 = np.array( interpolate.splev(u, tck, der=2) ).T
    
    curvs = (de1[:, 0] * de2[:, 1] - de1[:, 1] * de2[:, 0]) / (de1[:, 0]**2 + de1[:, 1]**2)
    #curvs -= np.min(curvs)
    #curvs /= np.max(curvs)
    #peakind  =  signal.find_peaks_cwt(curvs,widths=np.arange(1, len(curvs)+1))#np.array([50] * len(curvs)))
    peakind = signal.argrelextrema(curvs,comparator= lambda x, y: x > y)
    
    ax1.scatter(de0[0], de0[1], c=curvs, s=100)
    ax1.scatter(de0[0][peakind], de0[1][peakind], s=400)
    
    ax1.autoscale()
    plt.show()    
    
