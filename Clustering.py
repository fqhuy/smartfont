# -*- coding: utf-8 -*-
"""
Created on Tue May 13 11:23:10 2014

@author: phan
"""
import numpy as np
from scipy.spatial.distance import euclidean as euc
from sklearn.cluster import KMeans, MeanShift
import Feature as ft

def cluster_fonts(path):
    '''
    '''
    
def compute_best_ids(queries, pWs, features, orig_pWs=None, dmat=None, parts=None, n_neighbors = 1):
    '''
    features n_samples x n_parts x feature_length
    dist_mats n_samples x n_parts x n_parts
    query n_parts * n_parts (missing element = NaN)
    '''
    
    n_parts = np.sqrt(queries[0].shape[0])
    n_tests = len(queries)
    pWs1 = orig_pWs.reshape(-1, n_parts, n_parts)
    #find neighbouring samples
    mask = np.bitwise_not(np.isnan(queries[0]))
    maskp = np.isnan(np.diag(queries[0].reshape((n_parts, n_parts))))
    
    clusters = []
    best_ids = np.zeros((n_tests, n_parts),dtype=int)
    print 'clustering'
    
    for sx ,sample in enumerate(pWs):
        print '.',
        #determine the local clusters in each feature vector
        ms = MeanShift()
        labels = ms.fit_predict(features[sx])
        clusters.append(labels)
        
    clusters = np.array(clusters)
    
    print 
    print 'processing ..'
    for qx, query in enumerate(queries):
        
        #best_ids.append([])
        ds = []
        for sx ,sample in enumerate(pWs):
            ds.append( euc( query[mask], sample[mask] ) )
        
        ds = np.array(ds)
        neighbors = np.argsort(ds)[:3]
        neighbor_weights = 1 / ds[neighbors] 
        print neighbors,
        missing_parts = np.where(maskp)[0]
        
        for mp in missing_parts:
            dists = np.zeros(n_parts , dtype=np.float32)
            dists[maskp] = np.inf
            #for each neighboring font
            for nx, n in enumerate(neighbors):
                given_parts = np.where(np.bitwise_not(maskp))[0]
                
                #for each sample part
                for px, p in enumerate(given_parts):
                    cluster_id = clusters[n][p]
                    cluster_members = np.where(clusters[n] == cluster_id)[0]
                    d = 0
                    
                    #d += euc(features[n, mp], features[n, p])
                    #d += pWs1[n, p, mp]
                    d += ft.earthmover(features[n, mp], features[n, p], dmat)
                    #for cm in cluster_members:
                    #    d += ft.earthmover(features[n, mp], features[n, cm], dmat)
                        #d += pWs1[n, mp, p]
                        #d += euc(features[n,cm], features[n,mp])
                        
                    #d /= len(cluster_members)
                    
                    dists[p] += d #* neighbor_weights[nx]
                    
            best_id = np.argmin(dists)
            best_ids[qx, mp] = best_id
            
        pass
    
    return best_ids