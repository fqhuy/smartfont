from distutils.core import setup, Extension
import numpy

# define the extension module
gicp = Extension('gicp', sources=['GICP.cpp'],
                          include_dirs=[numpy.get_include(), '/usr/local/include', 'ANN'],
                          libraries = ['gicp', 'ANN', 'gsl','gslcblas','stdc++','m'],
                          #extra_compile_args = ['-fPIC'],
                          library_dirs = ['/usr/local/lib'],
                          )

# run the setup
setup(ext_modules=[gicp])
