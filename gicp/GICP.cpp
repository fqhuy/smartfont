#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

// program options
//#include <boost/program_options.hpp>
//#include <boost/filesystem/path.hpp>
//#include <boost/filesystem/fstream.hpp>
//#include <boost/tokenizer.hpp> 

#include "gicp.h"
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <cstdio>
#include <vector>

using namespace std;
using namespace dgc::gicp;

int get_c_array(PyArrayObject** obj, double ***c_array)
{
    int n_vertices = PyArray_DIM((PyObject*)(*obj), 0);
    long int dims[] = {n_vertices, 2};
    PyArray_Descr *descr = PyArray_DescrFromType(NPY_DOUBLE);    
    PyArray_AsCArray((PyObject**)obj, (void*)c_array, dims, 2, descr);
    return n_vertices;
}

bool load_points(GICPPointSet *set, PyArrayObject *data) {
  bool error = false;

  GICPPoint pt;
  pt.range = -1;
  for(int k = 0; k < 3; k++) {
    for(int l = 0; l < 3; l++) {
      pt.C[k][l] = (k == l)?1:0;
    }
  }
  
  double **array;
  int n_a = get_c_array(&data, &array); 
  //printf("n_a=%d",n_a);
  for(int i = 0; i < n_a; ++i){
      pt.x = array[i][0];
      pt.y = array[i][1];
      pt.z = 0;
      set->AppendPoint(pt);
  }

  return false;  
};

PyObject* transform_2_ndarray(dgc_transform_t t)
{
  int r, c;
  long int dims[] = {4, 4};
  PyArrayObject *out = (PyArrayObject*) PyArray_ZEROS(2, dims, NPY_DOUBLE, 0);
    
  //fprintf(stderr, "%s:\n", str);
  for(r = 0; r < 4; r++) {
    for(c = 0; c < 4; c++){
        void *ptr = PyArray_GETPTR2(out, r, c);
        PyArray_SETITEM(out, ptr, PyFloat_FromDouble(t[r][c]));
    }
  }
  return (PyObject*) out;
}

static PyObject* icp(PyObject* self, PyObject* args){
    PyArrayObject *a_data;
    PyArrayObject *b_data; 
    PyArrayObject *corres;
    double d_max;
    double gicp_epsilon;
    
    if (!PyArg_ParseTuple(args, "O!O!O!dd", &PyArray_Type, &a_data,  &PyArray_Type, &b_data, &PyArray_Type, &corres, &d_max, &gicp_epsilon))
        return NULL;
        
    //printf("d_max %f", d_max);
    GICPPointSet p1, p2;
    dgc_transform_t t_base, t0, t1;    
    //printf("test: %d %d",n_a, n_b);
    
    dgc_transform_identity(t_base);
    dgc_transform_identity(t0);
    dgc_transform_identity(t1);    
    
    load_points(&p1, a_data);
    load_points(&p2, b_data);
    
    p1.SetGICPEpsilon(gicp_epsilon);
    p2.SetGICPEpsilon(gicp_epsilon);  
    p1.BuildKDTree();
    p1.ComputeMatrices();
    p2.BuildKDTree();
    p2.ComputeMatrices(); 
    
    // align the point clouds
    // cout << "Aligning point cloud..." << endl;
    dgc_transform_copy(t1, t0);
    //p2.SetDebug(debug);
    p2.SetMaxIterationInner(8);
    p2.SetMaxIteration(100);
    
    double **c_corres;
    get_c_array(&corres, &c_corres);
    int iterations = p2.AlignScan(&p1, c_corres, t_base, t1, d_max);

    // print the result
    // cout << "Converged: " << endl;
    PyObject* re = transform_2_ndarray(t1);
    //cout << "Converged: " << endl;
    //dgc_transform_print(t_base, "t_base");
    //dgc_transform_print(t0, "t0");  
    //dgc_transform_print(t1, "t1");
     
    Py_INCREF(re);
    return re;
}

/*  define functions in module */
static PyMethodDef Methods[] =
{
     {"icp", icp, METH_VARARGS,
         "compute the transformation matrix"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC
initgicp(void)
{
     (void) Py_InitModule("gicp", Methods);
     /* IMPORTANT: this must be called */
     import_array();
}
