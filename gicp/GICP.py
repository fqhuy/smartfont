import numpy as np
import gicp
from os.path import join
from cgal.CGAL import SKELETON
from matplotlib import pyplot as plt
import Common as cm
import matplotlib.patches as patches
from matplotlib.path import Path
if __name__ == '__main__':
    path = '/media/phan/BIGDATA/SMARTFONTS/data'
    data = np.load(join(path, 'Adamina-Regular.ttf.npz'))
    ch = cm.all_chars.index('X')
    graph1 = data['graphs'][ch]
    data = np.load(join(path, 'AbrilFatface-Regular.ttf.npz'))
    graph2 = data['graphs'][ch]
    
    a_data = np.ascontiguousarray([graph1.node[node]['pos'] for node in graph1.nodes_iter() if graph1.node[node]['mode'] == SKELETON]) #- 500.
    b_data = np.ascontiguousarray([graph2.node[node]['pos'] for node in graph2.nodes_iter() if graph2.node[node]['mode'] == SKELETON]) #/ 2000.
    
    corres = np.zeros((len(a_data), 3),dtype='double')
    re = gicp.icp(a_data, b_data, corres, 500.0, 1e-2)
    a_data = np.hstack((a_data, np.tile([0,1], a_data.shape[0]).reshape(a_data.shape[0], 2)))
    c_data = re.dot(a_data.T).T
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.scatter(a_data[:, 0], a_data[:, 1],c='r')
    ax.scatter(b_data[:, 0], b_data[:, 1],c='g')
    ax.scatter(c_data[:, 0], c_data[:, 1],c='b')
    
    verts = []
    codes = []
    for px, p in enumerate(a_data):
        verts.append(tuple(p[0:2]))
        codes.append(Path.MOVETO)
        
        verts.append(tuple(b_data[ int(corres[px][1]) ]))
        codes.append(Path.LINETO)
        
    path = Path(verts, codes)
    patch = patches.PathPatch(path, facecolor='orange', lw=2)
    ax.add_patch(patch)
    plt.show()
    print re
