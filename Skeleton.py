# -*- coding: utf-8 -*-
"""
Created on Tue Apr 22 14:04:30 2014

@author: phan
"""

'''
Glyph bitmap monochrome rendring
'''
from freetype import *
import os
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from skimage import morphology as morph
from skimage.io import imsave
from skimage.transform import rescale
from Common import all_chars, capital_chars

def bits(x):
    data = []
    for i in range(8):
        data.insert(0, int((x & 1) == 1))
        x = x >> 1
    return data

def to_numpy_array(filename, char):
    face = Face(filename)
    face.set_char_size( 128 * 64 )
    face.load_char(char, FT_LOAD_RENDER |
                        FT_LOAD_TARGET_MONO )

    bitmap = face.glyph.bitmap
    width  = face.glyph.bitmap.width
    rows   = face.glyph.bitmap.rows
    pitch  = face.glyph.bitmap.pitch

    data = []
    for i in range(bitmap.rows):
        row = []
        for j in range(bitmap.pitch):
            row.extend(bits(bitmap.buffer[i*bitmap.pitch+j]))
        data.extend(row[:bitmap.width])
    Z = np.array(data).reshape(bitmap.rows, bitmap.width)
    return Z
    
def skeletonize(filename, path, chars=all_chars):
    face = Face(join(path,'ttf', filename))
    allchars = []
    for c in chars:
        ic = to_numpy_array(join(path,'ttf', filename),c)
        #skc = np.asarray(morph.skeletonize(ic), dtype='int32') * 255
        #skc = rescale(ic, 2)
        ic = np.asarray(ic, dtype='uint8')
        #skc = rescale(skc, 4)
        #imsave(join(path, 'img', filename + '.' + c + '.pgm'), ic)
        allchars.append(ic)
    
    np.savez(join(path, 'img', filename + '.npz'), bitmaps=allchars, chars=all_chars)

def batch_skeletonize(files, path, chars=all_chars):
    for f in files:
        skeletonize(f, path, all_chars)
        
if __name__ == '__main__':
    import multiprocessing
    path = '/media/phan/BIGDATA/SMARTFONTS'
    data_path = '/media/phan/BIGDATA/SMARTFONTS/ttf'
    #output_path = '/media/phan/BIGDATA/SMARTFONTS/img'
    allfiles = sorted([f for f in os.listdir(data_path) \
    if os.path.isfile(join(data_path, f)) and f.endswith('.ttf')])
    part = len(allfiles) / 4
    for i in range(4):
        if i < 3:
            files = allfiles[i * part : (i + 1) * part]
        else:
            files = allfiles[i * part : len(allfiles)]
        #for f in files:
        p = multiprocessing.Process(target=batch_skeletonize,args=(files, path, all_chars))
        p.start()
        #skeletonize(f, path, all_chars)
                                