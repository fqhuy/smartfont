import numpy as np
cimport numpy as np
from cython.operator cimport dereference as deref, preincrement as inc
import sys
from libc.math cimport abs,pow,sqrt

DTYPE = np.double
ctypedef np.double_t DTYPE_t
    
cpdef double hausdorff(np.ndarray[DTYPE_t,ndim=2] A, np.ndarray[DTYPE_t,ndim=2] B, int mode=0):
    # mode = 0 - use forward only
    # mode = 1 - use forward and backward
    # Get the sizes of the input arrays
    
    cdef int Arows, Acols, Brows, Bcols
    cdef double mindist, tempdist
    Arows = A.shape[0]
    Acols = A.shape[1]
    Brows = B.shape[0]
    Bcols = B.shape[1]
    
    # Check for same dimensions
    
    if Acols != Bcols:
        print "The dimensions of the points in the two feature vectors are different"
    
    # Calculate the forward HD
    
    cdef double fhd = 0
    
    for i in range(Arows):
        mindist = 1e10
        for j in range(Brows):
            #tempdist = sqrt(((A[i, 0]-B[j, 0])*(A[i, 0]-B[j, 0])) + ((A[i, 1]-B[j, 1])*(A[i, 1]-B[j, 1])))
            tempdist = ((A[i, 0]-B[j, 0])*(A[i, 0]-B[j, 0])) + ((A[i, 1]-B[j, 1])*(A[i, 1]-B[j, 1]))
            if tempdist < mindist:
                mindist = tempdist
                
        fhd = fhd + mindist

    fhd = fhd / Arows
    
    # Calculate the reverse HD
    
    cdef double rhd = 0
    if mode == 1:
        for i in range(Brows):
            mindist = 1e10;
            for j in range(Arows):
                #tempdist = sqrt(((A[j, 0]-B[i, 0])*(A[j, 0]-B[i, 0])) + ((A[j, 1] - B[i, 1])*(A[j, 1] - B[i, 1])))
                tempdist = ((A[j, 0]-B[i, 0])*(A[j, 0]-B[i, 0])) + ((A[j, 1] - B[i, 1])*(A[j, 1] - B[i, 1]))
                if tempdist < mindist:
                    mindist = tempdist
            
            rhd = rhd + mindist
        
        rhd = rhd / Brows
    
    return max(fhd,rhd)