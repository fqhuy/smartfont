# -*- coding: utf-8 -*-
"""
Created on Tue Feb 10 13:17:48 2015

@author: phan
"""
 
import sys, os
import numpy as np
import Common as cm
from Geometry import to_paths, to_polygons, load_svg_font
from os.path import join 

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import (QByteArray, QDataStream, QFile, QFileInfo,
        QIODevice, QPoint, QPointF, QRectF, Qt, SIGNAL, QRect, QTimer)
from PyQt4.QtGui import (QPainterPath, QGraphicsPathItem, QApplication, QCursor, QFont, QFontComboBox, QCheckBox, QSlider,
        QFontMetrics, QGraphicsItem, QGraphicsPixmapItem, QGraphicsItemGroup, QMainWindow, QDockWidget, QWidget,
        QGraphicsScene, QGraphicsTextItem, QGraphicsView, QGridLayout, QTableWidget,
        QHBoxLayout, QLabel, QMatrix, QMenu, QMessageBox, QPainter, QTableWidgetItem,
        QPen, QPixmap, QPrintDialog, QPrinter, QPushButton, QSpinBox, QGraphicsGridLayout,
        QStyle, QTextEdit, QVBoxLayout, QListWidget,QBrush, QColor, QGraphicsWidget,
        QGraphicsPolygonItem, QPolygonF, QHBoxLayout)
        
class BezierItem(QGraphicsItem):
    def __init__(self, contour, color, parent, idd):
        super(BezierItem, self).__init__(parent)
        self.rect = QRectF(0,0,0,0)
        self.idd = idd
        #self.handleGroup = QGraphicsItemGroup(parent=self)
        self.color = color
        self.handle_size = 15
        self.setFlags(self.flags()                  |
                    QGraphicsItem.ItemIsSelectable  |
#                    QGraphicsItem.ItemIsMovable     |
                    QGraphicsItem.ItemSendsGeometryChanges)         
#                    QGraphicsItem.ItemIsFocusable   )   
        
        #self.handleGroup.setFiltersChildEvents(False)
        #self.handleGroup.setHandlesChildEvents(False)
        
        self.controls = []
        for px in range(contour.elementCount()):
            elem = contour.elementAt(px)
            
            control = HandleItem(QPointF(elem), size=self.handle_size, idd=px, parent=self, elem_type=elem.type)
            self.controls.append(control)
            #self.handleGroup.addToGroup(control)
        
        
        self.bezier = QGraphicsPathItem(contour, self)
        self.updateRect()
        #self.handleGroup.setVisible(False)
        
    def updateRect(self):
        pos = []
        for control in self.controls:
            pos.append((control.x(), control.y()))
        pos = np.array(pos)
        if len(pos) > 0:
            xmax,xmin,ymax,ymin = pos[:,0].max(), pos[:,0].min(), pos[:,1].max(), pos[:,1].min()
            #return self.mapToScene( QRectF(xmin - 5, ymin - 5, xmax - xmin + 5, ymax - ymin + 5) ).boundingRect()
            self.prepareGeometryChange()
            #self.rect = self.controls[0].boundingRect().adjusted(-20, -20, 20, 20)
            self.rect = QRectF(xmin - 5, ymin - 5, xmax - xmin + 5, ymax - ymin + 5)
            self.parentItem().update()
            
    def paint(self, painter, option, widget):
        #return
        qp = painter
        qp.setPen(QPen(QBrush(self.color), 25))
        if hasattr(self, 'handleGroup'):
            if len(self.handleGroup.childItems()) > 0:
                qp.drawPolyline(*[ c.pos() for c in self.handleGroup.childItems()])
            
    def boundingRect(self):
        if hasattr(self, 'rect'):
            return self.rect
        else:
            return QRectF(self.x(), self.y(), 0.0, 0.0)

        
class MutableBezierItem(BezierItem):
    def __init__(self, points, color, parent, idd):
        super(MutableBezierItem, self).__init__(points, color, parent, idd)
        #self.handleGroup.setVisible(True)
        '''
        if len(self.handleGroup.childItems()) > 0:
            titem = QGraphicsTextItem(str(self.idd),parent=self.controls[0])
            titem.setPos(QPointF(0,0))
            titem.setFont(QFont('',40))
            titem.scale(1,-1)
        '''
      
        
class HandleItem(QGraphicsItem):
    def __init__(self, position, size, idd, parent, color=Qt.green, elem_type=None):
        super(HandleItem, self).__init__(parent)
        self.setPos(position)
        self.size = size
        self.color = color
        self.idd = idd
        self.type = elem_type
        #self.setZValue(10)
        self.setFlags(self.flags()                  |
#                    QGraphicsItem.ItemIsSelectable  |
                    QGraphicsItem.ItemIsMovable     |
                    QGraphicsItem.ItemSendsGeometryChanges |
                    QGraphicsItem.ItemIsFocusable   )
                    
        self.rect = QRectF(0,0,size,size)
                    
    def boundingRect(self):
        size = self.size
        return self.rect.adjusted(-size,-size,0,0)
        
    def paint(self, painter, option, widget):
        qp = painter

        if self.type == QPainterPath.CurveToElement:
            qp.setPen(QtGui.QColor(168, 34, 3))
            qp.setBrush(QBrush(QtGui.QColor(3, 50, 3), Qt.SolidPattern))
        elif self.type == QPainterPath.CurveToDataElement:
            qp.setPen(QtGui.QColor(168, 34, 3))
            qp.setBrush(QBrush(QtGui.QColor(3, 50, 200), Qt.SolidPattern))
        else:
            qp.setPen(QtGui.QColor(34, 34, 3))
            qp.setBrush(QBrush(self.color, Qt.SolidPattern))
            
        qp.drawEllipse(self.boundingRect())
    
    #'''    
    def mousePressEvent(self, e):
        if e.button() == 1:
            #self.parentItem().parentItem().deselectOthers(None)
            self.parentItem().setSelected(True)
            #self.parentItem().parentItem().bringToFront(self.parentItem().idd)
            #self.parentItem().update()
            
        elif e.button() == 2:
            self.parentItem().remove(self)
            
    def itemChange(self, change, value):
        
        if change == QGraphicsItem.ItemPositionChange:
            #print 'itemChange'
            self.parentItem().updateRect()
            p = self.parentItem().bezier.path()
            p.setElementPositionAt(self.idd, self.x(), self.y())
            self.parentItem().bezier.setPath(p);
            
        return super(HandleItem, self).itemChange(change, value)
        
    def setSize(self, size):
        self.prepareGeometryChange()
        self.size = size
        self.rect = QRectF(0,0,size,size)
        
    def setColor(self, color):
        self.color = color
        self.update()

class GlyphItem(QGraphicsItem):
    def __init__(self, path, fontname, character, mutable=False, parent=None):
        super(GlyphItem, self).__init__(parent=parent)
        filename = join(path, fontname + '.svg')
        self.character = character
        self.filename = filename
        self.fontname = fontname
        self.glyph_paths = load_svg_font(filename, [character])[0] #to_paths(filename, [character])[0]
        #poly = to_polygons(filename, [character])
        self.bound = QRectF(0,0,100,100)
        self.setFlags(self.flags()                   |
        #            QGraphicsItem.ItemIsSelectable  |
        #            QGraphicsItem.ItemIsMovable     |
                     QGraphicsItem.ItemIsFocusable   )

        self.contours = []             
        for contour in self.glyph_paths:
            self.contours.append(BezierItem(contour, color=Qt.green, parent=self,idd=0))
            
            
    def boundingRect(self):
        #pass
        topleft = []
        botright = []
        #for contour in self.contour_group.childItems():
        for contour in self.contours:
            rect = contour.boundingRect()
            topleft.append([rect.x(),rect.y()])
            botright.append([rect.x() + rect.width(), rect.y() + rect.height()])
        
        
        topleft = np.min(np.array(topleft), axis=0)
        botright = np.max(np.array(botright), axis=0)
        
        return QRectF(topleft[0], topleft[1], botright[0] - topleft[0], botright[1] - topleft[1])
            
    def addPath(self, path):
        pass
    
    def paint(self, painter,  option, widget):
        pass

    
class GlyphWidget(QGraphicsWidget):
    def __init__(self, path, fontname, character, mutable):
        super(GlyphWidget, self).__init__()
        
        self.item = GlyphItem(path, fontname, character, parent = self, mutable=mutable)
        self.scale(0.05,-0.05)
        
    def mousePressEvent(self, e):
        pass
    
class Demo(QtGui.QMainWindow):
    def __init__(self, root):
        super(Demo, self).__init__()
        self.root = root
        self.initUI()
    
    def initUI(self):
        rightLayout = QVBoxLayout()
        gridLayout = QGridLayout()
        #gridLayout.addWidget(self.spnNbsize, 3,1)
        rightLayout.addLayout(gridLayout)
        rightPanel = QWidget()
        rightPanel.setLayout(rightLayout)
        
        #list of fonts
        self.classList = QListWidget()
        self.files = sorted([f[:-4] for f in os.listdir(self.root) \
        if os.path.isfile(join(self.root, f)) and f.endswith('.svg')])[:20]
            
        self.classList.addItems(self.files)
        self.classList.setCurrentRow(0)
        
        rightLayout.addWidget(self.classList)
        self.connect(self.classList, SIGNAL("itemSelectionChanged()"), self.listChanged)
        
        #the list of characters
        ncols = 9
        nrows = int(len(cm.all_chars) / ncols + 1)
        charList = QTableWidget(nrows, ncols)
        charList.setCurrentCell(0, 0)
        self.charTable = charList
        for col in range(ncols):
            charList.setColumnWidth(col, 25)
            for row in range(nrows):
                if col * nrows + row >= len(cm.all_chars): break
                item = QTableWidgetItem(cm.all_chars[col * nrows + row])
                charList.setItem(row, col, item)
                
        self.connect(self.charTable, SIGNAL("itemSelectionChanged()"), self.itemSelectionChanged)
        rightLayout.addWidget(charList)
        
        #Add the panel to a dock for flexibility
        rightDock = QDockWidget("Fonts and Characters", self)
        rightDock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        rightDock.setWidget(rightPanel)
        self.addDockWidget(Qt.RightDockWidgetArea, rightDock)
        
        self.mainview = QGraphicsView()
        self.mainview.scale(0.6,-0.6)
        self.scene = QGraphicsScene(self)
        self.scene.setBackgroundBrush(QBrush(Qt.white,Qt.SolidPattern))
        glyph = GlyphItem(self.root, str(self.classList.currentItem().text()), 'Q', mutable=False)
        
        self.ch = glyph
        self.scene.addItem(glyph)
        self.mainview.setScene(self.scene)
        self.setCentralWidget(self.mainview)

        #Bottom dock shows all available charaters, rendered..
        self.overview = QGraphicsView()
        self.scene_1 = QGraphicsScene(self)
        self.overview.setScene(self.scene_1)
        layout = QGraphicsGridLayout()
        self.previewLayout = layout
        layout.setSpacing(40)
        #add all characters' outline to view_1
        for i in [0,1,2]:
            for j in range(8):
                ch = GlyphWidget(self.root, 
                                 str(self.classList.currentItem().text()), \
                                 cm.capital_chars[i * 8 + j], mutable=False)
                self.scene_1.addItem(ch)
                layout.addItem(ch, i, j)
                
        form = QGraphicsWidget()
        form.setLayout(layout)
        self.scene_1.addItem(form)

        bottomDock = QDockWidget("Glyph" , self)
        bottomDock.setAllowedAreas(Qt.BottomDockWidgetArea)
        bottomDock.setWidget(self.overview)        
        self.addDockWidget(Qt.BottomDockWidgetArea, bottomDock)       
        
    def listChanged(self):
        self.scene.removeItem(self.ch)
        ch = GlyphItem(self.root, str(self.classList.currentItem().text()),
                           self.charTable.currentItem().text(), mutable=True)
        self.scene.addItem(ch)
        self.ch = ch
        
    def itemSelectionChanged(self):
        self.scene.removeItem(self.ch)
        ch = GlyphItem(self.root, str(self.classList.currentItem().text()),
                           self.charTable.currentItem().text(), mutable=True)
        self.scene.addItem(ch)
        self.ch = ch
        #self.scene.update()
        
def main(args):
    app = QtGui.QApplication(sys.argv)
    demo = Demo('/media/phan/BIGDATA/SMARTFONTS/ttf')
    demo.show()
    app.exec_()
 
if __name__=='__main__':
    main(sys.argv[1:])