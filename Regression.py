'''
Created on 24 Jul, 2014

@author: phan
'''
from sklearn.mixture import GMM
import numpy as np
from numpy.linalg import inv
from scipy.stats import multivariate_normal
from os.path import join
import Visualization as vz
from matplotlib import pyplot as plt
#from sklearn.cluster import KMeans

def gmm_bruteforce(X, gmm):
    '''
    predict missing data using bruteforce
    '''
    

def gmm_predict(X, gmm):
    '''
    X = (N x D') D' < D(gmm)
    http://pypr.sourceforge.net/mog.html#rasmussen2006
    '''
    Mu = gmm.means_
    D = len(Mu[0])
    K = gmm.n_components
    S = [s * np.identity(D) for s in gmm.covars_]
    #S = gmm.covars_
    Pi = gmm.weights_
    Lambda = [inv(s) for s in S]
    #n. dimensions for x_a (unknown)
    #pD = D - X.shape[1]
    #maskA = np.array([True] * pD + [False] * X.shape[1])
    #maskB = np.invert(maskA)
    
    Y = []
    for x in X:
        #print '.',
        #remove some dimentions
        maskA =  np.repeat(False, D)
        maskA[np.isnan(x)] = True
        #maskA[np.random.permutation(D)[:21]] = False #np.array([i > 0.2 for i in x])
        maskB = np.bitwise_not(maskA)
        x_p = x[maskB]
        
        #pgmm = GMM(n_components=40)
        pMu = []    
        pS = []
        pPi = []
        tmp = sum([Pi[k_] * multivariate_normal.pdf(x_p[None, :], \
        mean=Mu[k_][maskB],cov=S[k_][maskB][:, maskB]) for k_ in range(K)])        
        for k in range(K):
            #pS_k = S[k][maskA][:, maskA] #
            pS_k = inv( Lambda[k][maskA][:, maskA] )
            #pMu_k = Mu[k][maskA] - pS_k.dot( Lambda[k][maskA][:,maskB] ).dot( (x_p - Mu[k][maskB]) )
            pMu_k = Mu[k][maskA] + S[k][maskA][:, maskB].dot(inv(S[k][maskB][:, maskB])).dot( (x_p - Mu[k][maskB]) )
            
            pPi_k = Pi[k] * multivariate_normal.pdf(x_p[None, :], \
            mean=Mu[k][maskB],cov=S[k][maskB][:, maskB]) / tmp
            
            pMu.append(pMu_k)
            pS.append(pS_k)
            pPi.append(pPi_k)
            
        #sorted_pPi = sorted(zip(range(K), pPi), key=lambda y: y[1])[::-1]
        y = x.copy() #np.zeros_like(x) + 0.001 #np.repeat(np.nan, len(x)) #
        y[maskA] = 0
        #y[maskA] = pMu[sorted_pPi[0][0]]
        for k in range(K): 
            y[maskA] += pPi[k] * pMu[k]
        
        #y[maskA] /= K
        Y.append( y )
        
    return np.array(Y)

if __name__=='__main__':
    import Common as cm
    import sys, os
    from Common import nlab2rgb
    from numpy.random import permutation
    from sklearn.metrics import mean_squared_error
    from fast.Distance import hausdorff, hausdorff_abs
    from ColorManifolds import align_colors
    from KernelKMeans import KernelKMeans
    
    #find local styles (within a window)
    PATH = '/media/phan/BIGDATA/DPAINTINGS'
    IPATH = join(PATH, 'img')
    OPATH = join(PATH, 'data')
    # Read data file
    print "READING DATA..."
    
    if len(sys.argv) > 1:
        authors = sys.argv[1:]
    else:
        authors = os.listdir(join(PATH, 'data'))
    
    Xs = {}
    Xs_orig = {}
    kkms = {}
    labels = {}

    for author in authors:
        if os.path.isfile(join(PATH, 'data', author, 'models.npz')) and \
            os.path.isfile(join(PATH, 'data', author, 'dpaintings.npz')):
            data = np.load(join(OPATH, author, 'dpaintings.npz'))
            
            if (not 'concat_lab' in data) or (not 'concat_order' in data):
                continue
            X = data['concat_lab']
            Xs[author] = X.reshape((-1, X.shape[1] * X.shape[2])) #[::2]
            Xs_orig[author] = data['orig_lab'].reshape((-1,  X.shape[1] * X.shape[2]))
            kkms[author] = data['kkm'].tolist()
            order = data['concat_order']
            labels[author] = data['labels'][order]
    
    auids = [0]
    
    for auid in auids:
        author = authors[auid]
        X = Xs[author]
        X_orig = Xs_orig[author]
        #kkm = kkms[author]
        
        np.random.seed(100)
        perm = np.random.permutation(len(X))
        train_X = X[perm[::2]] #Y[::2]
        test_X = X[perm[1::2]] #X[1::2]
        
        gmm = GMM(n_components=80, n_iter=2000, thresh=0.0001, n_init=1, covariance_type='diag', init_params='wmc')   
        #gmm.means_ = np.array([X[labels[author]==l].mean(axis=0) for l in range(kkm.n_clusters)])
        #gmm.weights_ = np.array([np.sum(labels[author] == l) / float(len(X))  for l in range(kkm.n_clusters)])
        gmm.fit(train_X)
        
        trange = (0, -1)
        test_X_copy = test_X[trange[0]:trange[1]].copy()
        n_obs = 5
        
        np.random.seed(100)
        
        perm = np.random.permutation(10)[:10 - n_obs]
        perm = np.concatenate([[p * 3, p * 3 + 1, p * 3 + 2] for p in perm])
        print perm
        
        test_X_copy[:, perm] = np.nan
        test_X_copy = align_colors(test_X_copy, train_X.reshape((-1, 10, 3)), clustering=True, dupl=True)
        
        print 'testing length', len(test_X_copy)
        Y = gmm_predict(test_X_copy, gmm)
        total_dist = 0
        
        for y_pred, y_test in zip(Y, test_X[trange[0]:trange[1]]):
            total_dist += hausdorff(y_pred[np.bitwise_not(np.isnan(y_pred))].reshape((-1, 3)), 
                                           y_test[perm].reshape((-1, 3)), mode=1)
            
        #for y_pred, y_test in zip(Y, test_X[trange[0]:trange[1]]):
        #    total_dist += mean_squared_error(y_test[perm], y_pred[np.bitwise_not(np.isnan(y_pred))])
            
        print total_dist #/ len(Y)
    
    fig = plt.figure('compare')
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132)
    ax3 = fig.add_subplot(133)
    
    ax1.imshow(nlab2rgb(test_X[trange[0]:trange[1]].reshape((-1, 10, 3))), interpolation='none')
    ax2.imshow(nlab2rgb(test_X_copy.reshape((-1, 10, 3))), interpolation='none')
    ax3.imshow(nlab2rgb(Y.reshape((-1, 10, 3))), interpolation='none')
    
    plt.show()
