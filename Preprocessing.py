# -*- coding: utf-8 -*-
"""
Created on Sat Jul  5 10:29:36 2014

@author: phan
"""
from os.path import join
import Geometry as geo
import Common as cm
import os
import numpy as np

if __name__ == "__main__":
    path = '/media/phan/BIGDATA/SMARTFONTS/ttf'
    savepath = '/media/phan/BIGDATA/SMARTFONTS/data'
    #files = sorted([f for f in os.listdir(path) if f.endswith('.ttf')])
    #fontnames = [f[:-3] for f in files]
    count = 0
    files = ['AlegreyaSC-Italic.ttf']
    for f in files:
        print count
        count += 1
        fname = join(path, f)
        outlines = geo.to_polygons(fname, cm.all_chars)
        
        np.savez(join(savepath, f + '.npz'), \
        outlines=outlines, \
        annotations=[[]] * 62, \
        segments=[[]] * 62, \
        chars=list(cm.all_chars))