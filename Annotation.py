# -*- coding: utf-8 -*-
"""
Created on Fri May 16 12:24:56 2014

@author: phan
"""

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode PyQt4 tutorial 

In the example, we draw randomly 1000 red points 
on the window.

author: Jan Bodnar
website: zetcode.com 
last edited: September 2011
"""

import sys, random, os, re
from os.path import join
import numpy as np

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import (QPoint, SIGNAL)
from PyQt4.QtGui import (QApplication, QCursor, QDialog,
        QDialogButtonBox, QFileDialog, QFont, QFontComboBox,
        QFontMetrics, QGraphicsItem, QGraphicsPixmapItem,
        QGraphicsScene, QGraphicsTextItem, QGraphicsView, QGridLayout,
        QHBoxLayout, QLabel, QMatrix, QMenu, QMessageBox, QPainter,
        QPen, QPixmap, QPrintDialog, QPrinter, QPushButton, QSpinBox,
        QStyle, QTextEdit, QVBoxLayout, QListWidget,QBrush, QColor, QImage)

class Annotator(QtGui.QWidget):
    def __init__(self, root):
        super(Annotator, self).__init__()
        self.root = root
        pattern = re.compile(r'(.jpg)|(.jpeg)|(.JPG)|(.png)|(.bmp)|(.tiff)|(.pgm)')
        #self.files = [f for f in os.listdir(root) if os.path.isfile(join(root, f)) and pattern.match(f) is not None]
        self.files = sorted([f for f in os.listdir(root) \
        if os.path.isfile(join(root, f)) and f.endswith('.pgm')])
        self.current_file_ = 0
        self.background = QImage(join(root , self.files[0]))
        
        #self.current_stroke_ = 0
        self.strokes_ = []
        #self.pos_ = []
        self.moving_ = None
        self.current_stroke_ = 0
        self.strokes_ += [[]]
        self.load()
        self.pos_ = self.strokes_[0]
        self.initUI()
        
    def initUI(self):
        '''
        '''
        #self.setGeometry(0, 0, 600, 600)
        #self.setWindowTitle('Points')
        #self.show()
        
    def load(self):
        data_file = join(self.root, self.files[self.current_file_]) + '.npy'
        if os.path.isfile(data_file):
            data = np.load(data_file)
            if data.shape[0] >= 1:
                self.strokes_[0] = [QPoint(p[0], p[1]) for p in data[0]]
                for i in range(1, data.shape[0]):
                    self.strokes_.append([QPoint(p[0], p[1]) for p in data[i]])
        
    def save(self):
        data = []
        for strk in self.strokes_:
            data.append(  [(p.x(), p.y()) for p in strk])
        np.save(join(self.root, self.files[self.current_file_]) +  '.npy', np.asarray(data))

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        
        qp.begin(self)
        #qp.translate(100, 100)
        qp.drawImage(0, 0, self.background)
        qp.setPen(QtGui.QColor(168, 34, 3))
        qp.setFont(QtGui.QFont('Arial', 20))        
        qp.drawText(20, 20, str(self.current_stroke_))
        self.drawPoints(qp)
        qp.end()
        
    def setCurrentFile(self,idx):
        if idx >= 0 and idx < len(self.files):
            self.current_file_ = idx
            self.background = QImage( join(self.root, self.files[self.current_file_]) )
            self.moving_ = None
            self.current_stroke_ = 0
            self.strokes_ = []
            self.strokes_ += [[]]
            self.load()
            self.pos_ = self.strokes_[0]
            self.update()
        
    def keyPressEvent(self, e):
        #idd = self.current_stroke_
        if e.text() == 'q':
            if self.current_stroke_ > 0:
                idd = self.current_stroke_ - 1
        elif e.text() == 'e':
            idd = self.current_stroke_ + 1
        elif e.text() == ' ' or e.text() == 'b' or e.text() == '-'  or e.text() == '+':
            if e.text() == ' ':
                self.save()
                self.current_file_ += 1
            if e.text() == 'b':
                self.save()
                self.current_file_ -= 1
            if e.text() == '-':
                self.current_file_ -= 1
            if e.text() == '+':
                self.current_file_ += 1            
                
            self.background = QImage( join(self.root, self.files[self.current_file_]) )
            self.moving_ = None
            self.current_stroke_ = 0
            self.strokes_ = []
            self.strokes_ += [[]]
            self.load()
            self.pos_ = self.strokes_[0]
            idd = 0
        else:   
            idd = int(e.text())
            
        self.current_stroke_ = idd
        if idd == len(self.strokes_):
            self.strokes_ += [[]]
        self.pos_ = self.strokes_[idd]
        self.update()
        
    def mousePressEvent(self, e):
        add = True
        if e.button() == 4:
            add = False
            del self.strokes_[self.current_stroke_]
            self.current_stroke_ = 0
            self.update()
            return
            
        for ip, p in enumerate(self.pos_):
            if ((p.x() - e.pos().x()) * (p.x() - e.pos().x()) + \
            (p.y() - e.pos().y()) * (p.y() - e.pos().y())) < 100:
                if e.button() == 1:
                    self.moving_ = ip
                elif e.button() == 2:
                    #self.pos_.pop(ip)
                    del self.strokes_[self.current_stroke_][ip]
                    if len(self.strokes_[self.current_stroke_]) == 0:
                        del self.strokes_[self.current_stroke_]
                        self.current_stroke_ = 0
                        self.pos_ = self.strokes_[0]
                    #print e.button()
                add = False
        if add:
            self.pos_ += [e.pos()]

        self.update()
                
    def mouseReleaseEvent(self, e):
        self.moving_ = None
        
    def mouseMoveEvent(self, e):
        if self.moving_ is not None:
            self.pos_[self.moving_] = e.pos()
            self.update()
        #print "pos = " + str(e.pos().x())
        
    def drawPoints(self, qp):
        #print list(QColor.colorNames())
        colorList = list(QColor.colorNames())
        for idd, strk in enumerate(self.strokes_):
            color = QColor(colorList[idd+10])
            if idd == self.current_stroke_:
                qp.setPen(QPen(QBrush(color), 4))
            else:
                qp.setPen(QPen(QBrush(color), 2))
            #size = self.size()
            if len(strk) > 0:
                qp.drawPolyline(*strk)
            for p in strk:
                #x = random.randint(1, size.width()-1)
                #y = random.randint(1, size.height()-1)
                x = p.x()
                y = p.y()
                #qp.drawPoint(x, y)     
                qp.drawEllipse(x-5,y-5, 10, 10)
                
class MainForm(QDialog):
    def __init__(self, parent=None, root='.'):                
        super(MainForm, self).__init__(parent)
        rightPanel = QVBoxLayout()
        self.classList = QListWidget()
        self.files = sorted([f for f in os.listdir(root) \
        if os.path.isfile(join(root, f)) and f.endswith('.pgm')])
        self.classList.addItems(self.files)
        self.classList.setCurrentRow(0)
        rightPanel.addWidget(self.classList)
        self.connect(self.classList, SIGNAL("itemSelectionChanged()"), self.listChanged)
        
        layout = QHBoxLayout()
        self.ann = Annotator(root)
        layout.addWidget(self.ann, 1)
        layout.addLayout(rightPanel)
        self.setLayout(layout)
        
    def listChanged(self):
        if hasattr(self,'files'):
            self.ann.setCurrentFile(self.classList.currentRow())
            self.ann.setFocus()
        
def main():
    #app = QtGui.QApplication(sys.argv)
    if len(sys.argv) == 1:
        root = '.'
    else:
        root = sys.argv[1]
    #ex = Annotator(root)
    #sys.exit(app.exec_())
    app = QApplication(sys.argv)
    form = MainForm(root=root)
    rect = QApplication.desktop().availableGeometry()
    form.resize(int(rect.width() * 0.6), int(rect.height() * 0.9))
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()