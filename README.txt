Deps: (python 2.7)

- freetype-py
- shapely (with optional geos for speeding up)
- numpy & scipy
- matplotlib
- scikits-learn
- scikits-image
- gensim
- gicp
- GPy
- networkx
