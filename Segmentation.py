# -*- coding: utf-8 -*-
"""
Created on Tue Apr 22 14:00:40 2014

@author: phan
"""
import numpy as np
import math
import copy
from os.path import join
from freetype import *
from skimage import segmentation 
from skimage.io import imread
from skimage.draw import line
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from matplotlib.patches import Ellipse, Polygon
from matplotlib.colors import cnames
#from shapely.geometry import asLineString
from shapely.geometry import *
import shapely
from numpy.linalg import norm
from scipy.spatial.distance import euclidean as euc
from scipy.spatial.distance import mahalanobis
from Geometry import Angle as geoa
from Geometry import min_curvature, ave_normal, ave_tangent, angle_between1
from Geometry import inf_point as infp
from Geometry import to_polygons
from Common import eps, neighbours, hausdorff_distance, color_names
from fast.Distance import hausdorff
from Graph import skel, rename_node
import threading
import time
from cgal import CGAL as cgal
import networkx as nx
from gicp import gicp

import Visualization as vz
from scipy import interpolate
from scipy import signal
import Feature as ft
from sklearn.decomposition import PCA
import Common as cm

def T(vector):
    return np.array([-vector[1], vector[0]])

def mark(image, ann):
    def line_(x1,y1,x2,y2, color):
        rr, cc = line(y1, x1, y2, x2)
        image[rr,cc] = color
    
    idd = 0
    
    for segment in ann:
        first = segment[0]
        idd += 1
        for second in segment[1:]:
            line_(first[0], first[1], second[0], second[1], idd)
            first = second

def longest_path(polytree, terms):
    terms = list(set(terms))
    bestpath = []
    bestlength = 0
    for t1 in range(len(terms)):
        for t2 in range(t1):
            if t1 != t2:
                path = nx.shortest_path(polytree, terms[t1], terms[t2], 'd')
                length = nx.shortest_path_length(polytree, terms[t1], terms[t2], 'd')
                if length > bestlength:
                    bestlength = length
                    bestpath = path
                
    return bestpath

def connect_subgraphs(subgraphs, graph):
    #restore the graph to 
    terms = []
    n_nodes = 0
    all_nodes = []
    for subgraph in subgraphs:
        n_nodes += len(subgraph.nodes())
        all_nodes.extend(subgraph.nodes())
        
        terms.extend( [node for node, degree in subgraph.degree() if degree == 1] ) # degree_iter
    
    candidates = []
    for t0 in range(len(terms)):
        for t1 in range(t0):
            #if t0 != t1: 
            paths = list(nx.all_simple_paths(graph, terms[t0], terms[t1]))
            for path in paths:
                #if set(path).issuperset(set(all_nodes)):
                # make sure that the path covers most of the nodes
                if len( set(path).intersection(set(all_nodes)) ) > len(all_nodes) * 0.6:
                    #length = nx.shortest_path_length(graph, t0, t1, 'd')
                    length = sum([graph.edge[path[p]][path[p+1]]['d'] for p in range(len(path) - 1)])
                    candidates.append((path, length))
    
    if len(candidates) > 0:                    
        candidates = np.array(candidates)
        idd = np.argmin(candidates[:, 1])
        return candidates[idd, 0] #nx.subgraph(graph, candidates[idd, 0])
    else:
        return []
        
def compare_graphs(graph1, graph2):
    terms1 = [node for node, degree in graph1.degree() if degree == 1] # degree_iter
    terms2 = [node for node, degree in graph2.degree() if degree == 1] # degree_iter
    if len(terms2) != 2 or len(terms1) != 2:
        return 1e10
    
    cases = [(0, 1), (1, 0)]
    min_d = 1e10
    for frm, to in cases:
        d = 0
        path1 = list(nx.all_simple_paths(graph1, terms1[frm], terms1[to]))[0]
        path2 =  list(nx.all_simple_paths(graph2, terms2[0], terms2[1]))[0]
        poly1 = np.ascontiguousarray( [graph1.node[p]['pos'].tolist() for p in path1] )
        poly2 = np.ascontiguousarray( [graph2.node[p]['pos'].tolist() for p in path2] )

        d = hausdorff(poly1, poly2) #+ math.acos(v1.dot(v2) / (norm(v1) * norm(v2))) * 50000.0 #+ ft.earthmover(feat1, feat2) * 200000

        if d < min_d:
            min_d = d

    return min_d
            
def partition(graph_, min_curv=1.0, min_length=100.0):
    '''
    partition a graph into simple open polyline
    '''
    graph = graph_.copy()
    def _remove_cycles(g):
        cycles = nx.cycle_basis(g)
        for cycle in cycles:
            _split(cycle[0], g)
            _split(cycle[len(cycle) / 2], g)    
            
    def _break(g):
        _coms = nx.connected_component_subgraphs(g)
        for _com in _coms:
            for node in _com.nodes():
                if node.startswith('spl'):
                    split = len(node.split('.')[0]) + 1
                    rename_node(_com, node, node[split:])
        return _coms
        
    def _split(n, g):
        nbs = g.neighbors(n)
        for idx, nb in enumerate(nbs):
            newnode = 'spl' + str(idx) + '.' + n
            #rename_node(g, n, newnode)
            attr = g.node[n].copy()
            g.add_node(newnode, attr)
            g.add_edge(newnode, nb, g.edge[nb][n] )
            g.add_edge(nb, newnode, g.edge[n][nb] )
            
        g.remove_node(n)
    
    flag = False    
    for node, degree in graph.degree(): # degree_iter
        if degree >= 3:
            _split(node, graph)
            flag = True
            
    #_remove_cycles(graph)
        
    #if the graph is not splittable. randomly split it into halves
    #if not flag:
    #    _split(graph.nodes()[0], graph)

    subgraphs = nx.connected_component_subgraphs(graph)
    #for subgraph in subgraphs:
    #    if len(nx.cycle_basis(subgraph)) != 0:
    #        _remove_cycles(subgraph)
            
    coms = []
    for subgraph in subgraphs:
        #rename the nodes
        for node in subgraph.nodes():
            if node.startswith('spl'):
                split = len(node.split('.')[0]) + 1
                rename_node(subgraph, node, node[split:])
        k = 2
        if len(subgraph.nodes()) < 4:
            coms.append(subgraph)
            continue
        
        terms1 = [node for node, degree in subgraph.degree() if degree == 1] # degree_iter
        if len(terms1) != 2:
            terms1 = [subgraph.nodes()[0], subgraph.nodes()[1]]
            #coms.append(subgraph)
            #continue
        
        paths = list(nx.all_simple_paths(subgraph, terms1[0], terms1[1]))
        if len(paths) != 0:
            maxpath = np.argmax([sum([subgraph.edge[e[0]][e[1]]['d'] for e in subgraph.edges(p)]) for p in paths])
            path1 = np.array( paths[maxpath] )
        else:
            coms.append(subgraph)
            continue
        
        points = np.array([subgraph.node[p]['pos'].tolist() for p in path1])
        
        #if sum([euc(points[p],points[p+1]) for p in range(len(points)-1)]) < min_length:
        #    coms.append(subgraph)
        #    continue
        
        if not len(path1) > k:
            coms.append(subgraph)
            continue
        
        #u = np.arange(0, 1.01, 0.2)
        try:
            tck, u = interpolate.splprep([points[:, 0], points[:, 1]], s=len(points), k=k)
        except Exception, e:
            coms.append(subgraph)
            continue
            
        de1 = np.array( interpolate.splev(u, tck, der=1) ).T
        de2 = np.array( interpolate.splev(u, tck, der=2) ).T
        curvs = (de1[:, 0] * de2[:, 1] - de1[:, 1] * de2[:, 0]) / (de1[:, 0]**2 + de1[:, 1]**2)
        peakind = signal.argrelextrema(curvs,comparator= lambda x, y: x > y)
        '''
        peakind = []
        for p in range(1, len(points)-1):
            if abs(angle_between1(points[p - 1] - points[p], points[p] - points[p+1])) > np.radians(45):
                peakind.append(p)
        peakind = [np.array(peakind)]
        '''   
        if len(peakind[0]) > 0:
            for nodex, node in enumerate(path1[peakind[0]]):
                #if abs( curvs[ peakind[0][nodex] ] ) > min_curv:
                _split(node, subgraph)
            
            coms.extend(_break(subgraph))
        else:
            coms.append(subgraph)
        
    return coms
        
def segment_simple(image, markers):
    '''
    '''
    #find local 
    markers[~image] = -1
    labels_rw = segmentation.random_walker(image, markers)
    return labels_rw

def _snap(distances, contours, point):
    for ddx, dd in enumerate(distances):
        for dx, d in enumerate(dd):
            d1 = contours.project(point)
            if abs(d - d1) < 5:
                return (ddx, dx)
    
    return None

def min_dist(a, b):
    d = 0
    for p in a:
        d += np.min([euc(p, p1) for p1 in b])
    return d

def segment_graph_auto(graph, ref_graph, ref_anns):
    '''
    segment a graph using template
    '''
    graphe = skel([graph], mode = 0)[0]
    graph = skel([graph], mode=1)[0]
    ref_graphe = skel([ref_graph], mode=0)[0]
    ref_graph = skel([ref_graph], mode=1)[0]
    
    #try to fit the ref_graph onto the graph, nodes_iter -> node
    b_data = np.ascontiguousarray([graph.node[node]['pos'] for node in graph.nodes() \
                                   if graph.node[node]['mode'] == cgal.SKELETON]) #- 500.
    a_data = np.ascontiguousarray([ref_graph.node[node]['pos'] for node in ref_graph.nodes() \
                                   if ref_graph.node[node]['mode'] == cgal.SKELETON]) #/ 2000.
    
    corres = np.zeros((len(a_data), 3),dtype='double')
    re = gicp.icp(a_data, b_data, corres, 500.0, 1e-2)
    a_data = np.hstack((a_data, np.tile([0,1], a_data.shape[0]).reshape(a_data.shape[0], 2)))
    trans_a_data = re.dot(a_data.T).T
    
    #now transfer the terminals' positions back to the graph
    node_id = 0
    for node in ref_graph.nodes(): # nodes_iter
        ref_graph.node[node]['pos'] = trans_a_data[node_id][0:2]
        node_id += 1
    
    #over-partition a graph 
    anns = [[] for _ in range(len(ref_anns))]
    subgraphs = partition(graph)
    #vz.draw_graphs(subgraphs + [ref_graph]) #+ [ref_graph]
    
    #for each subgraph, find its correspondence annotation (segment)
    terms = [[] for _ in range(len(ref_anns))]
    idd = 0
    #decide which partition belongs to which part of the character
    for subgraph in subgraphs:
        #print color_names[idd], 
        idd += 1
        '''
        terms1 = [node for node, degree in subgraph.degree_iter() if degree == 1]
        path1 = list(nx.all_simple_paths(subgraph, terms1[0], terms1[1]))[0]
        poly1 = np.array([subgraph.node[p]['pos'].tolist() for p in path1])
        vz.draw_curvatures(poly1)
        '''
        best_id = 0
        best_d = 1e10
        
        #TODO: remove "bad" partition here. (possibly consider the angle bisectors vs inner_bisectos)
        terms_ = [node for node, degree in subgraph.degree() if degree == 1] # degree_iter
        
        if len(terms_) != 2:
            continue
        
        #attemp to avoid bad segments (probably wrong)
        '''
        path_ = list(nx.all_simple_paths(subgraph, terms_[0], terms_[1]))[0]
        angs = []
        for nodex in range(len(path_) - 1):
            vec1 = -subgraph.node[path_[nodex]]['pos'] + subgraph.node[path_[nodex + 1]]['pos'] 
            nbs = nx.neighbors(graphe, path_[nodex])
            vec2s = []
            for nb in nbs:
                if graphe.node[nb]['mode'] == cgal.NON_SKELETON:
                    vec2s.append(- subgraph.node[path_[nodex]]['pos'] + graphe.node[nb]['pos'])
            
            for vec2 in vec2s:
                ang = abs(angle_between1(vec1, vec2))
                angs.append(ang)
        
        #if np.isnan( np.degrees(abs(np.array(angs).mean() - np.radians(90))) ):
        #    pass
        #print abs(np.array(angs).mean() - np.radians(90))
        if len(angs) > 2:
            if abs(np.array(angs).mean() - np.radians(90)) > np.radians(30):
                continue
        #else:
        #    continue
        '''
        #----------------------------------------------------------------------------
        for annx, ann in enumerate(ref_anns):
            if len(ann) == 0:
                continue
            
            if not all([node in ref_graphe.nodes() for node in ann]):
                continue

            d = compare_graphs(subgraph, nx.subgraph(ref_graphe, ann))
            if d < best_d:
                best_d = d
                best_id = annx
        
        #terms_ = [node for node, degree in subgraph.degree_iter() if degree == 1]
        terms[best_id].extend([subgraph.nodes().index(terms_[0]) + len(anns[best_id]), \
                               subgraph.nodes().index(terms_[1]) + len(anns[best_id])])
        
        anns[best_id].extend(subgraph.nodes())
        
    paths = [[] for _ in range(len(ref_anns))]            
    #Really extract the ending points for a path-annotation
    for annx, ann in enumerate(anns):
        if len(ann) == 0:
            continue
        newann = []
        for a in ann:
            if a.startswith('spl'):
                newann.append(a[5:])
            else:
                newann.append(a)
        
        newann_ = copy.deepcopy(newann)
        newann = set(newann)
        subgraph = nx.subgraph(graph, newann)
        
        coms = nx.connected_component_subgraphs(subgraph)
        if len(coms) > 1:
            path = connect_subgraphs(coms, graph)
        else:
            path = longest_path(subgraph, [newann_[t] for t in terms[annx]])
            
        if len(path) == 0:
            continue
        
        d1 = euc(graph.node[path[0]]['pos'],ref_graphe.node[ref_anns[annx][0]]['pos'])
        d2 = euc(graph.node[path[0]]['pos'], ref_graphe.node[ref_anns[annx][-1]]['pos'])
        if d1 < d2:
            paths[annx] = path
        else:
            paths[annx] = list(reversed(path))
        #terms = [t[5:] for t in coms[maxcom].nodes() if t.startswith('spl.')]
    
    #deal with empty annotation. Try to find one from neighboring points
    for annx in range(len(anns)):
        if len(anns[annx]) <= 2:
            if len(ref_anns[annx]) == 0:
                print "!(%d)" % annx, 
                continue
            ref_skel = [ref_graphe.node[a]['pos'] for a in ref_anns[annx]]
            #ref_line = asLineString(ref_skel)
            terms1 = [[],[]]
            for node in graphe.nodes(): # nodes_iter
                node_pos  = graphe.node[node]['pos']
                for i in [0, -1]:
                    if euc(ref_skel[i], node_pos) < 150:
                        terms1[abs(i)].append(node)
            
            min_d = 1e10
            min_path = []
            for t1 in terms1[0]:
                for t2 in terms1[1]:
                    path = nx.all_simple_paths(graphe, t1, t2).next()
                    this_skel = [graphe.node[p]['pos'] for p in path]
                    #d = hausdorff_distance(np.ascontiguousarray(ref_skel), np.ascontiguousarray(this_skel))
                    d = hausdorff(np.ascontiguousarray(ref_skel), np.ascontiguousarray(this_skel),mode=1)
                    if d < min_d:
                        min_d = d 
                        min_path = path
                    #length = sum([graphe.edge[path[p]][path[p+1]]['d'] for p in range(len(path)-1)])
                    
            paths[annx] = min_path
            #remove overlapping nodes from other anns
            for annx1 in range(len(paths)):
                if annx1 != annx:
                    for node in min_path:
                        if node in paths[annx1]:
                            paths[annx1].remove(node)
                                
    return paths 
    #return anns

def segment_graph_old(graph, outlines, anns, modes=None):
    '''
    @mode [stroke or cap]
    '''
    ''' search for unoccupied inner bisectors '''
    if modes == None:
        modes = ['stroke'] * len(anns)
        
    def _return_empty(caps, ppp, all_points,idd=-1):
        print '[%d]' % (idd),
        caps.append([])
        ppp.append([])
        all_points.append([])   
            
    def _recur(_paths, _term, _idd):
        nbs = nx.neighbors(graph, _term)
        for nb in nbs:
            if graph.node[nb]['occupied'] == -1:
                graph.node[nb]['occupied'] = _idd
                if (graph.node[nb]['mode'] == cgal.SKELETON):
                    _recur(_paths, nb, _idd)
                else:
                    _paths.append(nb)
                    
    #preparing the contour
    contours = []
    for outline in outlines:
        polygon = asPolygon(outline)
        
        if not polygon.is_empty:
            if not polygon.is_valid:
                polygon = polygon.buffer(0)
            contours.append(polygon)
    
    for x in range(len(contours)):
        for y in range(len(contours)):
            if x!= y and contours[x] != None and contours[y] != None:
                if contours[x].contains(contours[y]):
                    contours[x] = contours[x].difference(contours[y])
                    contours[y] = None
    contours = MultiPolygon([c for c in contours if c != None])
    if not contours.is_valid:
        contours = contours.buffer(0)
    #---------------------------
    
    for node in graph.nodes(): # nodes_iter
        graph.node[node]['occupied'] = -1
    
    #gray out all nodes belong to the annotations    
    for annx, ann in enumerate(anns):
        if len(ann) == 0:
            continue
        
        if not isinstance(ann[0], str):
            continue
        
        for p in ann:
            graph.node[p]['occupied'] = annx
    
    ppp = []
    all_points = []
    caps = []
    for segx, seg in enumerate(anns):
        points_ = [[], [], [], []]
        if modes[segx] != 'stroke':
            continue
        
        #if encounter empty annotation, simply add empty data (this is okay)
        if len(seg) < 3:
            _return_empty(caps, ppp, all_points,segx)
            continue
            
        #forward a segment to segment_vector if it does not contain graph nodes
        if not isinstance(seg[0], str):
            print 'not annotation!'
            _return_empty(caps, ppp, all_points, segx)      
            continue
            
        parts = [[],[], [], []]
        for nodex, node in enumerate(seg):
            if nodex < len(seg) - 1:
                vec = graph.node[ seg[nodex + 1] ]['pos'] - graph.node[node]['pos']
            else:
                vec = graph.node[node]['pos'] - graph.node[ seg[nodex - 1] ]['pos']
                vec = vec / norm(vec)
                
            nvec = np.array([-vec[1], vec[0]]) #+ graph.node[node]['pos']
            C = -np.dot(nvec, graph.node[node]['pos'])
            nbs = nx.neighbors(graph, node)
            graph.node[node]['occupied'] = segx
            
            for _, nb in enumerate(nbs):
                status = graph.node[nb]['occupied']
                if status == -1:
                    #easy case, the outline is directly connected to the skeleton
                    if graph.node[nb]['mode'] == cgal.NON_SKELETON:
                        if np.dot(graph.node[nb]['pos'], nvec) + C > 0:
                            parts[0].append(nb)
                            parts[2].append(node)
                        else:
                            parts[1].append(nb)
                            parts[3].append(node)
                        graph.node[nb]['occupied'] = segx

                    #hard case, recursively find the outlines from a point on the skeleton
                    elif nodex != 0 and nodex != len(seg) - 1:
                        if len(parts[0]) == 0 or len(parts[1]) == 0:
                            continue
                        tmp = []
                        _recur(tmp, nb, segx)
                        if len(tmp) == 0:
                            continue
                        
                        if len(tmp)  > 1:
                            sgraph = graph.subgraph(tmp)
                            terms = [_nd for _nd, _dg in sgraph.degree() if _dg == 1] # degree_iter
                            if len(terms) == 2:
                                paths = []
                                if graph.has_edge(parts[0][-1], terms[0]) or graph.has_edge(parts[1][-1], terms[0]):
                                    paths = nx.all_simple_paths(sgraph, terms[0], terms[1]).next()
                                else:
                                    paths = nx.all_simple_paths(sgraph, terms[1], terms[0]).next()
                            else:
                                continue
                                
                        if len(tmp) == 1:
                            paths = tmp
                            
                        if np.dot(graph.node[nb]['pos'], nvec) + C > 0:
                            for _ in range(len(paths)):
                                parts[2].append(node)
                            parts[0].extend(paths)
                        else:
                            for _ in range(len(paths)):
                                parts[3].append(node)
                            parts[1].extend(paths)
                            
                        graph.node[nb]['occupied'] = segx
           
            points = [None, None, None, None]
            points[0], points[1], points[2], points[3] = ([graph.node[p]['pos'] for p in parts[0]], \
            [graph.node[p]['pos'] for p in parts[1]], \
            [graph.node[p]['pos'] for p in parts[2]], \
            [graph.node[p]['pos'] for p in parts[3]])
            points_ = points

        pp0, pp1 = copy.deepcopy(points_[0]), copy.deepcopy(points_[1]) #[graph.node[p]['pos'] for p in points_[0]], [graph.node[p]['pos'] for p in points_[1]]
        if len(pp0) == 0 or len(pp1) == 0:
            print '(%d)' % segx, 
            return [], [], []
        
        pp1.reverse()
        pp = pp0 + pp1
        caps.append([[0, len(pp) - 1], [len(pp0) - 1, len(pp0)]])
        ppp.append(pp) 
        all_points.append(points_)
    
    #Now simply assign remaining segments to the closest ref annotations
    polygons = [asPolygon(p) for p in ppp if len(p) > 2]
    for poly in polygons:
        #if not poly.is_valid:
        #    poly_ = poly.buffer(0)
        contours = contours.difference(poly.buffer(1))
        
    if contours.geom_type == 'Polygon' or contours.geom_type == 'PolygonAdapter':
        contours = [contours]   
        
    for segx, seg in enumerate(anns):
        if modes[segx] == 'cap':
            #if not contours.is_valid:
            #    _return_empty(caps, ppp, all_points)
            #    continue
                
            found = False
            if len(seg) > 1:
                ls = asLineString([graph.node[n]['pos'] for n in anns[segx]])
                for segment in contours:
                    if segment.intersects(ls):
                        if segment.intersection(ls).length > ls.length * 0.5:
                            tmp = np.array(segment.exterior)
                            ppp.append(tmp)
                            caps.append([])
                            all_points.append(tmp) #segments == points
                            found = True
                            break
            if not found:
                _return_empty(caps, ppp, all_points, segx)
         
    return ppp, caps, all_points
            
def segment_graph(graph, outlines, anns, modes=None): #, mirror=False, min_curv=0.001, angle_diff=15, search_vector_factor=4, step=25, nbsize=1, shearing=0, modes=None):
    '''
    @mode [stroke or cap]
    all_nodes - [left_outline,right_outline,  mid_left,  mid_right]
    '''
    ''' search for unoccupied inner bisectors '''
    if modes == None:
        modes = ['stroke'] * len(anns)
        
    def _return_empty(caps, ppp, all_points,idd=-1):
        print '[%d]' % (idd),
        caps.append([])
        ppp.append([])
        all_points.append([])   
            
    def _recur(_paths, _term, _idd):
        nbs = nx.neighbors(graph, _term)
        for nb in nbs:
            if graph.node[nb]['occupied'] == -1:
                graph.node[nb]['occupied'] = _idd
                if (graph.node[nb]['mode'] == cgal.SKELETON):
                    _recur(_paths, nb, _idd)
                else:
                    _paths.append(nb)
                    

    
    for node in graph.nodes(): # nodes_iter in old networkx
        graph.node[node]['occupied'] = -1
    
    #gray out all nodes that belong to the annotations    
    for annx, ann in enumerate(anns):
        if len(ann) == 0:
            continue
        
        if not isinstance(ann[0], str):
            continue
        
        for p in ann:
            graph.node[p]['occupied'] = annx
    
    ppp = [] #the segments' outline
    all_points = [] #the points along a stroke left, mid-left, right, mid-right
    caps = []
    
    #----------------------- BEGIN STROKE PROCESSING -----------------------------------------------
    #------- Process the stroke first by moving along the skeleton and pick corresponding outline vertices-------
    for segx, seg in enumerate(anns):
        points_ = [[], [], [], []]
        if modes[segx] != 'stroke':
            continue
        
        #if encounter empty annotation, simply add empty data (this is okay)
        if len(seg) < 3:
            _return_empty(caps, ppp, all_points,segx)
            continue
            
        #forward a segment to segment_vector if it does not contain graph nodes
        if not isinstance(seg[0], str):
            print 'not annotation!'
            _return_empty(caps, ppp, all_points, segx)      
            continue
            
        parts = [[],[], [], []]
        for nodex, node in enumerate(seg):
            if nodex < len(seg) - 1:
                vec = graph.node[ seg[nodex + 1] ]['pos'] - graph.node[node]['pos']
            else:
                vec = graph.node[node]['pos'] - graph.node[ seg[nodex - 1] ]['pos']
                vec = vec / norm(vec)
                
            nvec = np.array([-vec[1], vec[0]]) #+ graph.node[node]['pos']
            C = -np.dot(nvec, graph.node[node]['pos'])
            nbs = nx.neighbors(graph, node)
            graph.node[node]['occupied'] = segx
            
            for _, nb in enumerate(nbs):
                status = graph.node[nb]['occupied']
                if status == -1:
                    #easy case, the outline is directly connected to the skeleton
                    if graph.node[nb]['mode'] == cgal.NON_SKELETON:
                        if np.dot(graph.node[nb]['pos'], nvec) + C > 0:
                            parts[0].append(nb)
                            parts[2].append(node)
                        else:
                            parts[1].append(nb)
                            parts[3].append(node)
                        graph.node[nb]['occupied'] = segx

                    #hard case, recursively find the outlines from a point on the skeleton
                    elif nodex != 0 and nodex != len(seg) - 1:
                        if len(parts[0]) == 0 or len(parts[1]) == 0:
                            continue
                        tmp = []
                        _recur(tmp, nb, segx)
                        if len(tmp) == 0:
                            continue
                        
                        if len(tmp)  > 1:
                            sgraph = graph.subgraph(tmp)
                            terms = [_nd for _nd, _dg in sgraph.degree() if _dg == 1] # degree_ter in old networkx
                            if len(terms) == 2:
                                paths = []
                                if graph.has_edge(parts[0][-1], terms[0]) or graph.has_edge(parts[1][-1], terms[0]):
                                    paths = nx.all_simple_paths(sgraph, terms[0], terms[1]).next()
                                else:
                                    paths = nx.all_simple_paths(sgraph, terms[1], terms[0]).next()
                            else:
                                continue
                                
                        if len(tmp) == 1:
                            paths = tmp
                            
                        if np.dot(graph.node[nb]['pos'], nvec) + C > 0:
                            for _ in range(len(paths)):
                                parts[2].append(node)
                            parts[0].extend(paths)
                        else:
                            for _ in range(len(paths)):
                                parts[3].append(node)
                            parts[1].extend(paths)
                            
                        graph.node[nb]['occupied'] = segx
           
            points = [None, None, None, None]
            points[0], points[1], points[2], points[3] = ([graph.node[p]['pos'].tolist() for p in parts[0]], \
            [graph.node[p]['pos'].tolist() for p in parts[1]], \
            [graph.node[p]['pos'].tolist() for p in parts[2]], \
            [graph.node[p]['pos'].tolist() for p in parts[3]])
            points_ = points

        pp0, pp1 = copy.deepcopy(points_[0]), copy.deepcopy(points_[1]) #[graph.node[p]['pos'] for p in points_[0]], [graph.node[p]['pos'] for p in points_[1]]
        if len(pp0) == 0 or len(pp1) == 0:
            print '(%d)' % segx, 
            return [], [], []
        
        pp1.reverse()
        pp = pp0 + pp1
        caps.append([[0, len(pp) - 1], [len(pp0) - 1, len(pp0)]])
        ppp.append(pp) 
        all_points.append(points_)
    
    #If we don't care about caps, just quit now
    if not 'caps' in modes:
        return ppp, None, all_points
        
    #---------------------BEGIN CAP PROCESSING -----------------------------------------------------
    #------------Now simply assign remaining segments to the closest ref annotations----------------
    #preparing the contour (for Caps only)
    contours = []
    for outline in outlines:
        polygon = asPolygon(outline)
        
        if not polygon.is_empty:
            if not polygon.is_valid:
                polygon = polygon.buffer(0)
            contours.append(polygon)
    
    for x in range(len(contours)):
        for y in range(len(contours)):
            if x!= y and contours[x] != None and contours[y] != None:
                if contours[x].contains(contours[y]):
                    contours[x] = contours[x].difference(contours[y])
                    contours[y] = None
                    
    contours = MultiPolygon([c for c in contours if c != None])
    if not contours.is_valid:
        contours = contours.buffer(0)
    
        
    polygons = [asPolygon(p) for p in ppp if len(p) > 2]
    for poly in polygons:
        #if not poly.is_valid:
        #    poly_ = poly.buffer(0)
        contours = contours.difference(poly.buffer(1))
        
    if contours.geom_type == 'Polygon' or contours.geom_type == 'PolygonAdapter':
        contours = [contours]   
        
    for segx, seg in enumerate(anns):
        if modes[segx] == 'cap':
            #if not contours.is_valid:
            #    _return_empty(caps, ppp, all_points)
            #    continue
                
            found = False
            if len(seg) > 1:
                ls = asLineString([graph.node[n]['pos'] for n in anns[segx]])
                for segment in contours:
                    if segment.intersects(ls):
                        if segment.intersection(ls).length > ls.length * 0.5:
                            tmp = np.array(segment.exterior)
                            ppp.append(tmp)
                            caps.append([])
                            all_points.append(tmp) #segments == points
                            found = True
                            break
            if not found:
                _return_empty(caps, ppp, all_points, segx)
    #--------------------------- END CAP PROCESSING ----------------------------------------------
                
    return ppp, caps, all_points
    
def segment_vector( contours, anns, mirror=False, min_curv=0.0005, angle_diff=25,
                   search_vector_factor=4, step=20, nbsize = 2, shearing = 0): #fontname, character
    '''
    segmentation based on vector .
    '''
    #contours_ = to_polygons(fontname,chars=character)[0]
    contours = asMultiLineString(contours)
    theta = math.radians(shearing)
    #print 'entering segment_vector'
    '''
    figure = plt.figure(figsize=(1000,1000))
    axis = figure.add_subplot(111)
    '''
    length = np.max([asLineString(sgt).length for sgt in anns])
    ppp = []
    caps = []

    for sx, segment in enumerate(anns):
        sgt = asLineString(segment)
        points = [[],[],[]]
        pos = 0
        p0 = Point(segment[0])
        points0 = []
        #lns = []
        
        ''' sampling the trajectory '''
        while not p0.distance(Point(sgt.coords[-1])) < eps:
            p0 = sgt.interpolate(pos)
            pos += step
            points0.append(p0)
         
        skip = False
        #if len(segment) == 2:
        if contours.distance(Point(segment[0])) < 1e-5 \
        and contours.distance(Point(segment[1])) < 1e-5:  
            #print 'skipping ..'
            points_ = [[]] * 3
            points_[1] = copy.deepcopy(points0)
            points_[2] = copy.deepcopy(points0)
            points_[0] = copy.deepcopy(points0)
            skip = True
        
        ''' generate projections of a segment on the boundary '''
        if not skip:
            for p0x in range(len(points0)):
                p = [points0[p0x],None,None]
                nbs = [np.array(j) for j in neighbours(points0, p0x, 1)]
                n_vec = ave_normal(nbs, geoa)
                th = -np.sign(n_vec.dot([0, 1])) * theta
                rot_matrix = np.array([[math.cos(th), -math.sin(th)], [math.sin(th), math.cos(th)]])

                n_p0 = rot_matrix.dot( n_vec )
    
                vec1 = n_p0 * search_vector_factor  + np.array(p[0])
                vec2 = -n_p0 * search_vector_factor  + np.array(p[0])
                
                p[1] = LineString([p[0], vec1]).intersection(contours)
                p[2] = LineString([p[0], vec2]).intersection(contours)
     
                for i in [1,2]:
                    if not isinstance(p[i], MultiPoint):
                        if not isinstance(p[i],Point):
                            p[i] = []
                        else:
                            p[i] = [p[i]]
    
                    if len(p[i]) > 0:
                        if p[i][0].distance(p[0]) <= p[i][-1].distance(p[0]):
                            points[i].append(p[i][0])
                        else:
                            points[i].append(p[i][-1])
                    else:
                        points[i].append(Point(infp))
                        
                points[0].append(p[0])
                        
            ''' Now remove irrelevant points and create missing points'''
            assert(len(points[1]) == len(points[2]) == len(points[0]))
            points_ = [[],[],[]]
            for px in range(len(points[0])):
                p = [points[0][px], points[1][px], points[2][px]]
                valids = [True, True]
                #coefs = []
                for i in [1,2]:
                    if p[i].equals(infp):
                        valids[i - 1] = False
                        continue
                    
                    scale = nbsize
                    nbs = [np.array(j) for j in neighbours(points[i], px, scale)]
                    #if px >= scale and px < len(points[i]) - scale - 1:
                        #Detect and remove segments which do not near-parallel with the tangent
                    t = ave_tangent(nbs, geoa)
                    nbs0 = [np.array(j) for j in neighbours(points[0], px, scale)]
                    t0 = ave_tangent(nbs0, geoa)
                    if math.acos(np.round(t.dot(t0), decimals=8)) > math.radians(angle_diff):
                        valids[i - 1] = False
                    
                    #Detect and remove low curvature points
                    '''
                    cval, _ = min_curvature(nbs, geoa)
                    if cval < self.min_curv:
                        valids[i - 1] = False
                        continue
                    '''
    
                ''' mirror a point if its counterpart is missing '''
                if mirror:
                    for j, k in [(1,2),(2,1)]:
                        if (not valids[j - 1]) and valids[k - 1]:
                            valids[j - 1] = True
                            i = 1
                            sample = None
                            while (px + i < len(points[0]) - 1) or (px - i > 0) :
                                if px + i < len(points[0]) - 2:
                                    oleft = points[j][px + i]
                                    if not oleft.equals(infp):
                                        sample = px + i
                                        break
                                if px - i > 0:
                                    oright = points[j][px - i]
                                    if not oright.equals(infp):
                                        sample = px - i
                                        break
                                i += 1
                                
                            if sample != None:
                                d1 = points[0][sample].distance(points[1][sample])
                                d2 = points[0][sample].distance(points[2][sample])
                                ratio = d1 / d2
                            else:
                                ratio = 1
                            
                            p1_ = np.array(p[0]) - np.array(p[k])
                            p1_ = (p1_ / norm(p1_)) * norm(p1_) * ratio  + np.array(p[0])
                            p[j] = Point(p1_)
                            
                ''' snapping points to the outline '''
                if valids[0] == True:
                    points_[1].append(p[1])                
                    
                if valids[1] == True:
                    points_[2].append(p[2])
    
                points_[0].append(p[0])
                
        pp1 = [np.array(j) for j in points_[1]]
        pp2 = [np.array(j) for j in points_[2]]
        pp0 = [np.array(j) for j in points_[0]]
        if len(pp1) == 0 or len(pp2) == 0:
            #print "One half is empty"
            pp1 = copy.deepcopy(pp0)
            pp2 = copy.deepcopy(pp0)
            
        pp2.reverse()
        pp = pp1 + pp2
        caps.append([[0, len(pp) - 1], [len(pp1) - 1, len(pp1)]])
        ppp.append(pp)

    return ppp, caps, [points_[1], points_[2], points_[0], points_[0]]

def save(data_path, f, ch, sgs, cps, pts):
        data = np.load(join(data_path, f))
        if 'annotations' in data:
            anns = data['annotations']
        outlines = data['outlines']
        graphs = data['graphs']
        length = len(cm.all_chars)
        if 'segments' in data:
            segments = data['segments'].tolist()
            if len(segments) != length:
                segments = [[] for _ in range(length)]
        else:
            segments = [[] for _ in range(length)]
            
        if 'points' in data:
            points = data['points'].tolist()
            if len(points) != length:
                points = [[] for _ in range(length)]
        else:
            points = [[] for _ in range(length)]       
            
        if 'caps' in data:
            caps = data['caps'].tolist()
            if len(caps) != length:
                caps = [[] for _ in range(length)]
        else:
            caps = [[] for _ in range(length)]
                                  
        chars = cm.all_chars
        chx = chars.index(ch, )
        segments[chx] = sgs
        caps[chx] = cps
        points[chx] = pts
        np.savez(join(data_path, f), segments=segments, caps=caps, \
                 outlines=outlines, chars=chars, points=points,graphs=graphs,annotations=anns)

if __name__ == '__main__':
    import os
    path = '/media/phan/BIGDATA/SMARTFONTS'
    data_path = join(path, 'data')
    files = sorted([f for f in os.listdir(data_path) \
    if os.path.isfile(join(data_path, f)) and f.endswith('.npz')])
    sdata = np.load(join(path, 'word_specs.npz'))
    chrange = [26, 52] #[cm.all_chars.index('K', ),52] #
    specs = sdata['specs']
    mains = sdata['mains']
    #files = ['Felipa-Regular.ttf.npz', 'Fondamento-Regular.ttf.npz'] #,'Arbutus-Regular.ttf.npz','Baumans-Regular.ttf.npz']
    for f in files:
        data = np.load(join(data_path, f))
        if 'annotations' in data:
            anns = data['annotations']
            specs_ = np.array([len(a) for a in anns])
            if np.all(specs_[chrange[0]:chrange[1]] >= specs[chrange[0]:chrange[1]]):
                #if np.all(specs_ >= specs):
                valid = True
                for ch in range(chrange[0], chrange[1]):
                    for pt in range(specs[ch]):
                        if len(anns[ch][pt]) == 0:
                            valid = False
                            print f, '*(%d,%d)' % (ch, pt)
                            break
                if not valid: 
                    continue
                        
                print f
                outlines = data['outlines']
                graphs = data['graphs']
                length = len(cm.all_chars)
                if 'segments' in data:
                    segments = data['segments'].tolist()
                    if len(segments) != length:
                        segments = [[] for _ in range(length)]
                else:
                    segments = [[] for _ in range(length)]
                    
                if 'points' in data:
                    points = data['points'].tolist()
                    if len(points) != length:
                        points = [[] for _ in range(length)]
                else:
                    points = [[] for _ in range(length)]       
                    
                if 'caps' in data:
                    caps = data['caps'].tolist()
                    if len(caps) != length:
                        caps = [[] for _ in range(length)]
                else:
                    caps = [[] for _ in range(length)]
                                          
                chars = cm.all_chars#data['chars']
                
                for chx in range(chrange[0], chrange[1]):
                    print chars[chx], 
                    valid = True
                    modes = ['cap'] * len(anns[chx])
                    nmains = mains[chx]
                    for i in range(nmains):
                        modes[i] = 'stroke'
                                       
                    segs, cps, pts = segment_graph(graphs[chx], outlines[chx], anns[chx], modes=modes)
                    
                    if len(segs) == 0: valid = False
                    if all([len(seg) == 0 for seg in segs]): valid = False
                    if all([len(pt) > 0 for pt in pts]):
                        if any([len(pt[0]) == 0 for pt in pts]): valid = False
                    else:
                        valid = False
                    #if all([len(cp) == 0 for cp in cps]): valid = False 
                    
                    if valid:
                        segments[chx] = segs
                        caps[chx] = cps
                        points[chx] = pts
                    else:
                        print '#',
                 
                np.savez(join(data_path, f), segments=segments, caps=caps, \
                         outlines=outlines, chars=chars, points=points,graphs=graphs,annotations=anns)
                print     
