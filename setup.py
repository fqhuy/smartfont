# -*- coding: utf-8 -*-
"""
Created on Fri Jul 11 11:46:22 2014

@author: phan
"""

from distutils.core import setup, Extension

setup(
    name="geometry",
    ext_modules = [Extension("geometry", ["geometry.c"], extra_compile_args = ['-g'])],
    author = 'Phan Quoc Huy',
    author_email = 'quochuy.phan87@gmail.com',
    description = ("geometry processing"),
    license = 'GPL v3',
    keywords = 'tangent, normal',
    )
