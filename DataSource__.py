# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 02:26:31 2015

@author: phan
"""
import Common as cm
import numpy as np
import Feature as ft
from os.path import join
import os
import re
import cv2
from shapely.geometry import Point, asPolygon
from scipy.spatial.distance import euclidean
import Geometry as geo
from fast.Distance import hausdorff
from copy import deepcopy

def part_id(char, part):
    return char + str(part).zfill(2)

def word2part(word, chars=cm.capital_chars, dst_ch=None):
    acts = ['>', '<', '^']
    part1, part2 = re.split('>|\^|<', word)
    ch1, ch2 = part1[0], part2[0]
    pt1, pt2 = int(part1[1:]), int(part2[1:])
    
    if dst_ch == ch1:
        return acts.index(word[len(word) / 2]), chars.index(ch1), pt1, chars.index(ch2), pt2 
    else:
        return acts.index(word[len(word) / 2]), chars.index(ch2), pt2, chars.index(ch1), pt1
        
def get_parts(mode, chrange):
    parts = []
    chars = [cm.all_chars[c] for c in chrange]
    for ch in chars:
        if mode == 'outline' or mode == 'skel':
            frm, to = 0 , cm.mains[cm.all_chars.index(ch)]
        elif mode == 'cap':
            #frm, to = 0 , cm.specs[cm.all_chars.index(ch)] - cm.mains[cm.all_chars.index(ch)]
            frm, to =  cm.mains[cm.all_chars.index(ch)], cm.specs[cm.all_chars.index(ch)]
        elif mode == 'all':
            frm, to = 0, cm.specs[cm.all_chars.index(ch)]    
        for p in range(frm, to):
            parts.append(ch + str(p).zfill(2))
            
    return parts
    
def get_feature_names(mode, chrange):
    parts = get_parts(mode, chrange)
    n_parts = len(parts)
    fnames = []
    
    if mode is 'outline':
        for i in range(n_parts):
            for j in range(i + 1):
                if i != j:
                    fnames.append( parts[i] + '>' + parts[j] )
                    fnames.append( parts[i] + '<' + parts[j] )
                    
    elif mode in ['skel', 'cap']:
        for i in range(n_parts):
            for j in range(i + 1):
                if i != j:
                    fnames.append( parts[i] + '>' + parts[j] ) #normal comparision
                    fnames.append( parts[i] + '<' + parts[j] ) #flipped left-right
                    fnames.append( parts[i] + '^' + parts[j] ) #flipped up-down
    return fnames

def get_similarity_vec(fonts, parts, vocab, chars, dist=euclidean):
    '''
    extract the similarity vector from fonts
    '''
    sim_vecs = []
    #for each font
    for font in fonts:
        #print '.',
        n_words = len(vocab)
        sim_vec = np.zeros(len(vocab), dtype=float)
        #for each feat
        for i in range(n_words):
            #print '-',
            act, ch1, pt1, ch2, pt2 = word2part(vocab[i], chars)
            #only flip one part
            if len(font[ch1][pt1][0]) == 0 or len(font[ch2][pt2][act]) == 0:
                sim_vec[i] = np.nan
            else:
                sim_vec[i] = dist(font[ch1][pt1][0], font[ch2][pt2][act])
                    
        #sim_vec -= sim_vec.mean()
        
        #if not np.all(np.isnan(sim_vec)):
        #    sim_vec /= sim_vec[ np.bitwise_not(np.isnan(sim_vec)) ].max()
        
        sim_vecs.append(sim_vec)
        
    return sim_vecs    

    
def get_features(fonts, mode, chrange):
    '''
    extract features from parts
    '''
    
    all_feats = []
    for fname, data in fonts:
        #print fname
        all_feats.append([])
        for cx, ch in enumerate(data):
            if cx not in chrange:
                continue
            
            all_feats[-1].append([])
            for px, part in enumerate(ch):
                all_feats[-1][-1].append([[], [], []])
                if len(part) == 0:
                    continue
                
                if mode is 'outline' and px < cm.mains[cx]:
                    #feat = [ft._calc_feature_thickness(part)]
                    feat = []
                    feat.append( ft._calc_feature_stroke(part, mode = 'stroke', n_samples = 25, normalized=True).flatten() )
                    rpart = [part[0][::-1], part[1][::-1], part[2][::-1], part[3][::-1]]
                    feat.append( ft._calc_feature_stroke(rpart, mode = 'stroke', n_samples = 25, normalized=True).flatten() )
                    #feat = [tmp.flatten()]
                    #tmp1 = deepcopy(tmp)
                    #tmp1 = np.concatenate([tmp1[:len(tmp1/2)][::-1], tmp1[len(tmp1/2):][::-1]])
                    #feat.append(tmp1.flatten())
                    #feat.append(feat[0][::-1])
                    #all_feats[-1][-1].append(feat)
                    all_feats[-1][-1][-1] = feat
                    
                elif mode is 'skel' and px < cm.mains[cx]:
                    feat = []
                    feat.append(ft._calc_feature_skel(part, n_samples = 25, normalized=True).flatten())
                    feat.append(ft._calc_feature_skel(part * np.array([-1, 1]), n_samples = 25, normalized=True).flatten())                    
                    feat.append(ft._calc_feature_skel(part * np.array([1, -1]), n_samples = 25, normalized=True).flatten())
                    #all_feats[-1][-1].append(feat)
                    all_feats[-1][-1][-1] = feat
                    
                elif mode is 'cap' and px >= cm.mains[cx]:
                    feat = []
                    if len(part) == 0:
                        poly = Point(0,0).buffer(5)
                    else:
                        poly = asPolygon(part)
                    
                    samples = np.array(geo.sample_polyline(poly.exterior, n_samples = 30))    
                    feat.append(samples)
                    feat.append(samples * np.array([-1, 1]))                    
                    feat.append(samples * np.array([1, -1]))
                    #all_feats[-1][-1].append(feat)
                    all_feats[-1][-1][-1] = feat
                
    return all_feats
                
if __name__ == '__main__':
    from scipy.spatial.distance import wminkowski
    #from Common import dists
    
    path = cm.PATH
    files = sorted([f for f in os.listdir(join(path, 'data')) if f.endswith('.ttf.npz')])[:400]
    
    specs = cm.specs
    chrange = np.arange(26, 52) #for capital chars
    feats = None
    prefix = 'full3.' #'withorientation.'
    sc = cv2.createShapeContextDistanceExtractor() #sc.computeDistance ft.simple_hausdorff
    #hausdorff1 = lambda a, b: hausdorff(a, b, mode = 1)
    
    weuc1 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[1.0] * (len(a1)/2), [0.05] * (len(a1)/2)]).T.flatten())
    weuc2 = lambda a1, a2: wminkowski(a1, a2, p=2, w=np.vstack([[0.1] * (len(a1)/2), [1.0] * (len(a1)/2)]).T.flatten())
    dists = {'outline' : weuc1, 'cap' : ft.simple_hausdorff, 'skel' : weuc2}
    
    modes = ['skel'] #['outline' , 'cap', 'skel']
    for mode in modes:
        feats = None
        fonts = []
        print mode
        #get_feature_names(mode, chrange)
        if not os.path.isfile(join(path, prefix + mode + '.data.npz')):
            for f in files:
                data = np.load(join(path, 'data' , f))
                if 'points' in data:
                    points = data['points']
                    specs_ = np.array([len(a) for a in points])
                    if np.all(specs_[chrange] >= specs[chrange]):
                        print f 
                        if mode == 'skel':
                            anns = data['annotations']
                            graphs = data['graphs']
                            points = [[]] * len(cm.all_chars)
                            for ax, ann in enumerate(anns): #every character
                                graph = graphs[ax]
                                pss = []
                                for seg in ann: #every segment
                                    ps = []
                                    try:
                                        ps = [graph.node[p]['pos'].tolist() for p in seg]
                                    except:
                                        print '#',
                                        pass
                                    pss.append(list(ps))
                                points[ax] = np.array(pss)
                        
                        if mode == 'outline':
                            #this is to clean up the mess
                            for char in points:
                                for part in char:
                                    if isinstance(part, list) and len(part) == 4:
                                        for i in range(4):
                                            part[i] = np.array(part[i])
    
                        if mode == 'cap':
                            for char in points:
                                for px, part in enumerate(char):
                                    char[px] = np.array(char[px])
                                    
                        fonts.append((f, points))
       
            np.savez(join(path, prefix + mode + '.data.npz'), fonts=fonts)
        else:
            tmp = np.load(join(path, prefix + mode + '.data.npz'))
            feats = None
            if 'feats' in tmp:
                feats = tmp['feats']
            #elif 'fonts' in tmp:
            fonts = tmp['fonts']
            
        print 'total number of fonts, ', len(fonts)
        
        #Features for all parts in all characters in all fonts
        if feats is None:
            feats = get_features(fonts, mode, chrange)
            np.savez(join(path, prefix + mode + '.data.npz'), fonts=fonts, feats=feats)
        
        vocab = get_feature_names(mode, chrange)
        parts = get_parts(mode, chrange)
        chars = [c for cx, c in enumerate(cm.all_chars) if cx in chrange]
        sim_vecs = get_similarity_vec(feats, parts, vocab, chars, dists[mode])
        
        fnames = [f[0] for f in fonts]
        #np.savez(join(path, mode + '.data.npz'), fonts=fonts, feats=feats, sim_vecs = sim_vecs)
        np.savez(join(path, prefix + mode + '.pWs.npz'), fnames=fnames, all_feats=feats, \
                 pWs=sim_vecs, W=vocab, parts=parts, chars=chars)