import logging, gensim
import numpy as np
from os.path import join
import os
from DataSource import SimpleBOWCorpus
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

class StyleModel:
    def __init__(self, path,  mode, prefix='', model=None, parts=None, vocab = None):
        if model != None and parts != None and vocab != None:
            self.lda = model
            self.parts = parts
            self.vocab = vocab
            self.mode = mode
        else:
            self.path = path
            self.mode = mode
            fmodel = join(self.path, prefix + self.mode + '.lda_model.txt')
            self.lda = gensim.models.ldamodel.LdaModel.load(fmodel)
            ds = np.load(join(self.path, prefix + self.mode + '.pWs.npz'))
            self.parts = ds['parts']
            self.fnames = ds['fnames']
            
            fvocab = open(join(self.path, prefix + self.mode + '.vocab.fonts.txt'),'r')
            self.vocab = np.array([v[:-1] for v in fvocab.readlines()])
            fvocab.close()
    
    def stylize(self, given_font, given_chars, values=False):
        input_words = [select_by_chars(given_font, \
        given_chars, self.vocab)]
        
        bestwords, probs = find_best_words(self.lda, input_words, self.parts, self.vocab)
        bestwords = list(self.vocab[bestwords])
        if values:
            return [get_values(w) for w in bestwords], probs
        else:
            return bestwords, probs

def get_values(word):
    '''
    Q01:N02
    '''
    return word[0], word[4], int(word[1:3]), int(word[5:7])
    
def select_by_chars(doc,chars,vocab):
    '''
    only select entries in a doc that contains chars
    '''
    ws = vocab[np.array(doc)[:,0]]
    
    newdoc = []
    wss = [] #All words which contain 'chars'
    for v in vocab:
        ch1, ch2, _, _ = get_values(v)
        if ch1 in chars and ch2 in chars:
            wss.append(v)
            
    for wx, w in enumerate(ws):
        if w in wss:
            newdoc.append(doc[wx])
            #newdoc.append((w, doc[wx][1]))
            
    #create a fake word to prevent reestimation.
    for w in wss:
        if w not in ws:
            newdoc.append((list(vocab).index(w),0))
            
    return newdoc

def inference(lda_model, data):
    '''
    infer the probability of seeing a word in the vocabulary in the current document (data)
    '''
    topic_weights = lda_model.inference(data)
    #print topic_weights
    word_probs = (lda_model.state.get_lambda() * topic_weights[0].transpose()).sum(axis=0)
    return word_probs

def find_best_words(lda_model, data, parts, vocab):
    word_probs = inference(lda_model, data)
    best = []
    bestprobs = []
    w_in_data = np.array(data)[0,:,0]
    parts_in_data = set([v[0:3] for v in vocab[w_in_data]])
    for p in parts:
        in_data = False
        for w in vocab[w_in_data]:
            if p in w[0:3]:
                in_data = True
                break
            
        if in_data:
            continue
        
        ids = []
        #find all words starting with a certain part
        for vx, v in enumerate(vocab):
            if v.startswith(p) and v[-3:] in parts_in_data:
                #print p
                ids.append(vx)
        if len(ids) == 0:
            raise
                
        bestid = ids[word_probs[ids].argmax()]
        bestprobs.append( word_probs[ids].max() )
        best.append(bestid)
        
    return best, bestprobs

if __name__ == '__main__':
    path = '/media/phan/BIGDATA/SMARTFONTS'# + '/models/3.1'
    prefix = ''
    modes = ['outline'] #,'skel','outline']
    skip = True
    for mode in modes:
        ds = np.load(join(path, prefix + mode + '.pWs.npz'))
        parts = ds['parts']
        
        #fnames = ds['fnames']
        #id2word = gensim.corpora.Dictionary.load_from_text(join(path,'vocab.fonts.txt'))
        #id2word =  dict(zip(range(len(ds['W'])),ds['W']))
        
        fvocab = open(join(path, prefix + mode + '.vocab.fonts.txt'),'r')
        vocab = np.array([v[:-1] for v in fvocab.readlines()])
        id2word = dict([(i, w) for i, w in enumerate(vocab)])
    
        mm = gensim.corpora.ucicorpus.UciCorpus(join(path, prefix + mode + '.docword.fonts.txt'), \
        join(path, prefix + mode + '.vocab.fonts.txt'))
    
        #mm = gensim.corpora.ucicorpus.UciCorpus(join(path, 'docword.kos.txt'), \
        #join(path,'vocab.kos.txt')) 
       
        #lda = gensim.models.ldamodel.LdaModel( \
        #corpus=mm, id2word=None, \
        #num_topics=100, update_every=0, passes=20)
        fmodel = join(path, prefix + mode + '.lda_model.txt')
        
        if not os.path.isfile(fmodel) or not skip:
            print 'training model (%s, %s)' % (prefix, mode)
            lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=8, 
                                              update_every=1, chunksize=80, passes=5)
            #lda = gensim.models.ldamodel.LdaModel(corpus=mm, id2word=id2word, num_topics=8, \
            #                                              update_every=0, passes=10)
                
            #lda.print_topics(5)
            lda.save(fmodel)
        else:
            lda = gensim.models.ldamodel.LdaModel.load(fmodel)
        
        print lda.show_topics(8,topn=20, formatted=True)
        #docs = SimpleBOWCorpus(join(path, prefix + mode + '.docword.fonts.txt'))
        #print fnames[2]
        #print select_by_chars(docs[2],'HMO',vocab)
        #bestwords, probs = find_best_words(lda, [select_by_chars(docs[7],'HMO',vocab)], parts, vocab)
        #print zip( vocab[bestwords], probs )
    